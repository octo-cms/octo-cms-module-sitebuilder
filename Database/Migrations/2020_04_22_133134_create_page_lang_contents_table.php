<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageLangContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_lang_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('page_lang_id')->unsigned();
            $table->integer('position')->default(0);
            $table->string('content_type', 50)->nullable();
            $table->bigInteger('content_id')->nullable()->unsigned();
            $table->text('targets')->nullable();
            $table->text('data')->nullable();
            $table->text('layout')->nullable();

            $table->foreign('page_lang_id')->references('id')
                ->on('page_langs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_lang_contents');
    }
}
