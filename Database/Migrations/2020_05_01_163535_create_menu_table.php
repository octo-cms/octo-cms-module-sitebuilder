<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateMenuTable
 *
 * @author danielepasi
 */
class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('blade', 100);
            $table->timestamps();
        });

        Schema::create('menu_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('menu_id')->unsigned();
            $table->string('lang', 5);
            $table->string('title')->nullable();

            $table->foreign('menu_id')->references('id')
                ->on('menus')->onDelete('cascade');
        });

        Schema::create('menu_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('menu_id')->unsigned();
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->bigInteger('page_id')->unsigned()->nullable();
            $table->integer('order')->default(0);
            $table->string('type', 100)->nullable();
            $table->text('data')->nullable();

            $table->foreign('menu_id')->references('id')
                ->on('menus')->onDelete('cascade');

            $table->foreign('parent_id')->references('id')
                ->on('menu_items')->onDelete('cascade');

            $table->foreign('page_id')->references('id')
                ->on('pages')->onDelete('cascade');
        });

        Schema::create('menu_item_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('menu_item_id')->unsigned();
            $table->string('lang', 10);
            $table->string('label')->nullable();

            $table->foreign('menu_item_id')->references('id')
                ->on('menu_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
        Schema::dropIfExists('menu_lang');
    }
}
