<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_entities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('module', 50);
            $table->string('entity', 50);
            $table->string('blade', 100);
            $table->string('target')->default('page');
            $table->text('instructions')->nullable();
            $table->text('settings')->nullable();
            $table->text('layout')->nullable();
            $table->string('src')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_entities');
    }
}
