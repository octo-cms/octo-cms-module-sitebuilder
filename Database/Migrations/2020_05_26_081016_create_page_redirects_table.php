<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageRedirectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_redirects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('old_url', 60)->index();
            $table->bigInteger('page_lang_id')->unsigned();
            $table->timestamps();

            $table->foreign('page_lang_id')->references('id')
                ->on('page_langs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_redirects');
    }
}
