<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateFootersTable
 */
class CreateFootersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->nullable();
            $table->boolean('enabled')->default(true);
            $table->integer('order')->default(0);
            $table->string('content_align')->nullable();
            $table->timestamps();
        });

        Schema::create('footer_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('footer_id')->unsigned();
            $table->string('content_type', 50)->nullable();
            $table->bigInteger('content_id')->nullable()->unsigned();
            $table->integer('order')->default(0);
            $table->integer('size')->default(12);
            $table->text('targets')->nullable();
            $table->text('data')->nullable();
            $table->text('layout')->nullable();

            $table->foreign('footer_id')->references('id')
                ->on('footers')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footers');
        Schema::dropIfExists('footer_contents');
    }
}
