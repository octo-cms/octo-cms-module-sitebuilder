<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBlockHtmlTable
 */
class CreateBlockHtmlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_html', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('module', 100)->nullable();
            $table->string('blade', 100);
            $table->string('target')->default('page');
            $table->text('instructions')->nullable();
            $table->text('settings')->nullable();
            $table->text('layout')->nullable();
            $table->string('src')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('html_blocks');
    }
}
