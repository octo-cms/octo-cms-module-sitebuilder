<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('type')->default('standard');
            $table->string('pageable_type', 50)->nullable();
            $table->bigInteger('pageable_id')->nullable()->unsigned();
            $table->boolean('published')->default(true);
            $table->boolean('sitemap')->default(true);
            $table->string('change_freq', 30)->default('weekly');
            $table->decimal('sitemap_priority', 2,1)->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
