<?php

namespace OctoCmsModule\Sitebuilder\DTO;

/**
 * Class PageLangDataDTO
 * @package OctoCmsModule\Sitebuilder\DTO
 */
class MenuItemDTO
{
    /** @var string */
    public $label;
    /** @var string */
    public $link;
    /** @var string */
    public $type;
    /** @var array */
    public $children;
    /** @var string */
    public $id;
}
