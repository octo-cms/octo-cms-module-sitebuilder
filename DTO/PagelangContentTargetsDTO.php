<?php

namespace OctoCmsModule\Sitebuilder\DTO;

/**
 * Class PagelangContentTargetsDTO
 *
 * @package OctoCmsModule\Sitebuilder\DTO
 */
class PagelangContentTargetsDTO
{
    public $values;
    /**
     * @var string
     */
    public $type;

    /**
     * PagelangContentTargetsDTO constructor.
     *
     * @param string $type
     * @param $values
     */
    public function __construct(
        string $type,
        $values
    ) {
        $this->type = $type;
        $this->values = $values;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'type'   => $this->type,
            'values' => $this->values
        ];
    }
}
