<?php

namespace OctoCmsModule\Sitebuilder\DTO;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Sitebuilder\Entities\PageLang;

/**
 * Class PageLangDataDTO
 *
 * @package OctoCmsModule\Sitebuilder\DTO
 */
class PageLangDataDTO
{
    /** @var string */
    public $template;
    /** @var PageLang */
    public $pageLang;
    /** @var Collection */
    public $contents;
    /** @var string */
    public $metaTitle;
    /** @var string */
    public $metaDescription;
    /** @var string */
    public $logo;
    /** @var array */
    public $gtmSettings;
    /** @var Collection */
    public $footers;
    /** @var array */
    public $optimizeSettings;
    /** @var array */
    public $faviconsData;
    
    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'template'         => $this->template,
            'pageLang'         => $this->pageLang,
            'contents'         => $this->contents,
            'metaTitle'        => $this->metaTitle,
            'metaDescription'  => $this->metaDescription,
            'logo'             => $this->logo,
            'gtmSettings'      => $this->gtmSettings,
            'footers'          => $this->footers,
            'optimizeSettings' => $this->optimizeSettings,
            'faviconsData'     => $this->faviconsData,
        ];
    }
}
