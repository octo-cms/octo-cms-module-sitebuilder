<?php

namespace  OctoCmsModule\Sitebuilder\Traits;

use Facades\OctoCmsModule\Core\Services\SettingService;
use OctoCmsModule\Sitebuilder\Entities\Page;
use Tightenco\Collect\Support\Arr;

/**
 * Trait PageableTrait
 *
 * @package OctoCmsModule\Sitebuilder\Traits
 */
trait PageUrlTrait
{

    /**
     * @param  Page $page
     * @return string
     */
    public function getPageUrl(Page $page)
    {
        if (empty($language)) {
            $language = app()->getLocale();
        }

        $item = $page->pageLangs->where('lang', '=', $language)->first();

        $locale = SettingService::getSettingByName('locale');

        $prefix = $locale == $language ? '' : ('/' . $language);

        return $prefix . '/' . Arr::get($item, 'url', null);
    }
}
