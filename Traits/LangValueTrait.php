<?php

namespace  OctoCmsModule\Sitebuilder\Traits;

use Illuminate\Support\Collection;
use OctoCmsModule\Core\Utils\LanguageUtils;

/**
 * Trait PageableTrait
 *
 * @package OctoCmsModule\Sitebuilder\Traits
 */
trait LangValueTrait
{
    /**
     * @param  Collection $collection
     * @param  string     $attribute
     * @return string
     */
    public function getLangValue(Collection $collection, string $attribute)
    {
        return LanguageUtils::getLangValue($collection, $attribute);
    }
}
