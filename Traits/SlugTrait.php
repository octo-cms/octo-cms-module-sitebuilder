<?php

namespace OctoCmsModule\Sitebuilder\Traits;

use Illuminate\Support\Str;

/**
 * Trait SlugTrait
 *
 * @package OctoCmsModule\Sitebuilder\Traits
 */
trait SlugTrait
{
    /**
     * @param $value
     *
     * @return string
     */
    public function retrieveSlug(string $value)
    {
        return Str::slug($value);
    }
}
