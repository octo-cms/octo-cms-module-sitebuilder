<?php

namespace  OctoCmsModule\Sitebuilder\Traits;

use Illuminate\Database\Eloquent\Relations\MorphOne;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Trait PageableTrait
 *
 * @package OctoCmsModule\Sitebuilder\Traits
 */
trait PageableTrait
{
    /**
     * @return MorphOne
     */
    public function page()
    {
        return $this->morphOne(Page::class, 'pageable', 'pageable_type', 'pageable_id');
    }
}
