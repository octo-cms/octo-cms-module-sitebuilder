<?php

namespace OctoCmsModule\Sitebuilder\Providers;

use Config;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use OctoCmsModule\Sitebuilder\Console\GenerateSitemapCommand;
use OctoCmsModule\Sitebuilder\Console\InstallSitebuilderCommand;
use OctoCmsModule\Sitebuilder\Console\SyncPageContentsCommand;
use OctoCmsModule\Sitebuilder\Http\Middleware\GetLanguageFromPathMiddleware;
use OctoCmsModule\Sitebuilder\Interfaces\FavIconServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\FooterServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\LangServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\MenuServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\PageRedirectServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\PageServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\PictureServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\RouteServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\SettingServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\SitemapServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\SlideServiceInterface;
use OctoCmsModule\Sitebuilder\Services\PageRedirectService;
use OctoCmsModule\Sitebuilder\Tests\Mocks\FavIconServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\FooterServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\LangServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\MenuServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\PageServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\PictureServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\RouteServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\SitemapServiceMock;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\Slide;
use OctoCmsModule\Sitebuilder\Tests\Mocks\SlideServiceMock;

/**
 * Class SitebuilderServiceProvider
 *
 * @package OctoCmsModule\Sitebuilder\Providers
 */
class SitebuilderServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Sitebuilder';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'sitebuilder';

    /**
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));

        $router->aliasMiddleware('set-lang', GetLanguageFromPathMiddleware::class);

        $this->commands(
            [
            GenerateSitemapCommand::class,
            InstallSitebuilderCommand::class,
            SyncPageContentsCommand::class,
            ]
        );

        Blade::component(
            'page-content-handler',
            getBindingImplementation($this->moduleName, "View\Components\PageContentHandler")
        );

        Blade::component(
            'footer-content-handler',
            getBindingImplementation($this->moduleName, "View\Components\FooterContentHandlerComponent")
        );

        Blade::component(
            'main-menu-component',
            getBindingImplementation($this->moduleName, "View\Components\MainMenuComponent")
        );

        Blade::component(
            'page-default-component',
            "OctoCmsModule\Sitebuilder\View\Components\PageDefaultComponent"
        );

        Blade::component(
            'image-component',
            "OctoCmsModule\Sitebuilder\View\Components\ImageComponent"
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        Relation::morphMap(
            [
            'BlockEntity' => BlockEntity::class,
            'BlockHtml'   => BlockHtml::class,
            'Slide'       => Slide::class,
            'Menu'        => Menu::class,
            ]
        );

        if ($this->app->environment() == 'testing') {
            $this->app->bind(LangServiceInterface::class, LangServiceMock::class);
            $this->app->bind(SitemapServiceInterface::class, SitemapServiceMock::class);
            $this->app->bind(RouteServiceInterface::class, RouteServiceMock::class);
            $this->app->bind(MenuServiceInterface::class, MenuServiceMock::class);
            $this->app->bind(PageServiceInterface::class, PageServiceMock::class);
            $this->app->bind(FooterServiceInterface::class, FooterServiceMock::class);
            $this->app->bind(SlideServiceInterface::class, SlideServiceMock::class);
            $this->app->bind(MenuServiceInterface::class, MenuServiceMock::class);
            $this->app->bind(PageRedirectServiceInterface::class, PageRedirectService::class);
            $this->app->bind(PictureServiceInterface::class, PictureServiceMock::class);
            $this->app->bind(SettingServiceInterface::class, SettingServiceMock::class);
            $this->app->bind(FavIconServiceInterface::class, FavIconServiceMock::class);
        } else {
            $this->app->bind(
                LangServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\LangService")
            );

            $this->app->bind(
                SitemapServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\SitemapService")
            );

            $this->app->bind(
                RouteServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\RouteService")
            );

            $this->app->bind(
                MenuServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\MenuService")
            );

            $this->app->bind(
                PageServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\PageService")
            );

            $this->app->bind(
                FooterServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\FooterService")
            );

            $this->app->bind(
                SlideServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\SlideService")
            );

            $this->app->bind(
                MenuServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\MenuService")
            );

            $this->app->bind(
                PageRedirectServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\PageRedirectService")
            );

            $this->app->bind(
                PictureServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\PictureService")
            );

            $this->app->bind(
                SettingServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\SettingService")
            );

            $this->app->bind(
                FavIconServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\FavIconService")
            );
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes(
            [
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
            ], 'config'
        );
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'),
            $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes(
            [
            $sourcePath => $viewPath,
            ], ['views', $this->moduleNameLower . '-module-views']
        );

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->moduleName, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
