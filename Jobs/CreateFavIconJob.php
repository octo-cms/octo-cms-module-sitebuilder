<?php

namespace OctoCmsModule\Sitebuilder\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\FavIconServiceInterface;

/**
 * Class CreateFavIconJob
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Sitebuilder\Jobs
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CreateFavIconJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Name handle
     *
     * @param SettingServiceInterface $settingService SettingServiceInterface
     * @param FavIconServiceInterface $favIconService FavIconServiceInterface
     *
     * @return void
     */
    public function handle(SettingServiceInterface $settingService, FavIconServiceInterface $favIconService)
    {
        $data = $settingService->getSettingByName(SettingNameConst::FE_FAVICON);
        $favIconService->createFromUrl(($data['src'] ?? ''));
    }
}
