@if(!empty($faviconsData))
    @if(!empty($faviconsData['ico']))
        <link rel="shortcut icon" href="{{$faviconsData['ico']}}"/>
    @endif
    @if(!empty($faviconsData['size57x57']))
        <link rel="apple-touch-icon" sizes="57x57" href="{{$faviconsData['size57x57']}}">
    @endif
    @if(!empty($faviconsData['size60x60']))
        <link rel="apple-touch-icon" sizes="60x60" href="{{$faviconsData['size60x60']}}">
    @endif
    @if(!empty($faviconsData['size72x72']))
        <link rel="apple-touch-icon" sizes="72x72" href="{{$faviconsData['size72x72']}}">
    @endif
    @if(!empty($faviconsData['size76x76']))
        <link rel="apple-touch-icon" sizes="76x76" href="{{$faviconsData['size76x76']}}">
    @endif
    @if(!empty($faviconsData['size114x114']))
        <link rel="apple-touch-icon" sizes="114x114" href="{{$faviconsData['size114x114']}}">
    @endif
    @if(!empty($faviconsData['size120x120']))
        <link rel="apple-touch-icon" sizes="120x120" href="{{$faviconsData['size120x120']}}">
    @endif
    @if(!empty($faviconsData['size144x144']))
        <link rel="apple-touch-icon" sizes="144x144" href="{{$faviconsData['size144x144']}}">
    @endif
    @if(!empty($faviconsData['size152x152']))
        <link rel="apple-touch-icon" sizes="152x152" href="{{$faviconsData['size152x152']}}">
    @endif
    @if(!empty($faviconsData['size180x180']))
        <link rel="apple-touch-icon" sizes="180x180" href="{{$faviconsData['size180x180']}}">
    @endif
    @if(!empty($faviconsData['size192x192']))
        <link rel="icon" type="image/png" sizes="192x192" href="{{$faviconsData['size192x192']}}">
    @endif
    @if(!empty($faviconsData['size32x32']))
        <link rel="icon" type="image/png" sizes="32x32" href="{{$faviconsData['size32x32']}}">
    @endif
    @if(!empty($faviconsData['size96x96']))
        <link rel="icon" type="image/png" sizes="96x96" href="{{$faviconsData['size96x96']}}">
    @endif
    @if(!empty($faviconsData['size16x16']))
        <link rel="icon" type="image/png" sizes="16x16" href="{{$faviconsData['size16x16']}}">
    @endif
    @if(!empty($faviconsData['manifest']))
        <link rel="manifest" href="{{$faviconsData['manifest']}}">
    @endif

    <meta name="msapplication-TileColor" content="#ffffff">
    @if(!empty($faviconsData['size144x144']))
        <meta name="msapplication-TileImage" content="{{$faviconsData['size144x144']}}">
    @endif
    <meta name="theme-color" content="#ffffff">
@endif
