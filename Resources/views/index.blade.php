@extends($template . '::layouts.master')

@section('content')

    @if(!empty($contents))
        @foreach($contents as $content)
            <x-page-content-handler :content="$content"/>
        @endforeach
    @endif
@endsection
