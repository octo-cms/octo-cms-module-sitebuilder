<?php

return [
    'sitebuilder'    => 'site builder',
    'settings'       => 'impostazioni',
    'menu'           => 'menu',
    'footers'        => 'footers',
    'pages'          => 'pagine',
    'blocks'         => 'blocchi',
    'page_redirects' => 'redirects',
];
