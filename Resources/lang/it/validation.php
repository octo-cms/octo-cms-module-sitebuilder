<?php

return [
    'blade'        => [
        'required' => 'Il campo blade è obbligatorio.',
        'string'   => 'Il campo blade deve essere una stringa.',
    ],
    'module'       => [
        'required' => 'Il campo module è obbligatorio.',
        'string'   => 'Il campo module deve essere una stringa.',
    ],
    'entity'       => [
        'required' => 'Il campo entity è obbligatorio.',
        'string'   => 'Il campo entity deve essere una stringa.',
    ],
    'instructions' => [
        'string' => 'Il campo instructions deve essere una stringa.',
    ],
    'settings'     => [
        '*' => [
            'name' => ['string' => 'Il nome della variabile deve essere una stringa']
        ]
    ],
    'old_url'      => [
        'required' => 'Il campo old url è obbligatorio.',
        'string'   => 'Il campo old url deve essere una stringa.',
    ],
    'page_lang_id' => [
        'required' => 'Il campo page lang id è obbligatorio.',
        'int'      => 'Il campo page lang id deve essere un numero intero.',
    ],
    'name'         => [
        'required' => 'Il campo name è obbligatorio.',
        'string'   => 'Il campo name deve essere una stringa.',
    ],
    'change_freq'  => [
        'required' => 'Il campo frequency è obbligatorio.',
    ],
    'published'  => [
        'required' => 'Il campo published è obbligatorio.',
    ],
    'sitemap'  => [
        'required' => 'Il campo sitemap è obbligatorio.',
    ],
    'sitemap_priority'  => [
        'required' => 'Il campo sitemap priority è obbligatorio.',
    ],
    'pageLangs'    => [
        'required' => 'Le traduzioni delle pagine sono obbligatorie.',
        '*'        => [
            'lang' => ['required' => 'Il campo lang è obbligatorio']
        ]
    ],
    'menuLangs'    => [
        'required' => 'Le traduzioni sono obbligatorie.',
        '*'        => [
            'lang' => ['string' => 'Il campo lang deve essere una stringa.']
        ]
    ],
    '*'            => [
        'id'            => ['required' => 'Il campo id è obbligatorio.'],
        'order'         => ['required' => 'Il campo order è obbligatorio.'],
        'type'          => ['required' => 'Il campo type è obbligatorio.'],
        'enabled'       => ['required' => 'Il campo enabled è obbligatorio.'],
        'name'          => ['required' => 'Il campo name è obbligatorio.'],
        'content_align' => ['required' => 'Il campo content align è obbligatorio.'],
        'menuItemLangs' => [
            'required' => 'Il campo menuItemLangs è obbligatorio.',
            '*'        => [
                'lang'  => ['required' => 'Il campo lang è obbligatorio.'],
                'label' => ['required' => 'Il campo label è obbligatorio.']
            ],
        ],
    ],
];
