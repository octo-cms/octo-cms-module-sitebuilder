const PageLang = {
    id: null,
    lang: '',
    title: '',
    meta_description: '',
    page_id: 0,
    template:'full',
    url: '',
    contents: []
};

export default PageLang
