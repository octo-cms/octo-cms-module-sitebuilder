const BlockEntity = {
    id: null,
    blade: '',
    standard: false,
    custom: false,
    entity: '',
    instructions: '',
    settings: [],
    contentCount: 0,
    layout: []
};

export default BlockEntity
