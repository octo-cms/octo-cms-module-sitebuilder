const MenuItem = {
    id: null,
    menu_id: null,
    parent_id: null,
    order: null,
    type: null,
    data: {},
    menuItemLangs: [],
    const: {},
    uuid: null
};

export default MenuItem;
