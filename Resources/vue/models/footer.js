const Footer = {
    id: null,
    name: '',
    enabled: true,
    order: 0,
    content_align: 'justify-content-between',
    contents: []
};

export default Footer
