const PageLangContent = {
    id: null,
    content_id: null,
    content_type: null,
    data: [],
    page_lang_id: null,
    position: null,
    targets: {
        type: '',
        values: []
    },
    type: null,
    layout: [],
};

export default PageLangContent
