const Slide = {
    id: null,
    caption: null,
    sub_caption: null,
    action_label: null,
    action_link: null,
    pictures: []
};

export default Slide
