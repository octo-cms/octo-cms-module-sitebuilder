const FooterContent = {
    id: null,
    footer_id: null,
    content_type: '',
    content_id: null,
    order: 0,
    size: 6,
    targets: [],
    data: [],
    layout: [],
};

export default FooterContent
