const Page = {
    id: null,
    name: '',
    type: 'standard',
    change_freq: 'weekly',
    pageLangs: [],
    published: true,
    sitemap: true,
    sitemap_priority: 0.5,
    pictures: []
};

export default Page
