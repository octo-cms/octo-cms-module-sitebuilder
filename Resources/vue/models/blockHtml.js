const BlockHtml = {
    id: null,
    blade: '',
    standard:false,
    custom: false,
    instructions: '',
    settings: [],
    contentCount: 0,
    layout: []
};

export default BlockHtml
