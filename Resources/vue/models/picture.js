const Picture = {
    id: null,
    src: null,
    webp: null,
    tag: 'main',
    pictureLangs: [],
    newImage: {
        src: null,
        height: null,
        width: null
    }
};

export default Picture;
