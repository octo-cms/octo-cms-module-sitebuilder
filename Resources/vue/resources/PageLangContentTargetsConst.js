import BlockEntityTargetCustom from "../components/BlockEntity/BlockEntityTargetCustom";
import BlockEntityTargetTag from "../components/BlockEntity/BlockEntityTargetTag";
import BlockEntityTargetNewest from "../components/BlockEntity/BlockEntityTargetNewest";

const PAGELANG_CONTENT_TARGETS = {
    custom : {
        value: 'custom',
        label: 'Definisci gli elementi',
        component: BlockEntityTargetCustom,
        description: "Definisci i singoli elementi e l'ordine con cui dovranno comparire nel blocco"
    },
    tag : {
        value: 'tag',
        label: 'Scegli per tag',
        component: BlockEntityTargetTag,
        description: "Gli elementi del blocco devono appartenere ad un tag specifico"
    },
    newest : {
        value: 'newest',
        label: 'Più recenti',
        component: BlockEntityTargetNewest,
        description: "Inserisci nel blocco gli ultimi elementi inseriti"
    },
};

export default PAGELANG_CONTENT_TARGETS
