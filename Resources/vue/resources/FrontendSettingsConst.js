const FrontendSettingsConst = {
    date_formats: [
        {
            format: 'j F y',
            placeholder: '08:32',
    },
        {
            format: 'Y-m-d',
            placeholder: '08:32',
    },
        {
            format: 'm/d/Y',
            placeholder: '08:32',
    },
        {
            format: 'd/m/Y',
            placeholder: '08:32',
    },
    ],
    hour_formats: [
        {
            format: 'H:i',
            placeholder: '08:32',
    },
        {
            format: 'h:i',
            placeholder: '8:32',
    },
        {
            format: 'G:i A',
            placeholder: '08:32 AM',
    },
        {
            format: 'g:i A',
            placeholder: '8:32 AM',
    },
    ],
};

export default FrontendSettingsConst
