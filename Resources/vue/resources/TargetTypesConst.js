const TargetTypesConst = {
    custom: 'custom',
    tag: 'tag',
    newest: 'newest'
};

export default TargetTypesConst
