import BlockLayoutTypeSelect from "../components/BlockLayout/BlockLayoutType/BlockLayoutTypeSelect";
import BlockLayoutTypeBoolean from "../components/BlockLayout/BlockLayoutType/BlockLayoutTypeBoolean";

const BlockLayoutConst = {
    select: {
        type: 'select',
        variableComponent: BlockLayoutTypeSelect
    },
    boolean: {
        type: 'boolean',
        variableComponent: BlockLayoutTypeBoolean
    },
};

export default BlockLayoutConst
