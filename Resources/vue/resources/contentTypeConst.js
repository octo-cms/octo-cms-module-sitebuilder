const contentTypeConst = {
    BlockHtml : {
        icon : 'coding',
        label: 'Blocco HTML',
        name: 'BlockHtml'
    },
    BlockEntity : {
        icon : 'server',
        label: 'Blocco Entity',
        name: 'BlockEntity'
    },
};

export default contentTypeConst
