import {endpoints} from "@/router/endpoints";

const blockEntitySettings = {
    'testimonials' : {
        name : 'testimonials',
        label: 'testimonials',
        routeIds: endpoints.BLOCK_ENTITY_IDS_TESTIMONIALS
    },
    'services' : {
        name : 'services',
        label: 'Servizi',
        routeIds: endpoints.BLOCK_ENTITY_IDS_SERVICES
    }
};

export default blockEntitySettings
