import MenuItemParent from "../components/Menu/MenuItemParent";
import MenuItemPage from "../components/Menu/MenuItemPage";
import MenuItemExternal from "../components/Menu/MenuItemExternal";
import MenuItemCategoryProduct from "../components/Menu/MenuItemCategoryProduct";
import MenuItemCategoryBlog from "../components/Menu/MenuItemCategoryBlog";
import MenuItemCategoryProductTag from "../components/Menu/MenuItemCategoryProductTag";
import MenuItemCategoryBlogTag from "../components/Menu/MenuItemCategoryBlogTag";

const MENUITEMS_CONST = [
    {
        label: 'Menu root con figli',
        icon: 'diagram',
        variant: 'info',
        module: 'core',
        type: 'parent',
        component: MenuItemParent,
        data: {}
    },
    {
        label: 'Collegato ad una pagina',
        icon: 'web-programming',
        variant: 'success',
        module: 'core',
        type: 'page',
        component: MenuItemPage,
        data: {page: {}, anchor: ''}
    },
    {
        label: 'Link esterno',
        icon: 'external-link-symbol',
        variant: 'warning',
        module: 'core',
        type: 'external',
        component: MenuItemExternal,
        data: {link: ''}
    },
    {
        label: 'Categoria Prodotto',
        icon: 'list',
        variant: 'secondary',
        module: 'catalog',
        type: 'categoryCatalog',
        component: MenuItemCategoryProduct,
        data: {root: ''}
    },
    {
        label: 'Categoria Prodotto TAG',
        icon: 'tag',
        variant: 'secondary',
        module: 'catalog',
        type: 'categoryCatalogTag',
        component: MenuItemCategoryProductTag,
        data: {tag: ''}
    },
    {
        label: 'Categoria Blog',
        icon: 'list',
        variant: 'secondary',
        module: 'blog',
        type: 'categoryBlog',
        component: MenuItemCategoryBlog,
        data: {root:''}
    },
    {
        label: 'Categoria Blog TAG',
        icon: 'tag',
        variant: 'secondary',
        module: 'catalog',
        type: 'categoryBlogTag',
        component: MenuItemCategoryBlogTag,
        data: {tag:''}
    },
    ];

    export default MENUITEMS_CONST
