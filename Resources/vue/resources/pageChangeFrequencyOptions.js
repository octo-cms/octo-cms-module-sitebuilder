const PageChangeFrequencyOptions = [
    {
        text: 'Daily',
        value: 'daily',
    },
    {
        text: 'Weekly',
        value: 'weekly',
    },
    {
        text: 'Monthly',
        value: 'monthly',
    },
    {
        text: 'Yearly',
        value: 'yearly',
    },
    ];

    export default PageChangeFrequencyOptions
