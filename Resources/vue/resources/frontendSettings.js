import PictureHtml from "@octoCmsComponents/models/pictureHtml";

const FrontendSettings = {
    fe_admin_mail: '',
    fe_title: [],
    fe_description: [],
    fe_address: [],
    fe_date_format: '',
    fe_hour_format: '',
    fe_main_language: '',
    fe_available_languages: [],
    fe_logo: PictureHtml,
    fe_favicon: PictureHtml,
    fe_gtm: { enabled: false, container_id: null },
    fe_google_optimize: { enabled: false, container_id: null, property_id: null},
    fe_iubenda: { enabled: false, link: '' },
    fe_social_sharing: {
        facebook: { enabled: false, link: null },
        twitter: { enabled: false, link: null },
        instagram: { enabled: false, link: null }
    },
};

export default FrontendSettings
