import BlockVariableString from "../components/BlockHtml/BlockVariable/BlockVariableString";
import BlockVariableHtml from "../components/BlockHtml/BlockVariable/BlockVariableHtml";
import BlockVariableImage from "../components/BlockHtml/BlockVariable/BlockVariableImage";
import BlockVariableBulletList from "../components/BlockHtml/BlockVariable/BlockVariableBulletList";

const blockHtmlConst = {
    string : {
        icon : 'text-1',
        label: 'Stringa',
        variableComponent : BlockVariableString
    },
    html : {
        icon : 'coding',
        label: 'HTML',
        variableComponent : BlockVariableHtml
    },
    image : {
        icon : 'image',
        label: 'Immagine',
        variableComponent : BlockVariableImage
    },
    bulletList: {
        icon: 'list',
        label: 'elenco puntato',
        variableComponent : BlockVariableBulletList
    }
};

export default blockHtmlConst
