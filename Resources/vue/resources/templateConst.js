const templateConst = {
    'full' : {
        label: 'Full',
        value: 'full'
    },
    'main-sx' : {
        label: '2 Cols - Main SX',
        value: 'main-sx'
    },
    'main-dx' : {
        label: '2 Cols - Main SX',
        value: 'main-dx'
    },
    '3-cols' : {
        label: '3 Cols',
        value: '3-cols'
    },
};

export default templateConst
