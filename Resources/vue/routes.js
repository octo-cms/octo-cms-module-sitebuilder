const routes = {
    CONFIG_SETTINGS_SITE_POST: 'api/admin/v1/sitebuilder/settings',

    /** CACHE **/
    CACHE_FLUSH: 'api/admin/v1/cache/flush',

    /** MENU **/
    MENU_INDEX: 'api/admin/v1/sitebuilder/menu',
    MENU_STORE: 'api/admin/v1/sitebuilder/menu',
    MENU_UPDATE: 'api/admin/v1/sitebuilder/menu/{id}',

    /** MENU ITEMS **/
    MENU_ITEMS_STORE: 'api/admin/v1/sitebuilder/menu/{id}/menu-items',

    /** FOOTER **/
    FOOTER_INDEX: 'api/admin/v1/sitebuilder/footers',
    FOOTER_STORE: 'api/admin/v1/sitebuilder/footers',
    FOOTER_GET_ALL_CONTENTS: 'api/admin/v1/sitebuilder/footers/get-all-contents',

    /** PAGES **/
    PAGES_SHOW: 'api/admin/v1/sitebuilder/pages/{id}',
    PAGES_VIEW_SHOW: 'sitebuilder/pages/{id}',
    PAGES_STORE: 'api/admin/v1/sitebuilder/pages',
    PAGES_UPDATE: 'api/admin/v1/sitebuilder/pages/{id}',
    PAGES_GET_ALL_CONTENTS: 'api/admin/v1/sitebuilder/pages/get-all-contents',
    PAGES_LANG_GET: 'api/admin/v1/sitebuilder/pages/{id}/page-langs',
    PAGES_LANG_UPDATE_CONTENTS: 'api/admin/v1/sitebuilder/pages/{id}/page-lang-contents',
    PAGES_REDIRECTS_STORE: 'api/admin/v1/sitebuilder/pages/redirects',
    PAGES_REDIRECTS_UPDATE: 'api/admin/v1/sitebuilder/pages/redirects/{id}',
    PAGES_REDIRECTS_DELETE: 'api/admin/v1/sitebuilder/pages/redirects/{id}',
    PAGES_LANG_SEARCH: 'api/admin/v1/sitebuilder/pages/redirects/search-page-lang-by-query',

    /** BLOCK HTML **/
    BLOCK_HTML_STORE: 'api/admin/v1/sitebuilder/block-html',
    BLOCK_HTML_UPLOAD_MEDIA: 'api/admin/v1/sitebuilder/block-html/upload-media',

    /** BLOCK ENTITY **/
    BLOCK_ENTITY_STORE: 'api/admin/v1/sitebuilder/block-entity',
    BLOCK_ENTITY_IDS_TESTIMONIALS: 'api/admin/v1/testimonials/block-entity-ids',
    BLOCK_ENTITY_IDS_SERVICES: 'api/admin/v1/services/block-entity-ids',
    BLOCK_ENTITY_IDS_PRODUCTS: 'api/admin/v1/catalog/products/block-entity-ids',
    BLOCK_ENTITY_IDS_NEWS: 'api/admin/v1/blog/news/block-entity-ids',
    BLOCK_ENTITY_IDS_PAGES: 'api/admin/v1/sitebuilder/pages/block-entity-ids',
    BLOCK_ENTITY_IDS_SLIDES: 'api/admin/v1/sitebuilder/slides/block-entity-ids',

    /** SLIDES **/
    SLIDES_STORE: 'api/admin/v1/sitebuilder/slides',
    SLIDES_SHOW: 'api/admin/v1/sitebuilder/slides/{id}',
    SLIDES_UPDATE: 'api/admin/v1/sitebuilder/slides/{id}',
    SLIDES_DELETE: 'api/admin/v1/sitebuilder/slides/{id}',

    /** SELECT **/
    SELECT: {
        pages: 'api/admin/v1/select/sitebuilder/pages',
        categoriesProducts: 'api/admin/v1/select/categories-products',
        categoriesNews: 'api/admin/v1/select/categories-news',
    },

    /** DATATABLES **/
    DATATABLES: {
        pages: 'api/admin/v1/datatables/sitebuilder/pages',
        pageRedirects: 'api/admin/v1/datatables/sitebuilder/pages/redirects',
        blockHtml: 'api/admin/v1/datatables/sitebuilder/block-html',
        blockEntities: 'api/admin/v1/datatables/sitebuilder/block-entities',
        slides: 'api/admin/v1/datatables/sitebuilder/slides',
    },
};

export {
    routes
}
