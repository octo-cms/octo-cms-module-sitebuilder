<?php


Route::group(
    ['prefix' => env('MIX_ADMIN_PREFIX'), 'middleware' => ['auth-admin']],
    function () {
        Route::group(
            ['prefix' => 'sitebuilder'],
            function () {
                Route::get(
                    'settings',
                    getRouteAction("Sitebuilder", "VueRouteController", 'settings')
                )
                ->name('admin.vue-route.sitebuilder.settings');

                Route::get(
                    'menu',
                    getRouteAction("Sitebuilder", "VueRouteController", 'menu')
                )
                ->name('admin.vue-route.sitebuilder.menu');

                Route::get(
                    'footers',
                    getRouteAction("Sitebuilder", "VueRouteController", 'footers')
                )
                ->name('admin.vue-route.sitebuilder.footers');

                Route::group(
                    ['prefix' => 'pages'],
                    function () {
                        Route::get(
                            '',
                            getRouteAction("Sitebuilder", "VueRouteController", 'pages')
                        )
                        ->name('admin.vue-route.sitebuilder.pages');

                        Route::get(
                            '{id}',
                            getRouteAction("Sitebuilder", "VueRouteController", 'showPage')
                        )
                        ->name('admin.vue-route.sitebuilder.show.page');
                    }
                );

                Route::get(
                    'blocks',
                    getRouteAction("Sitebuilder", "VueRouteController", 'blocks')
                )
                ->name('admin.vue-route.sitebuilder.blocks');

                Route::get(
                    'page-redirects',
                    getRouteAction("Sitebuilder", "VueRouteController", 'pageRedirects')
                )
                ->name('admin.vue-route.sitebuilder.page.redirects');
            }
        );
    }
);


$middlewares = [];
$middlewares[] = 'set-lang';
if (Module::has('Shop') && Module::isEnabled('Shop')) {
    $middlewares[] = 'choose-shop';
}

Route::group(
    ['middleware' => $middlewares],
    function () {
        Route::get('{any?}', getRouteAction("Sitebuilder", "RouteController", 'index'))
            ->where('any', '.*')
            ->name('sitebuilder.any.index');
    }
);
