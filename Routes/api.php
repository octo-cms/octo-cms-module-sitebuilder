<?php

use Illuminate\Support\Facades\Route;

Route::group(
    ['prefix' => 'admin/v1'],
    function () {

        Route::group(
            ['middleware' => ['auth:sanctum']],
            function () {

                Route::group(
                    ['prefix' => 'sitebuilder'],
                    function () {

                        Route::group(
                            ['prefix' => 'menu'],
                            function () {
                                Route::get(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\MenuController", 'index')
                                )->name('sitebuilder.menu.index');
                                Route::post(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\MenuController", 'store')
                                )->name('sitebuilder.menu.store');

                                Route::group(
                                    ['prefix' => '{id}'],
                                    function () {
                                        Route::get(
                                            '',
                                            getRouteAction("Sitebuilder", "V1\MenuController", 'show')
                                        )->name('sitebuilder.menu.show');

                                        Route::put(
                                            '',
                                            getRouteAction("Sitebuilder", "V1\MenuController", 'update')
                                        )->name('sitebuilder.menu.update');

                                        Route::post(
                                            'menu-items',
                                            getRouteAction("Sitebuilder", "V1\MenuController", 'storeMenuItems')
                                        )->name('sitebuilder.menu.store.items');
                                    }
                                );
                            }
                        );

                        Route::group(
                            ['prefix' => 'footers'],
                            function () {
                                Route::get(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\FooterController", 'index')
                                )->name('sitebuilder.footers.index');

                                Route::post(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\FooterController", 'store')
                                )->name('sitebuilder.footers.store');

                                Route::get(
                                    'get-all-contents',
                                    getRouteAction("Sitebuilder", "V1\FooterController", 'getAllContents')
                                )->name('sitebuilder.footers.get.all.contents');
                            }
                        );

                        Route::group(
                            ['prefix' => 'pages'],
                            function () {

                                Route::post(
                                    'block-entity-ids',
                                    getRouteAction("Sitebuilder", "V1\BlockEntityController", 'pageBlockEntityIds')
                                )->name('sitebuilder.pages.block.entity.ids');

                                Route::get(
                                    'get-all-contents',
                                    getRouteAction("Sitebuilder", "V1\PageController", 'getAllContents')
                                )->name('sitebuilder.pages.ge.all.contents');

                                Route::post(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\PageController", 'store')
                                )->name('sitebuilder.pages.store');

                                Route::group(
                                    ['prefix' => 'page-langs'],
                                    function () {
                                    }
                                );

                                Route::group(
                                    ['prefix' => '{id}'],
                                    function () {
                                        Route::get(
                                            '',
                                            getRouteAction("Sitebuilder", "V1\PageController", 'show')
                                        )->name('sitebuilder.pages.show');

                                        Route::get(
                                            'page-langs',
                                            getRouteAction("Sitebuilder", "V1\PageController", 'pageLangs')
                                        )->name('sitebuilder.pages.page.langs.get');

                                        Route::put(
                                            'page-lang-contents',
                                            getRouteAction("Sitebuilder", "V1\PageController", 'updatePageLangsContents')
                                        )->name('sitebuilder.pages.page.langs.update.contents');

                                        Route::put(
                                            'page-langs',
                                            getRouteAction("Sitebuilder", "V1\PageController", 'pageLangs')
                                        )->name('sitebuilder.pages.page.langs');


                                        Route::put(
                                            '',
                                            getRouteAction("Sitebuilder", "V1\PageController", 'update')
                                        )->name('sitebuilder.pages.update');
                                    }
                                );
                                Route::group(
                                    ['prefix' => 'redirects'],
                                    function () {
                                        Route::post(
                                            '',
                                            getRouteAction("Sitebuilder", "V1\PageRedirectController", 'store')
                                        )->name('sitebuilder.pages.redirects.store');

                                        Route::post(
                                            'search-page-lang-by-query',
                                            getRouteAction("Sitebuilder", "V1\PageRedirectController", 'searchPageLangByQuery')
                                        )->name('sitebuilder.pages.redirects.page.lang.search');

                                        Route::group(
                                            ['prefix' => '{id}'],
                                            function () {
                                                Route::put(
                                                    '',
                                                    getRouteAction("Sitebuilder", "V1\PageRedirectController", 'update')
                                                )->name('sitebuilder.pages.redirects.update');
                                                Route::get(
                                                    '',
                                                    getRouteAction("Sitebuilder", "V1\PageRedirectController", 'show')
                                                )->name('sitebuilder.pages.redirects.show');
                                                Route::delete(
                                                    '',
                                                    getRouteAction("Sitebuilder", "V1\PageRedirectController", 'delete')
                                                )->name('sitebuilder.pages.redirects.delete');
                                            }
                                        );
                                    }
                                );
                            }
                        );

                        Route::group(
                            ['prefix' => 'block-html'],
                            function () {
                                Route::post(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\BlockHtmlController", 'store')
                                )->name('sitebuilder.block.html.store');

                                Route::post(
                                    'upload-media',
                                    getRouteAction("Sitebuilder", "V1\BlockHtmlController", 'uploadMedia')
                                )->name('sitebuilder.block.html.upload.media');
                            }
                        );

                        Route::group(
                            ['prefix' => 'block-entity'],
                            function () {
                                Route::post(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\BlockEntityController", 'store')
                                )->name('sitebuilder.block.entities.store');
                            }
                        );

                        Route::group(
                            ['prefix' => 'slides'],
                            function () {
                                Route::post(
                                    'block-entity-ids',
                                    getRouteAction("Sitebuilder", "V1\BlockEntityController", 'slideBlockEntityIds')
                                )
                                ->name('sitebuilder.slides.block.entity.ids');
                                Route::post(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\SlideController", 'store')
                                )->name('sitebuilder.slides.store');
                                Route::group(
                                    ['prefix' => '{id}'],
                                    function () {
                                        Route::get(
                                            '',
                                            getRouteAction("Sitebuilder", "V1\SlideController", 'show')
                                        )->name('sitebuilder.slides.show');
                                        Route::put(
                                            '',
                                            getRouteAction("Sitebuilder", "V1\SlideController", 'update')
                                        )->name('sitebuilder.slides.update');
                                        Route::delete(
                                            '',
                                            getRouteAction("Sitebuilder", "V1\SlideController", 'delete')
                                        )->name('sitebuilder.slides.delete');
                                    }
                                );
                            }
                        );

                        Route::group(
                            ['prefix' => 'settings'],
                            function () {
                                Route::post('', getRouteAction("Sitebuilder", "V1\SettingController", 'updateSiteSettings'))->name('sitebuilder.settings.update.site.settings');
                            }
                        );
                    }
                );

                Route::group(
                    ['prefix' => 'datatables/sitebuilder'],
                    function () {
                        Route::post(
                            'block-html',
                            getRouteAction("Sitebuilder", "V1\BlockHtmlController", 'datatableIndex')
                        )->name('admin.datatables.sitebuilder.block.html.index');
                        Route::post(
                            'block-entities',
                            getRouteAction("Sitebuilder", "V1\BlockEntityController", 'datatableIndex')
                        )->name('admin.datatables.sitebuilder.block.entities.index');

                        Route::group(
                            ['prefix' => 'pages'],
                            function () {
                                Route::post(
                                    '',
                                    getRouteAction("Sitebuilder", "V1\PageController", 'datatableIndex')
                                )->name('admin.datatables.sitebuilder.pages.index');
                                Route::post(
                                    'redirects',
                                    getRouteAction("Sitebuilder", "V1\PageRedirectController", 'datatableIndex')
                                )->name('admin.datatables.sitebuilder.pages.redirects.index');
                            }
                        );
                        Route::post(
                            'slides',
                            getRouteAction("Sitebuilder", "V1\SlideController", 'datatableIndex')
                        )->name('admin.datatables.sitebuilder.slides.index');
                    }
                );

                Route::group(
                    ['prefix' => 'select/sitebuilder'],
                    function () {
                        Route::post(
                            'pages',
                            getRouteAction("Sitebuilder", "V1\PageController", 'selectIndex')
                        )->name('admin.select.sitebuilder.pages.index');
                    }
                );
            }
        );
    }
);
