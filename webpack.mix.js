const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

require('laravel-mix-alias');
mix.alias({
    '@octoCmsComponents': '../Admin/Resources/octo-cms-components',
    '@adminNodeModules': '../Admin/node_modules',
});

mix.options({
    postCss: [
        require('autoprefixer'),
    ],
})

const assetPath = '../../public/assets-' + process.env.MIX_ADMIN_PREFIX;

const assetVuePath = assetPath + '/vue/sitebuilder/'

mix.js(__dirname + '/Resources/vue/pages/settings.js', assetVuePath + 'settings.min.js');
mix.js(__dirname + '/Resources/vue/pages/menu.js', assetVuePath + 'menu.min.js');
mix.js(__dirname + '/Resources/vue/pages/footers.js', assetVuePath + 'footers.min.js');
mix.js(__dirname + '/Resources/vue/pages/pages.js', assetVuePath + 'pages.min.js');
mix.js(__dirname + '/Resources/vue/pages/blocks.js', assetVuePath + 'blocks.min.js');
mix.js(__dirname + '/Resources/vue/pages/pageBuilder.js', assetVuePath + 'page-builder.min.js');
mix.js(__dirname + '/Resources/vue/pages/pageRedirects.js', assetVuePath + 'pageRedirects.min.js');

// if (mix.inProduction()) {
    mix.version();
// }
