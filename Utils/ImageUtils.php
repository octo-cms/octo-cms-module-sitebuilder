<?php

namespace  OctoCmsModule\Sitebuilder\Utils;

use Jenssegers\Agent\Agent;
use OctoCmsModule\Core\Services\PictureService;

/**
 * Class ImageUtils
 *
 * @package OctoCmsModule\Sitebuilder\Utils
 */
class ImageUtils
{

    /**
     * @param  array $image
     * @return string
     */
    public static function getSrc(array $image)
    {

        $path = (new Agent())->browser() === 'Chrome'
            ? $image['webp']
            : $image['src'];

        return PictureService::BUCKET_BASE_URL . '/' .
            env('GOOGLE_CLOUD_STORAGE_BUCKET', '') . '/' .  $path ;
    }
}
