<?php

namespace OctoCmsModule\Sitebuilder\Utils;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;

/**
 * Class LivewireUtils
 *
 * @package OctoCmsModule\Sitebuilder\Utils
 */
class LivewireUtils
{
    /**
     * @param Builder $builder
     * @param array   $targets
     *
     * @return array|null
     */
    public function getEntities(Builder $builder, array $targets)
    {

        switch (Arr::get($targets, 'type', '')) {
            case PageLangContent::TARGET_TYPE_CUSTOM:
                return $this->getCustomEntities($builder, $targets);
            case PageLangContent::TARGET_TYPE_NEWEST:
                return $this->getNewestEntities($builder, $targets);
            case PageLangContent::TARGET_TYPE_TAG:
                return $builder
                    ->withAllTags([Arr::get($targets, 'values.tag', '')])
                    ->get();
            default:
                return null;
        }
    }

    /**
     * @param Builder $builder
     * @param array   $targets
     *
     * @return array|null
     */
    protected function getCustomEntities(Builder $builder, array $targets)
    {
        $ids = Arr::pluck(Arr::get($targets, 'values', []), 'id');

        $entities = $builder->whereIn('id', $ids)->get();

        if (empty($entities)) {
            return null;
        }

        $array = [];
        foreach ($ids as $id) {
            $array[] = $entities->find($id);
        }

        return $array;
    }

    /**
     * @param Builder $builder
     * @param array   $targets
     *
     * @return Builder[]|Collection
     */
    protected function getNewestEntities(Builder $builder, array $targets)
    {
        return $builder->orderBy('updated_at', 'desc')
            ->take(Arr::get($targets, 'values.limit', 5))
            ->get();
    }
}
