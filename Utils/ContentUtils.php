<?php

namespace OctoCmsModule\Sitebuilder\Utils;

use Illuminate\Support\Arr;

/**
 * Class ContentUtils
 *
 * @package OctoCmsModule\Sitebuilder\Utils
 */
class ContentUtils
{
    /**
     * @param $contentData
     *
     * @return array
     */
    public static function parseBlockData($contentData)
    {
        $data = [];
        foreach ($contentData as $variable) {
            $data[$variable['name']] = $variable['value'];
        }

        return $data;
    }

    /**
     * @param $contentLayout
     *
     * @return array
     */
    public static function parseBlockLayout($contentLayout)
    {
        $layout = [];

        foreach ($contentLayout as $item) {
            switch ($item['type']) {
                case 'boolean':
                    $layout[$item['name']] = Arr::get($item, 'selected', null);
                    break;
                case 'select':
                    $layout[$item['name']] = Arr::get($item, 'selected.value', null);
                    break;
            }
        }

        return $layout;
    }
}
