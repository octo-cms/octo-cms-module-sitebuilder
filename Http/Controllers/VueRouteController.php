<?php

namespace OctoCmsModule\Sitebuilder\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class VueRouteController
 *
 * @package OctoCmsModule\Sitebuilder\Http\Controllers
 */
class VueRouteController extends Controller
{
    /**
     * @return mixed
     */
    public function settings()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'sitebuilder/settings']);
    }

    /**
     * @return mixed
     */
    public function menu()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'sitebuilder/menu']);
    }

    /**
     * @return mixed
     */
    public function footers()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'sitebuilder/footers']);
    }

    /**
     * @return mixed
     */
    public function pages()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'sitebuilder/pages']);
    }

    /**
     * @return mixed
     */
    public function showPage()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'sitebuilder/page-builder']);
    }

    /**
     * @return mixed
     */
    public function blocks()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'sitebuilder/blocks']);
    }

    /**
     * @return mixed
     */
    public function blockEntities()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'sitebuilder/blockEntities']);
    }

    /**
     * @return mixed
     */
    public function pageRedirects()
    {
        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'sitebuilder/pageRedirects']);
    }
}
