<?php

namespace OctoCmsModule\Sitebuilder\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Sitebuilder\Http\Requests\StoreMenuItemsRequest;
use OctoCmsModule\Sitebuilder\Http\Requests\StoreMenuRequest;
use OctoCmsModule\Sitebuilder\Http\Requests\UpdateMenuRequest;
use OctoCmsModule\Sitebuilder\Interfaces\MenuServiceInterface;
use OctoCmsModule\Sitebuilder\Transformers\MenuItemResource;
use OctoCmsModule\Sitebuilder\Transformers\MenuResource;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MenuController
 *
 * @package OctoCmsModule\Sitebuilder\Http\Controllers\V1
 */
class MenuController extends Controller
{

    /**
     * @var MenuServiceInterface
     */
    protected $menuService;

    /**
     * MediaController constructor.
     *
     * @param MenuServiceInterface $menuService
     */
    public function __construct(MenuServiceInterface $menuService)
    {
        $this->menuService = $menuService;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $menus = Menu::all()->sortBy('name');

        $menus->load('menuLangs');

        return MenuResource::collection($menus);
    }

    /**
     * @param StoreMenuRequest $request
     *
     * @return JsonResponse|object
     */
    public function store(StoreMenuRequest $request)
    {
        $fields = $request->validated();
        $menu = new Menu();
        $menu->blade = Arr::get($fields, 'blade', null);
        $menu->name = Arr::get($fields, 'name', null);
        $menu->save();

        /**
 * @var Menu $menu
*/
        $menu = $this->menuService->saveMenu(
            $menu,
            $fields
        );

        $menu->load('menuLangs');

        return (new MenuResource($menu))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param $id
     *
     * @return MenuResource
     */
    public function show($id)
    {
        /**
 * @var Menu $menu
*/
        $menu = Menu::findOrFail($id);
        $menu->load('items');
        $menu->load('items.menuItemLangs');
        $menu->load('menuLangs');
        return new MenuResource($menu);
    }


    /**
     * @param StoreMenuItemsRequest $request
     * @param $id
     *
     * @return JsonResponse|object
     */
    public function storeMenuItems(StoreMenuItemsRequest $request, $id)
    {
        /**
 * @var Menu $menu
*/
        $menu = Menu::findOrFail($id);
        $menu->load('items');
        $menu->load('items.menuItemLangs');

        /**
 * @var Menu $menuUpdated
*/
        $menuUpdated = $this->menuService->storeMenuItems(
            $menu,
            $request->validated()
        );

        $menuItems = $menuUpdated->items()->get();
        $menuItems->load('menuItemLangs');

        return (MenuItemResource::collection($menuItems))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param UpdateMenuRequest $request
     * @param $id
     *
     * @return JsonResponse|object
     */
    public function update(UpdateMenuRequest $request, $id)
    {

        /**
 * @var Menu $menu
*/
        $menu = $this->menuService->saveMenu(
            Menu::findOrFail($id),
            $request->validated()
        );

        $menu->load('menuLangs');

        return (new MenuResource($menu))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }
}
