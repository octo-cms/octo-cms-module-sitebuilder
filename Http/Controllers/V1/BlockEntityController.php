<?php

declare(strict_types=1);

namespace OctoCmsModule\Sitebuilder\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\DTO\EntityIdsDTO;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Http\Requests\EntityIdsRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Core\Interfaces\EntityIdsServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\Slide;
use OctoCmsModule\Sitebuilder\Http\Requests\StoreBlockEntityRequest;
use OctoCmsModule\Sitebuilder\Transformers\BlockEntityResource;

use function response;

/**
 * @package OctoCmsModule\Sitebuilder\Http\Controllers\V1
 */
class BlockEntityController extends Controller
{
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService): JsonResponse
    {
        /**
 * @var Builder $entityBlocks
*/
        $entityBlocks = BlockEntity::query();

        $fields = $request->validated();

        $query = Arr::get($fields, 'query', '');
        if (!empty($query)) {
            $entityBlocks->where('blade', 'like', '%' . $query . '%')
                ->orWhere('module', 'like', '%' . $query . '%')
                ->orWhere('target', 'like', '%' . $query . '%');
        }

        $fields['orderBy'] = 'blade';

        $datatableDTO = $datatableService->getDatatableDTO($fields, $entityBlocks);

        $datatableDTO->collection = BlockEntityResource::collection($datatableDTO->builder->get());

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * @return JsonResponse|object
     */
    public function store(StoreBlockEntityRequest $request)
    {
        /**
 * @var array $fields
*/
        $fields = $request->validated();

        $id = Arr::get($fields, 'id', null);

        /**
 * @var BlockEntity $blockEntity
*/
        $blockEntity = !empty($id)
            ? BlockEntity::findOrFail($id)
            : new BlockEntity();

        $blockEntity->blade = Arr::get($fields, 'blade', null);
        $blockEntity->entity = Arr::get($fields, 'entity', null);
        $blockEntity->module = Arr::get($fields, 'module', null);
        $blockEntity->settings = Arr::get($fields, 'settings', null);
        $blockEntity->instructions = Arr::get($fields, 'instructions', null);
        $blockEntity->layout = Arr::get($fields, 'layout', null);
        $blockEntity->src = Arr::get($fields, 'src', null);

        $blockEntity->save();

        return (new BlockEntityResource($blockEntity))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function slideBlockEntityIds(
        EntityIdsRequest $request,
        EntityIdsServiceInterface $entityIdsService
    ): JsonResponse {
        $fields = $request->validated();

        /**
 * @var EntityIdsDTO $entityIdsDTO
*/
        $entityIdsDTO = $entityIdsService->getEntityIdsDTO(
            Slide::query()->whereNotIn('id', Arr::get($fields, 'excludedIds', [])),
            [
                'orderBy'     => 'id',
                'currentPage' => Arr::get($fields, 'currentPage', 1),
                'rowsInPage'  => Arr::get($fields, 'rowsInPage', 12),
            ]
        );

        $entityIdsDTO->collection = $entityIdsService->parseCollection(
            $entityIdsDTO->builder->get(),
            'id',
            'caption',
            'sub_caption'
        );

        return response()->json($entityIdsDTO, Response::HTTP_OK);
    }

    public function pageBlockEntityIds(
        EntityIdsRequest $request,
        EntityIdsServiceInterface $entityIdsService
    ): JsonResponse {
        $fields = $request->validated();

        /**
 * @var EntityIdsDTO $entityIdsDTO
*/
        $entityIdsDTO = $entityIdsService->getEntityIdsDTO(
            Page::with('pageLangs')->whereNotIn('id', Arr::get($fields, 'excludedIds', [])),
            [
                'orderBy'     => 'name',
                'currentPage' => Arr::get($fields, 'currentPage', 1),
                'rowsInPage'  => Arr::get($fields, 'rowsInPage', 12),
            ]
        );

        $entityIdsDTO->collection = $entityIdsService->parseMultiLangCollection(
            $entityIdsDTO->builder->get(),
            'page_langs',
            'id',
            'meta_title',
            'meta_description'
        );

        return response()->json($entityIdsDTO, Response::HTTP_OK);
    }
}
