<?php

namespace OctoCmsModule\Sitebuilder\Http\Controllers\V1;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Http\Requests\SelectRequest;
use OctoCmsModule\Core\Services\SelectService;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Http\Requests\SavePageRequest;
use OctoCmsModule\Sitebuilder\Http\Requests\UpdatePageLangsContentsRequest;
use OctoCmsModule\Sitebuilder\Interfaces\PictureServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\PageServiceInterface;
use OctoCmsModule\Sitebuilder\Transformers\BlockEntityResource;
use OctoCmsModule\Sitebuilder\Transformers\BlockHtmlResource;
use OctoCmsModule\Sitebuilder\Transformers\PageResource;
use OctoCmsModule\Sitebuilder\Transformers\PageLangResource;

/**
 * Class PageController
 *
 * @package OctoCmsModule\Sitebuilder\Http\Controllers\V1
 */
class PageController extends Controller
{

    /**
     * @var PageServiceInterface
     */
    protected $pageService;

    /**
     * @var PictureServiceInterface
     */
    protected $pictureService;

    /**
     * PageController constructor.
     *
     * @param PageServiceInterface    $pageService
     * @param PictureServiceInterface $pictureService
     */
    public function __construct(
        PageServiceInterface $pageService,
        PictureServiceInterface $pictureService
    ) {
        $this->pageService = $pageService;
        $this->pictureService = $pictureService;
    }

    /**
     * @param $id
     *
     * @return PageResource
     */
    public function show($id)
    {
        /**
 * @var Page $page
*/
        $page = Page::findOrFail($id);

        $page->load('pageLangs')
            ->load('pageLangs.contents')
            ->load('pageLangs.contents.type');

        return new PageResource($page);
    }

    /**
     * @param SavePageRequest $request
     *
     * @return JsonResponse|object
     */
    public function store(SavePageRequest $request)
    {
        /**
 * @var array
*/
        $fields = $request->validated();

        /**
 * @var Page
*/
        $page = $this->pageService->savePage(new Page(), $fields);

        $this->pictureService->savePicturesEntity($page, Arr::get($fields, 'pictures', []));

        $page->load('pageLangs')
            ->load('pageLangs.contents')
            ->load('pageLangs.contents.type');

        return (new PageResource($page))->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param DatatableRequest          $request
     * @param DatatableServiceInterface $datatableService
     *
     * @return JsonResponse
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        /**
 * @var Builder
*/
        $pages = Page::query()->orderByDesc('updated_at');

        /**
 * @var array
*/
        $fields = $request->validated();

        /**
 * @var string $query
*/
        $query = Arr::get($fields, 'query', '');

        $datatableDTO = $datatableService->getDatatableDTO(
            $fields,
            $this->pageService->filterPageBuilder(
                $pages,
                Arr::get($fields, 'filters', []),
                !empty($query) ? $query : ''
            )
        );

        $collection = $datatableDTO->builder->get();

        $collection
            ->load('pageLangs')
            ->load('pictures', 'pictures.pictureLangs');

        $datatableDTO->collection = PageResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * @param $id
     *
     * @return AnonymousResourceCollection
     */
    public function pageLangs($id)
    {

        $pageLangs = PageLang::where('page_id', '=', $id)
            ->with(
                [
                'contents' => function ($q) {
                    $q->orderBy('position');
                },
                'contents.type',
                ]
            )
            ->get();

        return PageLangResource::collection($pageLangs);
    }

    /**
     * @return JsonResponse|object
     */
    public function getAllContents()
    {
        return response()->json(
            [
            'blockEntity' => BlockEntityResource::collection(
                BlockEntity::where('target', '=', BlockEntity::TARGET_PAGE)
                    ->get()
                    ->sortBy('blade')
            ),
            'blockHtml'   => BlockHtmlResource::collection(
                BlockHtml::where('target', '=', BlockHtml::TARGET_PAGE)
                    ->get()->sortBy('blade')
            ),
            ]
        )->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param SavePageRequest $request
     * @param $id
     *
     * @return JsonResponse|object
     */
    public function update(SavePageRequest $request, $id)
    {
        /**
 * @var array
*/
        $fields = $request->validated();

        /**
 * @var Page $page
*/
        $page = Page::findOrFail($id);

        /**
 * @var Page
*/
        $pageUpdated = $this->pageService->savePage($page, $fields);

        $pageUpdated->load('pageLangs')
            ->load('pageLangs.contents')
            ->load('pageLangs.contents.type');

        $this->pictureService->savePicturesEntity($pageUpdated, Arr::get($fields, 'pictures', []));

        return (new PageResource($pageUpdated))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param SelectRequest $request
     * @param SelectService $selectService
     *
     * @return JsonResponse|object
     */
    public function selectIndex(SelectRequest $request, SelectService $selectService)
    {
        /**
 * @var array $fields
*/
        $fields = $request->validated();
        /**
 * @var string $query
*/
        $query = Arr::get($fields, 'query', '');

        $pages = Page::where('name', 'like', "%$query%")
            ->orderBy('name')
            ->get();

        return response()
            ->json($selectService->parseCollection($pages, 'name', 'id'))
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param UpdatePageLangsContentsRequest $request
     * @param $id
     *
     * @return AnonymousResourceCollection
     */
    public function updatePageLangsContents(UpdatePageLangsContentsRequest $request, $id)
    {
        /**
 * @var array
*/
        $fields = $request->validated();

        /**
 * @var Page $page
*/
        $page = Page::findOrFail($id)
            ->load('pageLangs');

        $this->pageService->updatePageLangsContents($page, $fields);

        $pageLangs = $page->pageLangs()
            ->with('contents', 'contents.type')
            ->get();

        return PageLangResource::collection($pageLangs);
    }
}
