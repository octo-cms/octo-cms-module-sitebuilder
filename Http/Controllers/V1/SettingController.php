<?php

namespace OctoCmsModule\Sitebuilder\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use OctoCmsModule\Core\Http\Controllers\V1\SettingController as SettingControllerCore;
use OctoCmsModule\Sitebuilder\Http\Requests\UpdateSiteSettingsRequest;
use OctoCmsModule\Sitebuilder\Interfaces\SettingServiceInterface;

/**
 * Class SettingController
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Sitebuilder\Http\Controllers\V1
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SettingController extends SettingControllerCore
{
    /**
     * SettingServiceInterface
     *
     * @var SettingServiceInterface
     */
    protected $settingService;

    /**
     * SettingController constructor.
     *
     * @param SettingServiceInterface $settingService SettingServiceInterface
     */
    public function __construct(SettingServiceInterface $settingService)
    {
        $this->settingService = $settingService;
    }

    /**
     * Name updateSiteSettings
     *
     * @param UpdateSiteSettingsRequest $request UpdateSiteSettingsRequest
     *
     * @return JsonResponse|object
     */
    public function updateSiteSettings(UpdateSiteSettingsRequest $request)
    {
        $fields = $request->validated();

        $this->settingService->saveSiteSettings($fields);

        return response()->json($this->mapAllSettings())->setStatusCode(Response::HTTP_OK);
    }
}
