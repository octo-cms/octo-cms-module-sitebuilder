<?php

declare(strict_types=1);

namespace OctoCmsModule\Sitebuilder\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Http\Requests\StoreBlockHtmlRequest;
use OctoCmsModule\Sitebuilder\Http\Requests\UploadHtmlMediaRequest;
use OctoCmsModule\Sitebuilder\Interfaces\PictureServiceInterface;
use OctoCmsModule\Sitebuilder\Transformers\BlockHtmlResource;

use function response;

/**
 * @package OctoCmsModule\Sitebuilder\Http\Controllers\V1
 */
class BlockHtmlController extends Controller
{
    protected PictureServiceInterface $pictureService;

    public function __construct(
        PictureServiceInterface $pictureService
    ) {
        $this->pictureService = $pictureService;
    }

    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService): JsonResponse
    {
        /**
 * @var Builder $htmlBlocks
*/
        $htmlBlocks = BlockHtml::query();

        $fields = $request->validated();

        $query = Arr::get($fields, 'query', '');
        if (!empty($query)) {
            $htmlBlocks->where('blade', 'like', '%' . $query . '%')
                ->orWhere('module', 'like', '%' . $query . '%')
                ->orWhere('target', 'like', '%' . $query . '%');
        }

        $fields['orderBy'] = 'blade';
        $datatableDTO = $datatableService->getDatatableDTO($fields, $htmlBlocks);

        $collection = $datatableDTO->builder->get();

        $collection->load('pageLangContents');

        $datatableDTO->collection = BlockHtmlResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * @return JsonResponse|object
     */
    public function store(StoreBlockHtmlRequest $request)
    {
        /**
 * @var array $fields
*/
        $fields = $request->validated();

        $id = Arr::get($fields, 'id', null);

        /**
 * @var BlockHtml $blockHtml
*/
        $blockHtml = !empty($id)
            ? BlockHtml::findOrFail($id)
            : new BlockHtml();

        $blockHtml->blade = Arr::get($fields, 'blade', null);
        $blockHtml->settings = Arr::get($fields, 'settings', null);
        $blockHtml->instructions = Arr::get($fields, 'instructions', null);
        $blockHtml->layout = Arr::get($fields, 'layout', null);
        $blockHtml->src = Arr::get($fields, 'src', null);

        $blockHtml->save();

        return (new BlockHtmlResource($blockHtml))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function uploadMedia(UploadHtmlMediaRequest $request): JsonResponse
    {
        return response()->json($this->pictureService->uploadPicture($request->validated(), BlockHtml::GCS_PATH));
    }
}
