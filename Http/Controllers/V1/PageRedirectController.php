<?php

namespace OctoCmsModule\Sitebuilder\Http\Controllers\V1;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Sitebuilder\Http\Requests\SearchPageLangByQueryRequest;
use OctoCmsModule\Sitebuilder\Http\Requests\StorePageRedirectRequest;
use OctoCmsModule\Sitebuilder\Http\Requests\UpdatePageRedirectRequest;
use OctoCmsModule\Sitebuilder\Interfaces\PageRedirectServiceInterface;
use OctoCmsModule\Sitebuilder\Transformers\PageLangResource;
use OctoCmsModule\Sitebuilder\Transformers\PageRedirectResource;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;

/**
 * Class PageRedirectController
 *
 * @package OctoCmsModule\Sitebuilder\Http\Controllers\V1
 */
class PageRedirectController extends Controller
{
    protected $pageRedirectService;

    /**
     * PageRedirectController constructor.
     *
     * @param PageRedirectServiceInterface $pageRedirectService
     */
    public function __construct(PageRedirectServiceInterface $pageRedirectService)
    {
        $this->pageRedirectService = $pageRedirectService;
    }

    /**
     * @param StorePageRedirectRequest $request
     *
     * @return JsonResponse|object
     */
    public function store(StorePageRedirectRequest $request)
    {
        /**
 * @var array $fields
*/
        $fields = $request->validated();

        /**
 * @var PageLang $pageLang
*/
        $pageLang = PageLang::findOrFail(Arr::get($fields, 'page_lang_id', ''));

        /**
 * @var PageRedirect $pageRedirect
*/
        $pageRedirect = $this->pageRedirectService->storePageRedirect($pageLang, $fields);

        $pageRedirect->load('pageLang');

        return (new PageRedirectResource($pageRedirect))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param  UpdatePageRedirectRequest $request
     * @param  $id
     * @return JsonResponse|object
     */
    public function update(UpdatePageRedirectRequest $request, $id)
    {
        /**
 * @var PageRedirect $pageRedirect
*/
        $pageRedirect = $this->pageRedirectService->updatePageRedirect(
            PageRedirect::findOrFail($id),
            $request->validated()
        );

        $pageRedirect->load('pageLang');

        return (new PageRedirectResource($pageRedirect))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|object
     */
    public function show($id)
    {
        /**
 * @var PageRedirect $pageRedirect
*/
        $pageRedirect = PageRedirect::findOrFail($id);

        $pageRedirect->load('pageLang');

        return (new PageRedirectResource($pageRedirect))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param DatatableRequest          $request
     * @param DatatableServiceInterface $datatableService
     *
     * @return JsonResponse
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        /**
 * @var Builder $pageRedirect
*/
        $pageRedirect = PageRedirect::query()->orderByDesc('updated_at');

        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $pageRedirect);

        $collection = $datatableDTO->builder->get();

        $collection->load('pageLang');

        $datatableDTO->collection = PageRedirectResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|object
     * @throws Exception
     */
    public function delete($id)
    {
        /**
 * @var PageRedirect $pageRedirect
*/
        $pageRedirect = PageRedirect::findOrFail($id);
        $pageRedirect->delete();

        return response()->json()->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    public function searchPageLangByQuery(SearchPageLangByQueryRequest $request)
    {
        /**
 * @var array $fields
*/
        $fields = $request->validated();

        /**
 * @var string $query
*/
        $query = Arr::get($fields, 'query', '');

        /**
 * @var Collection $pageLangs
*/
        $pageLangs = PageLang::where('url', 'like', "%$query%")
            ->orWhere('meta_title', 'like', "%$query%")
            ->take(5)
            ->get();

        return (PageLangResource::collection($pageLangs))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }
}
