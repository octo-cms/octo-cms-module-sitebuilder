<?php

declare(strict_types=1);

namespace OctoCmsModule\Sitebuilder\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\DTO\DatatableDTO;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Slide;
use OctoCmsModule\Sitebuilder\Http\Requests\SaveSlideRequest;
use OctoCmsModule\Sitebuilder\Interfaces\PictureServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\SlideServiceInterface;
use OctoCmsModule\Sitebuilder\Transformers\SlideResource;

use function response;

/**
 * @package OctoCmsModule\Sitebuilder\Http\Controllers\V1
 */
class SlideController extends Controller
{
    protected PictureServiceInterface $pictureService;
    protected SlideServiceInterface $slideService;

    public function __construct(
        SlideServiceInterface $slideService,
        PictureServiceInterface $pictureService
    ) {
        $this->slideService = $slideService;
        $this->pictureService = $pictureService;
    }

    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService): JsonResponse
    {
        /**
         * @var Builder $slides
         */
        $slides = Slide::query();

        $fields = $request->validated();

        $query = Arr::get($fields, 'query', '');
        if (!empty($query)) {
            $slides->where('caption', 'like', '%' . $query . '%')
                   ->orWhere('sub_caption', 'like', '%' . $query . '%');
        }

        /**
         * @var DatatableDTO $datatableDTO
         */
        $datatableDTO = $datatableService->getDatatableDTO($fields, $slides);

        /**
         * @var Collection $collection
         */
        $collection = $datatableDTO->builder->get();

        $collection->load('pictures');

        $datatableDTO->collection = SlideResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * @return JsonResponse|object
     */
    public function store(SaveSlideRequest $request)
    {
        /**
         * @var array $fields
         */
        $fields = $request->validated();

        /**
         * @var Slide $slide
         */
        $slide = $this->slideService->saveSlide(new Slide(), $fields);

        $this->pictureService->savePicturesEntity($slide, Arr::get($fields, 'pictures', []));

        return (new SlideResource($slide))
            ->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function show($id): SlideResource
    {
        /**
         * @var Slide $slide
         */
        $slide = Slide::findOrFail($id);

        $slide->load('pictures');

        return new SlideResource($slide);
    }

    /**
     * @param                  $id
     *
     * @return JsonResponse|object
     */
    public function update(SaveSlideRequest $request, $id)
    {
        /**
         * @var array $fields
         */
        $fields = $request->validated();

        /**
         * @var Slide $slide
         */
        $slide = $this->slideService->saveSlide(Slide::findOrFail($id), $fields);

        $this->pictureService->savePicturesEntity($slide, Arr::get($fields, 'pictures', []));

        return (new SlideResource($slide))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param $id
     *
     * @return JsonResponse|object
     *
     * @throws Exception
     */
    public function delete($id)
    {
        /**
         * @var Slide $slide
         */
        $slide = Slide::findOrFail($id);
        $slide->delete();

        return response()->json()->setStatusCode(Response::HTTP_NO_CONTENT);
    }
}
