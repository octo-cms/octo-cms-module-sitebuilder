<?php

namespace OctoCmsModule\Sitebuilder\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use App\Http\Controllers\Controller;
use OctoCmsModule\Sitebuilder\Http\Requests\StoreFootersRequest;
use OctoCmsModule\Sitebuilder\Interfaces\FooterServiceInterface;
use OctoCmsModule\Sitebuilder\Transformers\BlockEntityResource;
use OctoCmsModule\Sitebuilder\Transformers\BlockHtmlResource;
use OctoCmsModule\Sitebuilder\Transformers\FooterResource;
use OctoCmsModule\Sitebuilder\Transformers\MenuResource;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FooterController
 *
 * @package OctoCmsModule\Sitebuilder\Http\Controllers\V1
 */
class FooterController extends Controller
{

    /**
     * @var FooterServiceInterface
     */
    protected $footerService;

    /**
     * MediaController constructor.
     *
     * @param FooterServiceInterface $footerService
     */
    public function __construct(FooterServiceInterface $footerService)
    {
        $this->footerService = $footerService;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $menus = Footer::all()->sortBy('order');

        $menus->load('contents', 'contents.type');

        return FooterResource::collection($menus);
    }

    /**
     * @param StoreFootersRequest $request
     *
     * @return JsonResponse|object
     */
    public function store(StoreFootersRequest $request)
    {

        $this->footerService->saveFooters($request->validated());

        return response()
            ->json([])
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @return JsonResponse|object
     */
    public function getAllContents()
    {
        return response()->json(
            [
            'blockEntity' => BlockEntityResource::collection(
                BlockEntity::where('target', '=', BlockEntity::TARGET_FOOTER)
                    ->get()
                    ->sortBy('blade')
            ),
            'blockHtml'   => BlockHtmlResource::collection(
                BlockHtml::where('target', '=', BlockHtml::TARGET_FOOTER)
                    ->get()->sortBy('blade')
            ),
            'menus'       => MenuResource::collection(
                Menu::all()->where('name', '!=', Menu::MAIN_MENU)->sortBy('name')
            ),
            ]
        )->setStatusCode(\Illuminate\Http\Response::HTTP_OK);
    }
}
