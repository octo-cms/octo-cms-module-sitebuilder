<?php

namespace OctoCmsModule\Sitebuilder\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Interfaces\LangServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\RouteServiceInterface;

/**
 * Class RouteController
 *
 * @package OctoCmsModule\Sitebuilder\Http\Controllers
 * @author  danielepasi
 */
class RouteController extends Controller
{
    /**
     * @var LangServiceInterface
     */
    protected $langService;
    /**
     * @var RouteServiceInterface
     */
    protected $routeService;

    /**
     * RouteController constructor.
     *
     * @param LangServiceInterface  $langService
     * @param RouteServiceInterface $routeService
     */
    public function __construct(
        LangServiceInterface $langService,
        RouteServiceInterface $routeService
    ) {
        $this->langService = $langService;
        $this->routeService = $routeService;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        /**
         * @var string
         */
        $pathWithoutLang = $this->langService->removeLangPrefixFromPath($request->path());

        /**
         * @var PageLang $pageLang
         */
        $pageLang = $this->routeService->searchPageLang($pathWithoutLang);

        if (empty($pageLang)) {
            abort(404, 'Page not found');
        }

        return view()->first(
            $this->routeService->getViews($pageLang->page),
            $this->routeService->getPageLangData($pageLang)->toArray()
        );
    }
}
