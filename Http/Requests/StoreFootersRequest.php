<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreFootersRequest
 *
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 */
class StoreFootersRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.content_align'           => 'required',
            '*.enabled'                 => 'required',
            '*.id'                      => 'present|nullable',
            '*.name'                    => 'required',
            '*.order'                   => 'required',
            '*.contents'                => 'sometimes|array',
            '*.contents.*.content_id'   => 'sometimes|nullable',
            '*.contents.*.content_type' => 'sometimes',
            '*.contents.*.data'         => 'sometimes',
            '*.contents.*.targets'      => 'sometimes',
            '*.contents.*.footer_id'    => 'sometimes',
            '*.contents.*.order'        => 'sometimes',
            '*.contents.*.size'         => 'sometimes',
            '*.contents.*.layout'       => 'sometimes',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.content_align.required' => __('sitebuilder::validation.*.content_align.required'),
            '*.enabled.required'       => __('sitebuilder::validation.*.enabled.required'),
            '*.name.required'          => __('sitebuilder::validation.*.name.required'),
            '*.order.required'         => __('sitebuilder::validation.*.order.required'),
        ];
    }
}
