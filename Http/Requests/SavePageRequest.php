<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class SavePageRequest
 *
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 */
class SavePageRequest extends FormRequest
{
    use SavePictureRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return Arr::collapse([
            [
                'change_freq'                  => 'required',
                'name'                         => 'required',
                'pageLangs'                    => 'required|array',
                'published'                    => 'required|boolean',
                'sitemap'                      => 'required|boolean',
                'sitemap_priority'             => 'required',
                'type'                         => 'sometimes',
                'pageLangs.*.id'               => 'sometimes|nullable',
                'pageLangs.*.lang'             => 'required',
                'pageLangs.*.meta_description' => 'sometimes|nullable',
                'pageLangs.*.meta_title'       => 'sometimes|nullable',
                'pageLangs.*.template'         => 'sometimes|nullable',
                'pageLangs.*.url'              => 'sometimes|nullable',
            ],
            $this->getPictureRules(),
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'change_freq.required'      => __('sitebuilder::validation.change_freq.required'),
            'name.required'             => __('sitebuilder::validation.name.required'),
            'pageLangs.required'        => __('sitebuilder::validation.pageLangs.required'),
            'pageLangs.*.lang.required' => __('sitebuilder::validation.pageLangs.*.lang.required'),
            'published.required'        => __('sitebuilder::validation.published.required'),
            'sitemap.required'          => __('sitebuilder::validation.sitemap.required'),
            'sitemap_priority.required' => __('sitebuilder::validation.sitemap_priority.required'),
            'name.string'               => __('sitebuilder::validation.name.string'),
        ];
    }
}
