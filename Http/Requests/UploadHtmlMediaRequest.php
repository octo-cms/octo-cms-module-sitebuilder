<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UploadHtmlMediaRequest
 *
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 */
class UploadHtmlMediaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'src'    => 'required',
            'height' => 'sometimes',
            'width'  => 'sometimes',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
