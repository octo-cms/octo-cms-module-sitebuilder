<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateMenuRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class UpdateMenuRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menuLangs'         => 'required|array',
            'menuLangs.*.title' => 'present|nullable',
            'menuLangs.*.lang'  => 'present|string',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'menuLangs.required'      => __('sitebuilder::validation.menuLangs.required'),
            'menuLangs.*.lang.string' => __('sitebuilder::validation.menuLangs.*.lang.string'),
        ];
    }
}
