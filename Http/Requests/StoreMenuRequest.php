<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreMenuRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class StoreMenuRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|string',
            'blade'             => 'required|string',
            'menuLangs'         => 'required|array',
            'menuLangs.*.title' => 'present|nullable',
            'menuLangs.*.lang'  => 'present|string',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'           => __('sitebuilder::validation.name.required'),
            'name.string'             => __('sitebuilder::validation.name.string'),
            'blade.required'          => __('sitebuilder::validation.blade.required'),
            'blade.string'            => __('sitebuilder::validation.blade.string'),
            'menuLangs.required'      => __('sitebuilder::validation.menuLangs.required'),
            'menuLangs.*.lang.string' => __('sitebuilder::validation.menuLangs.*.lang.string'),
        ];
    }
}
