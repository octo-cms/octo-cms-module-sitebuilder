<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreBlockHtmlRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class StoreBlockHtmlRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'              => 'sometimes',
            'blade'           => 'required|string',
            'src'             => 'sometimes',
            'instructions'    => 'sometimes|string',
            'settings'        => 'sometimes|array',
            'settings.*.type' => 'sometimes|string',
            'settings.*.name' => 'sometimes|string',
            'layout'          => 'sometimes|array',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'blade.required'         => __('sitebuilder::validation.blade.required'),
            'blade.string'           => __('sitebuilder::validation.blade.string'),
            'instructions.string'    => __('sitebuilder::validation.instructions.string'),
            'settings.*.name.string' => __('sitebuilder::validation.settings.*.name.string'),
        ];
    }
}
