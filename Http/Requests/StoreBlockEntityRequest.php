<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreBlockEntityRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class StoreBlockEntityRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                    => 'sometimes',
            'module'                => 'required|string',
            'blade'                 => 'required|string',
            'entity'                => 'required|string',
            'src'                   => 'sometimes',
            'instructions'          => 'sometimes|string',
            'settings'              => 'sometimes|array',
            'settings.*.name'       => 'sometimes|string',
            'settings.*.text'       => 'sometimes|string',
            'settings.*.background' => 'sometimes|image',
            'layout'                => 'sometimes|array',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'blade.required'              => __('sitebuilder::validation.blade.required'),
            'blade.string'                => __('sitebuilder::validation.blade.string'),
            'module.required'             => __('sitebuilder::validation.module.required'),
            'module.string'               => __('sitebuilder::validation.module.string'),
            'entity.required'             => __('sitebuilder::validation.entity.required'),
            'entity.string'               => __('sitebuilder::validation.entity.string'),
            'instructions.string'         => __('sitebuilder::validation.instructions.string'),
            'settings.*.name.string'      => __('sitebuilder::validation.settings.*.name.string'),
        ];
    }
}
