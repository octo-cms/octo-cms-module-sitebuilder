<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreMenuItemsRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class StoreMenuItemsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.id'                    => 'present',
            '*.data'                  => 'present|array',
            '*.menuItemLangs'         => 'required|array',
            '*.order'                 => 'required',
            '*.parent_id'             => 'present',
            '*.type'                  => 'required',
            '*.menuItemLangs.*.lang'  => 'required',
            '*.menuItemLangs.*.label' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.menuItemLangs.required'         => __('sitebuilder::validation.*.menuItemLangs.required'),
            '*.order.required'                 => __('sitebuilder::validation.*.order.required'),
            '*.type.required'                  => __('sitebuilder::validation.*.type.required'),
            '*.menuItemLangs.*.lang.required'  => __('sitebuilder::validation.*.menuItemLangs.*.lang.required'),
            '*.menuItemLangs.*.label.required' => __('sitebuilder::validation.*.menuItemLangs.*.label.required'),
        ];
    }
}
