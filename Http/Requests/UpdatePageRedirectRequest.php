<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePageRedirectRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class UpdatePageRedirectRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_url'      => 'required|string',
            'page_lang_id' => 'sometimes|int',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'old_url.required' => __('sitebuilder::validation.old_url.required'),
            'old_url.string'   => __('sitebuilder::validation.old_url.string'),
            'page_lang_id.int' => __('sitebuilder::validation.page_lang_id.int'),
        ];
    }
}
