<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class SaveSlideRequest
 *
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 */
class SaveSlideRequest extends FormRequest
{
    use SavePictureRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Arr::collapse([
            [
                'caption'      => 'sometimes',
                'sub_caption'  => 'sometimes',
                'action_label' => 'sometimes',
                'action_link'  => 'sometimes',
            ],
            $this->getPictureRules(),
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
