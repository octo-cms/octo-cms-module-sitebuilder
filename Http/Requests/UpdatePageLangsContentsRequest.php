<?php

namespace OctoCmsModule\Sitebuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePageLangsContentsRequest
 *
 * @package OctoCmsModule\Sitebuilder\Http\Requests
 */
class UpdatePageLangsContentsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.id'                      => 'sometimes|nullable',
            '*.lang'                    => 'sometimes|nullable',
            '*.contents'                => 'sometimes|array',
            '*.contents.*.content_id'   => 'sometimes|nullable',
            '*.contents.*.content_type' => 'sometimes|nullable',
            '*.contents.*.data'         => 'sometimes|nullable|array',
            '*.contents.*.id'           => 'sometimes|nullable',
            '*.contents.*.position'     => 'required',
            '*.contents.*.type'         => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
