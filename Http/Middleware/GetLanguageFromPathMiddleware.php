<?php

namespace OctoCmsModule\Sitebuilder\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use OctoCmsModule\Sitebuilder\Interfaces\LangServiceInterface;

/**
 * Class GetLanguageFromPathMiddleware
 * @package OctoCmsModule\Core\Http\Middleware
 */
class GetLanguageFromPathMiddleware
{

    protected $langService;

    /**
     * GetLanguageFromPathMiddleware constructor.
     *
     * @param LangServiceInterface $langService
     */
    public function __construct(LangServiceInterface $langService)
    {
        $this->langService = $langService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $currentLanguage = $this->langService->getLangFromPath($request->path());

        app()->setLocale($currentLanguage);

        return $next($request);
    }
}
