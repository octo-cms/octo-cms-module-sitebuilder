<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\PageRedirectService;

use OctoCmsModule\Sitebuilder\Services\PageRedirectService;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StorePageRedirectTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\PageRedirectService
 */
class StorePageRedirectTest extends TestCase
{



    public function test_storePageRedirect()
    {
        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()->create();

        $fields = [
            'old_url'      => 'https://octopus.com',
            'page_lang_id' => $pageLang->id,
        ];

        /** @var PageRedirectService $pageRedirectService */
        $pageRedirectService = new PageRedirectService();

        /** @var PageRedirect $pageRedirect */
        $pageRedirect = $pageRedirectService->storePageRedirect($pageLang, $fields);

        $this->assertInstanceOf(PageRedirect::class, $pageRedirect);

        $this->assertDatabaseHas('page_redirects', $fields);
    }
}
