<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\PageRedirectService;

use OctoCmsModule\Sitebuilder\Services\PageRedirectService;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class UpdatePageRedirectTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\PageRedirectService
 */
class UpdatePageRedirectTest extends TestCase
{


    public function test_updatePageRedirect()
    {
        /** @var PageRedirect $pageRedirect */
        $pageRedirect = PageRedirect::factory()->create([
            'old_url' => 'path',
        ]);

        /** @var PageLang $pageLangToAssign */
        $pageLangToAssign = PageLang::factory()->create();

        $fields = [
            'old_url'      => 'updated_path',
            'page_lang_id' => $pageLangToAssign->id,
        ];

        /** @var PageRedirectService $pageRedirectService */
        $pageRedirectService = new PageRedirectService();

        $pageRedirectService->updatePageRedirect($pageRedirect, $fields);

        $this->assertDatabaseHas('page_redirects', [
            'id'           => $pageRedirect->id,
            'old_url'      => $fields['old_url'],
            'page_lang_id' => $pageLangToAssign->id,
        ]);
    }
}
