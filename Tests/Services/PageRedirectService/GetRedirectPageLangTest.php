<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\PageRedirectService;

use OctoCmsModule\Sitebuilder\Services\PageRedirectService;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetRedirectPageLangTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\PageRedirectService
 */
class GetRedirectPageLangTest extends TestCase
{



    public function test_getRedirectPageLang()
    {
        /** @var PageRedirect $pageRedirect */
        $pageRedirect = PageRedirect::factory()->create();

        /** @var PageRedirectService $pageRedirectService */
        $pageRedirectService = new PageRedirectService();

        $this->assertEquals(
            1,
            $pageRedirectService->getRedirectPageLang($pageRedirect->old_url)->id
        );
    }

    public function test_OldUrlDoesNotExist()
    {
        /** @var PageRedirectService $pageRedirectService */
        $pageRedirectService = new PageRedirectService();

        $this->assertNull($pageRedirectService->getRedirectPageLang('old_url'));
    }
}
