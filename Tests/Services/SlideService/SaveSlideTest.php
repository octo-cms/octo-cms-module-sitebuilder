<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\SlideService;

use OctoCmsModule\Sitebuilder\Services\SlideService;
use OctoCmsModule\Sitebuilder\Entities\Slide;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SaveSlideTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\SlideService
 */
class SaveSlideTest extends TestCase
{


    public function test_storeSlide()
    {
        /** @var array $fields */
        $fields = [
            'caption'      => 'caption',
            'sub_caption'  => 'sub_caption',
            'action_label' => 'label',
            'action_link'  => 'link',
        ];

        /** @var SlideService $slideService */
        $slideService = new SlideService();

        $slideService->saveSlide(new Slide(), $fields);

        $this->assertDatabaseHas('slides', [
            'id'           => 1,
            'caption'      => $fields['caption'],
            'sub_caption'  => $fields['sub_caption'],
            'action_label' => $fields['action_label'],
            'action_link'  => $fields['action_link'],
        ]);
    }

    public function test_updateSlide()
    {
        /** @var Slide $slide */
        $slide = Slide::factory()->create([
            'caption'      => 'caption',
            'sub_caption'  => 'sub_caption',
            'action_label' => 'label',
            'action_link'  => 'link',
        ])->first();

        /** @var array $fields */
        $fields = [
            'caption'      => 'new caption',
            'sub_caption'  => 'new sub_caption',
            'action_label' => 'new label',
            'action_link'  => 'new link',
        ];

        /** @var SlideService $slideService */
        $slideService = new SlideService();

        $slideService->saveSlide($slide, $fields);

        $this->assertDatabaseHas('slides', [
            'id'           => 1,
            'caption'      => $fields['caption'],
            'sub_caption'  => $fields['sub_caption'],
            'action_label' => $fields['action_label'],
            'action_link'  => $fields['action_link'],
        ]);
    }
}
