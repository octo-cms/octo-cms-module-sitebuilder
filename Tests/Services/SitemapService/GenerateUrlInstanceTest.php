<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\SitemapService;

use Illuminate\Database\Eloquent\Factories\Sequence;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Services\SitemapService;
use Spatie\Sitemap\Tags\Url;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GenerateUrlInstanceTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\SitemapService
 */
class GenerateUrlInstanceTest extends TestCase
{


    public function test_generateUrlInstanceNoAlternates()
    {
        /** @var Page $page */
        $page = Page::factory()
            ->has(PageLang::factory()->count(2)->state(new Sequence(['lang' => 'en'], ['lang' => 'de'])))
            ->create([
                'change_freq'      => 'daily',
                'sitemap_priority' => 0.5,
            ]);

        /** @var PageLang $defaultPageLang */
        $defaultPageLang = PageLang::factory()->create([
            'page_id' => $page->id,
            'url'     => 'page_lang_url',
            'lang'    => 'it',
        ]);

        /** @var SitemapService $sitemapService */
        $sitemapService = new SitemapService();

        /** @var Url $url */
        $url = $sitemapService->generateUrlInstance($page, 'it', ['it', 'en', 'de']);

        $this->assertEquals(
            $defaultPageLang->url,
            $url->url
        );

        $this->assertEquals(
            $defaultPageLang->updated_at,
            $url->lastModificationDate
        );

        $this->assertEquals(
            $page->sitemap_priority,
            $url->priority
        );

        $this->assertEquals(
            $page->change_freq,
            $url->changeFrequency
        );

        $this->assertCount(2, $url->alternates);
    }

    public function test_generateNullUrl()
    {
        /** @var SitemapService $sitemapService */
        $sitemapService = new SitemapService();

        $this->assertNull(
            $sitemapService->generateUrlInstance(
                Page::factory()->create(),
                'it',
                ['it', 'en', 'de'])
        );
    }
}
