<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\SettingService;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Sitebuilder\Services\SettingService;

/**
 * Class SaveSiteSettingsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\SettingService
 */
class SaveSiteSettingsTest extends TestCase
{


    public function test_storeSiteSettings()
    {
        Sanctum::actingAs(self::createAdminUser());

        $settings = [
            [
                'name'  => 'name-1',
                'value' => 'value-1',
            ],
            [
                'name'  => 'fe_logo',
                'value' => 'value-2',
            ],
        ];

        /** @var SettingService $settingService */
        $settingService = new SettingService();

        $settingService->saveSiteSettings($settings);

        foreach ($settings as $setting) {
            $this->assertDatabaseHas('settings', [
                'name'  => $setting['name'],
                'value' => serialize($setting['value']),
            ]);
        }
    }

    public function test_updateSiteSettings()
    {
        Sanctum::actingAs(self::createAdminUser());

        Setting::factory()->create([
            'name'  => 'name-1',
            'value' => 'value-1',
        ]);

        Setting::factory()->create([
            'name'  => 'fe_logo',
            'value' => 'value-2',
        ]);

        $settings = [
            [
                'name'  => 'name-1',
                'value' => 'updated-value-1',
            ],
            [
                'name'  => 'fe_logo',
                'value' => 'updated-value-2',
            ],
        ];

        /** @var SettingService $settingService */
        $settingService = new SettingService();

        $settingService->saveSiteSettings($settings);

        foreach ($settings as $setting) {
            $this->assertDatabaseHas('settings', [
                'name'  => $setting['name'],
                'value' => serialize($setting['value']),
            ]);
        }
    }
}
