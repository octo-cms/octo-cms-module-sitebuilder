<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\SettingService;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Sitebuilder\Services\SettingService;
use ReflectionException;

/**
 * Class saveSiteFaviconTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\SettingService
 */
class SaveSiteFaviconTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_storeSiteFavicon()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var array $faviconSettings */
        $faviconSettings = [
            'src'  => 'src',
            'webp' => 'webp',
        ];

        /** @var SettingService $settingService */
        $settingService = new SettingService();

        $this->invokeMethod(
            $settingService,
            'saveSiteFavicon',
            ['faviconSettings' => $faviconSettings]
        );

        $this->assertDatabaseHas('settings', [
            'name'  => SettingNameConst::FE_FAVICON,
            'value' => serialize($faviconSettings),
        ]);
    }

    /**
     * @throws ReflectionException
     */
    public function test_updateSiteFavicon()
    {
        Sanctum::actingAs(self::createAdminUser());

        Setting::factory()->create([
            'name'  => SettingNameConst::FE_FAVICON,
            'value' => [
                'src'  => 'src',
                'webp' => 'webp',
            ],
        ]);

        /** @var array $faviconSettings */
        $faviconSettings = [
            'src'  => 'src-updated',
            'webp' => 'webp-updated',
        ];

        /** @var SettingService $settingService */
        $settingService = new SettingService();

        $this->invokeMethod(
            $settingService,
            'saveSiteFavicon',
            ['faviconSettings' => $faviconSettings]
        );

        $this->assertDatabaseHas('settings', [
            'name'  => SettingNameConst::FE_FAVICON,
            'value' => serialize($faviconSettings),
        ]);
    }

    /**
     * @throws ReflectionException
     */
    public function test_storeSiteFaviconData()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var array $ */
        $faviconData = [
            'size57x75'  => 'image1',
            'size60x60'  => 'image2',
        ];

        /** @var SettingService $settingService */
        $settingService = new SettingService();

        $this->invokeMethod(
            $settingService,
            'saveSiteFaviconData',
            ['faviconData' => $faviconData]
        );

        $this->assertDatabaseHas('settings', [
            'name'  => SettingNameConst::FE_FAVICON_DATA,
            'value' => serialize($faviconData),
        ]);
    }

    /**
     * @throws ReflectionException
     */
    public function test_updateSiteFaviconData()
    {
        Sanctum::actingAs(self::createAdminUser());

        Setting::factory()->create([
            'name'  => SettingNameConst::FE_FAVICON_DATA,
            'value' => [
                'size57x75'  => 'image1',
                'size60x60'  => 'image2',
            ],
        ]);

        /** @var array $faviconData */
        $faviconData = [
            'size57x75'  => 'image3',
            'size60x60'  => 'image4',
        ];

        /** @var SettingService $settingService */
        $settingService = new SettingService();

        $this->invokeMethod(
            $settingService,
            'saveSiteFaviconData',
            ['faviconSettings' => $faviconData]
        );

        $this->assertDatabaseHas('settings', [
            'name'  => SettingNameConst::FE_FAVICON_DATA,
            'value' => serialize($faviconData),
        ]);
    }
}
