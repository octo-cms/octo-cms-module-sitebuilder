<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\FavIconService;

use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Jobs\LoggingJob;
use OctoCmsModule\Sitebuilder\Services\FavIconService;
use OctoCmsModule\Sitebuilder\Services\PictureService;
use OctoCmsModule\Sitebuilder\Services\SettingService;
use OctoCmsModule\Sitebuilder\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CreateFavIconsFromFileTest
 * @package OctoCmsModule\Sitebuilder\Tests\Services\FavIconService
 */
class CreateFavIconsFromFileTest extends TestCase
{


    const FAKE_IMAGE_NAME = "test.jpg";

    private function createFakeImage()
    {
        $img = Image::canvas(800, 600, '#ccc');

        $img->circle(500, 400, 300, function ($draw) {
            $draw->background('#ffff00');
        });

        $img->circle(300, 400, 300, function ($draw) {
            $draw->background('#0000ff');
        });

        $img->save(storage_path('temp') . '/' . self::FAKE_IMAGE_NAME);
    }

    private function removeFakeImage()
    {
        $fn = storage_path('temp') . '/' . self::FAKE_IMAGE_NAME;
        if (file_exists($fn)) {
            unlink($fn);
        }
    }

    public function test_createFavIconFileNotFound()
    {
        Queue::fake();

        $this->removeFakeImage();
        $service = new FavIconService(new SettingServiceMock());
        $res = $service->createFromFile(storage_path('temp') . "/" . self::FAKE_IMAGE_NAME);
        $this->assertTrue(!$res);

        Queue::assertPushed(LoggingJob::class, function ($job) {
            return true;
        });
    }


    public function test_createFavIcon()
    {
        Storage::fake('gcs');
        Queue::fake();

        $this->createFakeImage();

        $gcsPath =PictureService::BUCKET_BASE_URL.'/' . env('GOOGLE_CLOUD_STORAGE_BUCKET');

        $urls['size57x57'] = $gcsPath . '/favicons/57-favicon.png';
        $urls['size60x60'] = $gcsPath . '/favicons/60-favicon.png';
        $urls['size72x72'] = $gcsPath . '/favicons/72-favicon.png';
        $urls['size76x76'] = $gcsPath . '/favicons/76-favicon.png';
        $urls['size114x114'] = $gcsPath . '/favicons/114-favicon.png';
        $urls['size120x120'] = $gcsPath . '/favicons/120-favicon.png';
        $urls['size144x144'] = $gcsPath . '/favicons/144-favicon.png';
        $urls['size152x152'] = $gcsPath . '/favicons/152-favicon.png';
        $urls['size180x180'] = $gcsPath . '/favicons/180-favicon.png';
        $urls['size192x192'] = $gcsPath . '/favicons/192-favicon.png';
        $urls['size32x32'] = $gcsPath . '/favicons/32-favicon.png';
        $urls['size96x96'] = $gcsPath . '/favicons/96-favicon.png';
        $urls['size16x16'] = $gcsPath . '/favicons/16-favicon.png';
        $urls['ico'] = $gcsPath . '/favicons/favicon.ico';
        $urls['manifest'] = $gcsPath . '/favicons/manifest.json';

        $service = new FavIconService(new SettingService());
        $res = $service->createFromFile(storage_path('temp') . "/" . self::FAKE_IMAGE_NAME);

        $files = Storage::disk('gcs')->files('favicons');

        $this->assertCount(count($service->getSizes()) + 2, $files);
        $this->assertTrue($res);

        Queue::assertPushed(LoggingJob::class, function ($job) {
            return true;
        });

        $this->assertDatabaseHas('settings', [
            'name'  => SettingNameConst::FE_FAVICON_DATA,
            'value' => serialize($urls),
        ]);

        $this->removeFakeImage();

    }
}
