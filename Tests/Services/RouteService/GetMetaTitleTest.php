<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\RouteService;

use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Sitebuilder\Services\RouteService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetMetaTitleTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\RouteService
 */
class GetMetaTitleTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_getMetaTitle()
    {
        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()->create([
            'meta_title' => 'meta_title',
        ]);

        /** @var RouteService $routeService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertEquals(
            $pageLang->meta_title,
            $this->invokeMethod(
                $routeService,
                'getMetaTitle',
                [$pageLang]
            )
        );
    }

    /**
     * @throws ReflectionException
     */
    public function test_metaTitleEmpty()
    {
        /** @var Setting $setting */
        $setting = Setting::factory()->create([
            'name'  => 'fe_title',
            'value' => 'title',
        ]);

        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()->create([
            'meta_title' => null,
        ]);

        /** @var RouteService $routeService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertEquals(
            $setting->value,
            $this->invokeMethod(
                $routeService,
                'getMetaTitle',
                [$pageLang]
            )
        );
    }
}
