<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\RouteService;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Services\RouteService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetPageLangDataTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\RouteService
 */
class GetPageLangDataTest extends TestCase
{


    public function test_getPageLangData()
    {
        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()->create();

        /** @var RouteService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertEquals(
            [
                'template'         => strtolower(config('octo-cms.template.module')),
                'pageLang'         => $pageLang,
                'contents'         => new Collection(),
                'metaTitle'        => $pageLang->meta_title,
                'metaDescription'  => $pageLang->meta_description,
                'gtmSettings'      => null,
                'logo'             => null,
                'footers'          => new Collection(),
                'optimizeSettings' => null,
                'faviconsData'     => null,
            ],
            $routeService->getPageLangData($pageLang)->toArray()
        );
    }
}
