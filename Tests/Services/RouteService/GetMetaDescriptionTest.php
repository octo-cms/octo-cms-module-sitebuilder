<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\RouteService;

use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Sitebuilder\Services\RouteService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetMetaDescriptionTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\RouteService
 */
class GetMetaDescriptionTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_getMetaDescription()
    {
        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()->create([
            'meta_description' => 'meta_description',
        ]);

        /** @var RouteService $routeService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertEquals(
            $pageLang->meta_description,
            $this->invokeMethod(
                $routeService,
                'getMetaDescription',
                [$pageLang]
            )
        );
    }

    /**
     * @throws ReflectionException
     */
    public function test_metaDescriptionEmpty()
    {
        /** @var Setting $setting */
        $setting = Setting::factory()->create([
            'name'  => 'fe_description',
            'value' => 'description',
        ]);

        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()->create([
            'meta_description' => null,
        ]);

        /** @var RouteService $routeService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertEquals(
            $setting->value,
            $this->invokeMethod(
                $routeService,
                'getMetaDescription',
                [$pageLang]
            )
        );
    }
}
