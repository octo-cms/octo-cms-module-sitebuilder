<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\RouteService;

use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Services\RouteService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetViewsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\RouteService
 */
class GetViewsTest extends TestCase
{


    public function test_getPageViewBlog()
    {

        /** @var Page $page */
        $page = Page::factory()->create([
            'type' => Page::TYPE_NEWS,
        ]);

        /** @var RouteService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertEquals(
            [
                "blog.news",
                strtolower(config('octo-cms.template.module')) . "::blog.news",
                strtolower(config('octo-cms.blog.module')) . "::blog.news",
                "sitebuilder::blog.news",
            ],
            $routeService->getViews($page)
        );
    }

    public function test_getPageViewCatalog()
    {

        /** @var Page $page */
        $page = Page::factory()->create([
            'type' => Page::TYPE_CATALOG,
        ]);

        /** @var RouteService $routeService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertEquals(
            [
                "catalog.catalog",
                strtolower(config('octo-cms.template.module')) . "::catalog.catalog",
                strtolower(config('octo-cms.catalog.module')) . "::catalog.catalog",
                "sitebuilder::catalog.catalog",
            ],
            $routeService->getViews($page)
        );
    }

    public function test_getPageStandardViewTest()
    {
        $template = strtolower(config('octo-cms.template.module'));
        $module = 'sitebuilder';
        $view = 'index';

        $viewArray = [
            "$view",
            "$template::$view",
            "$module::$view",
            "sitebuilder::$view",
        ];

        /** @var Page $page */
        $page = Page::factory()->create([
            'type' => Page::TYPE_STANDARD,
        ]);

        /** @var RouteService $routeService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertEquals(
            $viewArray,
            $routeService->getViews($page)
        );
    }
}
