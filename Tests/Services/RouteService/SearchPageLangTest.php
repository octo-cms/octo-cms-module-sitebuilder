<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\RouteService;

use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Services\RouteService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SearchPageLangTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\RouteService
 */
class SearchPageLangTest extends TestCase
{


    public function test_searchPageLangTest()
    {
        $path = 'octopus.it';

        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()->create([
            'lang' => 'it',
            'url'  => $path,
        ]);

        /** @var RouteService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        /** @var Page $pageLangFromService */
        $pageLangFromService = $routeService->searchPageLang($path);

        $this->assertEquals(
            $pageLang->id,
            $pageLangFromService->id
        );
    }

    public function test_searchNullPageLangTest()
    {
        /** @var RouteService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        $this->assertNull(
            $routeService->searchPageLang('octopus.it')
        );
    }

    public function test_searchPageLangWithHomepageAndEmptyPath()
    {
        /** @var Page $page */
        Page::factory()
            ->has(PageLang::factory()->state(['lang' => 'it', 'url' => null]))
            ->create(['type' => Page::TYPE_HOMEPAGE,]);

        /** @var RouteService */
        $routeService = new RouteService(
            new SettingServiceMock()
        );

        /** @var PageLang $pageLangFromService */
        $pageLangFromService = $routeService->searchPageLang('');

        $this->assertEquals(
            1,
            $pageLangFromService->id
        );
    }
}
