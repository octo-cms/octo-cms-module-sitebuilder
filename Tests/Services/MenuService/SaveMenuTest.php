<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\MenuService;

use OctoCmsModule\Sitebuilder\Services\MenuService;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SaveMenuTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\MenuService
 */
class SaveMenuTest extends TestCase
{


    public function test_storeMenuLangs()
    {
        /** @var array $data */
        $data = [
            'menuLangs' => [
                [
                    'lang'  => 'it',
                    'title' => 'title it',
                ],
                [
                    'lang'  => 'en',
                    'title' => 'title en',
                ],
            ],
        ];
        /** @var Menu $menu */
        $menu = Menu::factory()->create()->first();

        /** @var MenuService $menuService */
        $menuService = new MenuService();

        $menuService->saveMenu($menu, $data);

        foreach ($data['menuLangs'] as $menuLang) {
            $this->assertDatabaseHas('menu_langs', [
                'menu_id' => $menu->id,
                'lang'    => $menuLang['lang'],
                'title'   => $menuLang['title'],
            ]);
        }
    }

    public function test_updateMenuLangs()
    {
        /** @var array $data */
        $data = [
            'menuLangs' => [
                [
                    'lang'  => 'it',
                    'title' => 'updated title it',
                ],
                [
                    'lang'  => 'en',
                    'title' => 'updated title en',
                ],
            ],
        ];

        /** @var Menu $menu */
        $menu = Menu::factory()
            ->has(MenuLang::factory()->state(['title' => 'title_it', 'lang' => 'it']))
            ->has(MenuLang::factory()->state(['title' => 'title_en', 'lang' => 'en']))
            ->create();

        /** @var MenuService $menuService */
        $menuService = new MenuService();

        $menuService->saveMenu($menu, $data);

        foreach ($data['menuLangs'] as $menuLang) {
            $this->assertDatabaseHas('menu_langs', [
                'menu_id' => $menu->id,
                'lang'    => $menuLang['lang'],
                'title'   => $menuLang['title'],
            ]);
        }
    }
}
