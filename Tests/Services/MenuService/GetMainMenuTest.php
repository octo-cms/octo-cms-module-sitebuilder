<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\MenuService;


use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Services\MenuService;
use OctoCmsModule\Core\Tests\TestCase;
use Faker\Factory as Faker;


class GetMainMenuTest extends TestCase
{


    public function test_getMainMenuTest()
    {
        $faker = Faker::create();

        /** @var Page $page */
        $page = Page::factory()->create([
            'name' => 'homepage',
            'type' => Page::TYPE_HOMEPAGE,
        ]);

        /** @var Menu $menu */
        $menu = Menu::factory()->create([
            'name' => Menu::MAIN_MENU
        ]);

        // First level

        $m1 = MenuItem::factory()->create([
            'menu_id' => $menu->id,
            'type'    => MenuItem::TYPE_EXTERNAL,
            'data'    => ['link' => $faker->url],
        ]);

        $m2 = MenuItem::factory()->create([
            'menu_id' => $menu->id,
            'type'    => MenuItem::TYPE_EXTERNAL,
            'data'    => ['link' => $faker->url],
        ]);

        // Second Level

        $m3 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m1->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ]);

        $m4 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m1->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ]);

        $m5 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m2->id,
            'type'      => MenuItem::TYPE_PAGE,
            'data'      => ['page' => ['value' => $page->id]],
        ]);

        $m6 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m2->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ]);

        // Third level

        $m7 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m6->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ]);

        $m8 = MenuItem::factory()->create([
            'menu_id'   => $menu->id,
            'parent_id' => $m6->id,
            'type'      => MenuItem::TYPE_PAGE,
            'data'      => ['page' => ['value' => $page->id]],
        ]);

        /** @var MenuService $menuService */
        $menuService = new MenuService();

        $result = $menuService->getMainMenu();

        $this->assertIsArray($result);

        $items = $result['items'];

        $this->assertCount(2, $items);
        $this->assertCount(2, $items[0]->children);
        $this->assertCount(2, $items[1]->children);
        $this->assertCount(2, $items[1]->children[1]->children);
    }
}
