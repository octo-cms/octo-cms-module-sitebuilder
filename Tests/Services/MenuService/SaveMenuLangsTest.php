<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\MenuService;

use OctoCmsModule\Sitebuilder\Services\MenuService;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuLang;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SaveMenuLangsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services
 */
class SaveMenuLangsTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_storeMenuLangs()
    {
        /** @var array $menuLangs */
        $menuLangs = [
            [
                'lang'  => 'it',
                'title' => 'title it',
            ],
            [
                'lang'  => 'en',
                'title' => 'title en',
            ],
        ];

        /** @var Menu $menu */
        $menu = Menu::factory()->create();

        /** @var MenuService $menuService */
        $menuService = new MenuService();

        $this->invokeMethod(
            $menuService,
            'saveMenuLangs',
            [$menu, $menuLangs]
        );

        foreach ($menuLangs as $menuLang) {
            $this->assertDatabaseHas('menu_langs', [
                'menu_id' => $menu->id,
                'lang'    => $menuLang['lang'],
                'title'   => $menuLang['title'],
            ]);
        }
    }

    /**
     * @throws ReflectionException
     */
    public function test_updateMenuLangs()
    {
        /** @var array $menuLangs */
        $menuLangs = [
            [
                'lang'  => 'it',
                'title' => 'new title it',
            ],
            [
                'lang'  => 'en',
                'title' => 'new title en',
            ],
        ];

        /** @var Menu $menu */
        $menu = Menu::factory()
            ->has(MenuLang::factory()->state(['title' => 'title_it', 'lang' => 'it']))
            ->has(MenuLang::factory()->state(['title' => 'title_en', 'lang' => 'en']))
            ->create();

        /** @var MenuService $menuService */
        $menuService = new MenuService();

        $this->invokeMethod(
            $menuService,
            'saveMenuLangs',
            [$menu, $menuLangs]
        );

        foreach ($menuLangs as $menuLang) {
            $this->assertDatabaseHas('menu_langs', [
                'menu_id' => $menu->id,
                'lang'    => $menuLang['lang'],
                'title'   => $menuLang['title'],
            ]);
        }
    }
}
