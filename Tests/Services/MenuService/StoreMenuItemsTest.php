<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\MenuService;

use OctoCmsModule\Sitebuilder\Entities\Page;
use ReflectionException;
use OctoCmsModule\Sitebuilder\Services\MenuService;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreMenuItemsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\MenuService
 */
class StoreMenuItemsTest extends TestCase
{


    /** @var Menu $menu */
    protected $menu;

    public function setUp(): void
    {
        parent::setUp();

        $this->menu = Menu::factory()->has(MenuItem::factory()->count(4), 'items')->create();

    }

    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];
        $providers[] = [
            [['id' => 1], ['id' => 2], ['id' => 3]],
            [1, 2, 3],
        ];

        $providers[] = [
            [['id' => 1], ['id' => 2], ['id' => 3], ['id' => 4]],
            [1, 2, 3, 4],
        ];

        $providers[] = [
            [],
            [],
        ];

        $providers[] = [
            [['id' => 1], ['id' => 2], ['id' => 3], ['id' => 4], ['id' => null]],
            [1, 2, 3, 4, 5],
        ];

        $providers[] = [
            [['id' => 1], ['id' => 3], ['id' => 4], ['id' => null]],
            [1, 3, 4, 5],
        ];

        return $providers;

    }

    /**
     * @param $fields
     * @param $results
     *
     * @throws ReflectionException
     * @dataProvider dataProvider
     */
    public function test_storeMenuItems($fields, $results)
    {

        /** @var MenuService $service */
        $service = new MenuService();

        /** @var Menu $menuUpdated */
        $menuUpdated = $this->invokeMethod(
            $service,
            'storeMenuItems',
            [
                'menu'   => $this->menu,
                'fields' => $fields,
            ]

        );

        $this->assertEquals(
            $menuUpdated->items()->pluck('id')->toArray(),
            $results
        );

    }

    /**
     * @throws ReflectionException
     */
    public function test_storeMenuItemsCheckDb()
    {
        /** @var MenuService $service */
        $service = new MenuService();

        /** @var Menu $menu */
        $menu = Menu::factory()->create();

        /** @var Menu $menuUpdated */
        $menuUpdated = $this->invokeMethod(
            $service,
            'storeMenuItems',
            [
                'menu'   => $menu,
                'fields' => [
                    0 => [
                        'data'          => [],
                        'id'            => null,
                        'order'         => 0,
                        'parent_id'     => null,
                        'type'          => "parent",
                        'menuItemLangs' => [
                            ['lang' => "it", "label" => "Home IT"],
                            ['lang' => "en", "label" => "Home EN"],
                        ],
                    ],
                ],
            ]

        );

        $this->assertInstanceOf(Menu::class, $menuUpdated);

        $this->assertDatabaseHas('menu_items', [
            'menu_id'   => $menu->id,
            'order'     => 0,
            'parent_id' => null,
            'type'      => "parent",
        ]);

        $this->assertDatabaseHas('menu_item_langs', [
            'lang'  => 'it',
            'label' => "Home IT",
        ]);

        $this->assertDatabaseHas('menu_item_langs', [
            'lang'  => 'en',
            'label' => "Home EN",
        ]);

    }

    /**
     * @throws ReflectionException
     */
    public function test_storeMenuItemsCheckPage()
    {

        /** @var MenuService $service */
        $service = new MenuService();

        /** @var Menu $menu */
        $menu = Menu::factory()->create();

        /** @var Page $page */
        $page = Page::factory()->create();

        /** @var Menu $menuUpdated */
        $menuUpdated = $this->invokeMethod(
            $service,
            'storeMenuItems',
            [
                'menu'   => $menu,
                'fields' => [
                    0 => [
                        'data'          => [
                            'page' => [
                                'name'  => "HomePage",
                                'value' => $page->id,
                            ],
                        ],
                        'id'            => null,
                        'order'         => 0,
                        'parent_id'     => null,
                        'type'          => "page",
                        'menuItemLangs' => [],
                    ],
                ],
            ]

        );

        $this->assertInstanceOf(Menu::class, $menuUpdated);

        $this->assertDatabaseHas('menu_items', [
            'menu_id'   => $menu->id,
            'order'     => 0,
            'parent_id' => null,
            'type'      => "page",
            'page_id'   => $page->id,
        ]);

    }


}
