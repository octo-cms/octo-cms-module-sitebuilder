<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\MenuService;

use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\MenuLang;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Services\MenuService;
use OctoCmsModule\Core\Tests\TestCase;
use Faker\Factory as Faker;


class GetFooterMenuTest extends TestCase
{


    public function test_getFooterMenuWrongID()
    {
        /** @var MenuService $menuService */
        $menuService = new MenuService();

        $result = $menuService->getFooterMenu(3);
        $this->assertEquals($result, null);
    }

    public function test_getFooterMenu()
    {
        $faker = Faker::create();

        /** @var Page $page */
        $page = Page::factory([
            'name' => 'homepage',
            'type' => Page::TYPE_HOMEPAGE,
        ])->create();

        /** @var Menu $menu */
        $menu = Menu::factory([
            'name' => "footer-menu"
        ])->create();

        /** @var MenuLang $menuLang */
        $menuLang=MenuLang::factory([
            'menu_id' => $menu->id,
            'lang'    => 'it'
        ])->create();


        // First level
        $m1 = MenuItem::factory([
            'menu_id' => $menu->id,
            'type'    => MenuItem::TYPE_EXTERNAL,
            'data'    => ['link' => $faker->url]
        ])->create();


        $m2 = MenuItem::factory([
            'menu_id' => $menu->id,
            'type'    => MenuItem::TYPE_EXTERNAL,
            'data'    => ['link' => $faker->url],
        ])->create();

        // Second Level

        $m3 = MenuItem::factory([
            'menu_id'   => $menu->id,
            'parent_id' => $m1->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ])->create();

        $m4 = MenuItem::factory([
            'menu_id'   => $menu->id,
            'parent_id' => $m1->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ])->create();

        $m5 = MenuItem::factory([
            'menu_id'   => $menu->id,
            'parent_id' => $m2->id,
            'type'      => MenuItem::TYPE_PAGE,
            'data'      => ['page' => ['value' => $page->id]],
        ])->create();

        $m6 = MenuItem::factory([
            'menu_id'   => $menu->id,
            'parent_id' => $m2->id,
            'type'      => MenuItem::TYPE_EXTERNAL,
            'data'      => ['link' => $faker->url],
        ])->create();

        /** @var MenuService $menuService */
        $menuService = new MenuService();

        $result = $menuService->getFooterMenu($menu->id);
        $this->assertIsArray($result);
        $this->assertCount(2, $result['items']);
        $this->assertEquals($result['title'], $menuLang->title);

    }
}
