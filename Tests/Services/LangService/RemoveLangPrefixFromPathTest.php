<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\LangService;

use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Sitebuilder\Services\LangService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class RemoveLangPrefixFromPathTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\LangService
 */
class RemoveLangPrefixFromPathTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'it/home', 'home'];
        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'it', ''];
        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'en/home', 'home'];
        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'ch/home', 'ch/home'];
        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'home', 'home'];
        $providers[] = [ false, [], 'it', 'home', 'home'];
        $providers[] = [ false, ['it', 'en', 'fr'], 'it', 'en/home', 'en/home'];

        return $providers;
    }

    /**
     * @param $multilanguage
     * @param $languages
     * @param $locale
     * @param $path
     * @param $result
     * @dataProvider dataProvider
     * @return void
     */
    public function test_getLangFromPath($multilanguage, $languages, $locale, $path, $result)
    {
        Setting::updateOrCreate(
            [ 'name' => 'multilanguage'],
            [ 'value' => $multilanguage ]);

        Setting::updateOrCreate(
            [ 'name' => 'languages'],
            [ 'value' => $languages ]);

        Setting::updateOrCreate(
            [ 'name' => 'locale'],
            [ 'value' => $locale ]);

        /** @var LangService $service */
        $service = new LangService(new SettingServiceMock());

        $this->assertEquals(
            $result,
            $service->removeLangPrefixFromPath($path)
        );


    }
}
