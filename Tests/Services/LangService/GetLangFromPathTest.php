<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\LangService;

use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Sitebuilder\Services\LangService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetLangFromPathTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\LangService
 */
class GetLangFromPathTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'it/home', 'it'];
        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'it', 'it'];
        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'en/home', 'en'];
        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'ch/home', 'it'];
        $providers[] = [ true, ['it', 'en', 'fr'], 'it', 'home', 'it'];
        $providers[] = [ false, [], 'it', 'home', 'it'];
        $providers[] = [ false, ['it', 'en', 'fr'], 'it', 'en/home', 'it'];

        return $providers;
    }

    /**
     * @param $multilanguage
     * @param $languages
     * @param $locale
     * @param $path
     * @param $result
     * @dataProvider dataProvider
     * @return void
     */
    public function test_getLangFromPath($multilanguage, $languages, $locale, $path, $result)
    {
        Setting::updateOrCreate(
            [ 'name' => 'multilanguage'],
            [ 'value' => $multilanguage ]);

        Setting::updateOrCreate(
            [ 'name' => 'languages'],
            [ 'value' => $languages ]);

        Setting::updateOrCreate(
            [ 'name' => 'locale'],
            [ 'value' => $locale ]);

        /** @var LangService */
        $service = new LangService(new SettingServiceMock());

        $this->assertEquals(
            $result,
            $service->getLangFromPath($path)
        );


    }
}
