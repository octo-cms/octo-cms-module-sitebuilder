<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\FooterService;

use OctoCmsModule\Sitebuilder\Services\FooterService;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Sitebuilder\Entities\FooterContent;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SaveFootersContentsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\FooterService
 */
class SaveFootersContentsTest extends TestCase
{



    /**
     * @throws ReflectionException
     */
    public function test_saveFootersAdding()
    {

        Footer::factory()->create();
        FooterContent::factory()->create();

        /** @var FooterService */
        $footerService = new FooterService();

        $this->invokeMethod(
            $footerService,
            'saveFootersContents',
            [
                'contents' => [
                    ["id" => 1, "footer_id" => 1, "order" => 0],
                    ["id" => 2, "footer_id" => 1, "order" => 0],
                ],
            ]
        );


        $this->assertDatabaseHas('footer_contents',
            ["id" => 1, "footer_id" => 1, "order" => 0]
        );

        $this->assertDatabaseHas('footer_contents',
            ["id" => 2, "footer_id" => 1, "order" => 0]
        );

    }


    /**
     * @throws ReflectionException
     */
    public function test_saveFootersDifference()
    {

        Footer::factory()->create();
        FooterContent::factory()->create();

        /** @var FooterService */
        $footerService = new FooterService();

        $this->invokeMethod(
            $footerService,
            'saveFootersContents',
            [
                'contents' => [
                    ["id" => 2, "footer_id" => 1, "order" => 0],
                ],
            ]
        );


        $this->assertDatabaseMissing('footer_contents',
            ['id' => 1]
        );

        $this->assertDatabaseHas('footer_contents',
            ["id" => 2, "footer_id" => 1, "order" => 0]
        );

    }

    /**
     * @throws ReflectionException
     */
    public function test_saveFootersRemove()
    {

        Footer::factory()->create();

        /** @var FooterService */
        $footerService = new FooterService();

        $this->invokeMethod(
            $footerService,
            'saveFootersContents',
            [
                'contents' => [],
            ]
        );


        $this->assertDatabaseMissing('footer_contents',
            ['id' => 1]
        );


    }
}
