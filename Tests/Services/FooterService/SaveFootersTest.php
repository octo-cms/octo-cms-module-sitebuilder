<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\FooterService;

use OctoCmsModule\Core\Exceptions\OctoCmsException;
use OctoCmsModule\Sitebuilder\Services\FooterService;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Core\Tests\TestCase;
use Throwable;

/**
 * Class SaveFootersTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\FooterService
 */
class SaveFootersTest extends TestCase
{


    /**
     * @throws OctoCmsException
     * @throws Throwable
     */
    public function test_saveFootersAdding()
    {

        Footer::factory()->create();

        /** @var FooterService */
        $footerService = new FooterService();

        $footerService->saveFooters([
            ['content_align' => 'test-align-1', 'enabled' => true, 'id' => 1, 'name' => 'test-1', 'order' => 1],
            ['content_align' => 'test-align', 'enabled' => true, 'id' => null, 'name' => 'test', 'order' => 1],
        ]);


        $this->assertDatabaseHas('footers',
            ['content_align' => 'test-align-1', 'enabled' => true, 'id' => 1, 'name' => 'test-1', 'order' => 1]
        );

        $this->assertDatabaseHas('footers',
            ['content_align' => 'test-align', 'enabled' => true, 'id' => 2, 'name' => 'test', 'order' => 1],
        );

    }

    /**
     * @throws OctoCmsException
     * @throws Throwable
     */
    public function test_saveFootersDifference()
    {

        Footer::factory()->create();

        /** @var FooterService */
        $footerService = new FooterService();

        $footerService->saveFooters([
            ['content_align' => 'test-align', 'enabled' => true, 'id' => null, 'name' => 'test', 'order' => 1],
        ]);


        $this->assertDatabaseMissing('footers',
            ['content_align' => 'test-align-1', 'enabled' => true, 'id' => 1, 'name' => 'test-1', 'order' => 1]
        );

        $this->assertDatabaseHas('footers',
            ['content_align' => 'test-align', 'enabled' => true, 'id' => 2, 'name' => 'test', 'order' => 1],
        );

    }

    /**
     * @throws OctoCmsException
     * @throws Throwable
     */
    public function test_saveFootersRemove()
    {

        Footer::factory()->create();

        /** @var FooterService */
        $footerService = new FooterService();

        $footerService->saveFooters([]);

        $this->assertDatabaseMissing('footers',
            ['content_align' => 'test-align-1', 'enabled' => true, 'id' => 1, 'name' => 'test-1', 'order' => 1]
        );

        $this->assertDatabaseMissing('footers',
            ['content_align' => 'test-align', 'enabled' => true, 'id' => 2, 'name' => 'test', 'order' => 1],
        );

    }
}
