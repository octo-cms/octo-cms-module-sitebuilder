<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\FooterService;

use OctoCmsModule\Sitebuilder\Services\FooterService;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GroupContentsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\FooterService
 */
class GroupContentsTest extends TestCase
{
    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = [
            [
                [
                    'contents' => [
                        ["id" => 7, "footer_id" => 5, "targets" => false, "order" => 0],
                        ["id" => 8, "footer_id" => 5, "targets" => false, "order" => 0],
                    ],
                ],
                [
                    'contents' => [
                        ["id" => 9, "footer_id" => 5, "targets" => false, "order" => 0],
                    ],
                ],
            ],
            3,
        ];

        $providers[] = [
            [
                [
                    'contents' => [
                        ["id" => 7, "footer_id" => 5, "targets" => false, "order" => 0],
                        ["id" => 8, "footer_id" => 5, "targets" => false, "order" => 0],
                    ],
                ],
                [
                    'contents' => [],
                ],
            ],
            2,
        ];

        $providers[] = [
            [
                [
                    'contents' => [],
                ],
                [
                    'contents' => [],
                ],
            ],
            0,
        ];

        return $providers;
    }

    /**
     * @dataProvider dataProvider
     *
     * @param $fields
     * @param $count
     *
     * @throws ReflectionException
     */
    public function test_groupContents($fields, $count)
    {

        $footerService = new FooterService();

        $results = $this->invokeMethod(
            $footerService,
            'groupContents',
            ['fields' => $fields]);

        $this->assertCount($count, $results);
    }
}
