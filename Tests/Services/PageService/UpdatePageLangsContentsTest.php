<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\PageService;

use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Exceptions\OctoCmsException;
use OctoCmsModule\Sitebuilder\Services\PageService;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class UpdatePageLangsContentsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\PageService
 */
class UpdatePageLangsContentsTest extends TestCase
{



    /**
     * @throws OctoCmsException
     */
    public function test_updatePageLangNotFound()
    {
        $this->expectException(OctoCmsException::class);

        /** @var Page $page */
        $page = Page::factory()->create();

        /** @var array $pageLangsData */
        $pageLangsData = [
            [
                'lang' => 'it',
            ],
        ];
        /** @var PageService */
        $pageService = new PageService();

        $pageService->updatePageLangsContents($page, $pageLangsData);
    }

    /**
     * @throws OctoCmsException
     */
    public function test_updatePageLangsContentsOk()
    {
        /** @var string $lang */
        $lang = 'it';

        /** @var Page $page */
        $page = Page::factory()->has(PageLang::factory()->state(['lang' => $lang]))->create();

        /** @var array $pageLangsData */
        $pageLangsData = [
            [
                'lang' => $lang,
            ],
        ];
        /** @var PageService */
        $pageService = new PageService();

        $pageService->updatePageLangsContents($page, $pageLangsData);

        $this->assertDatabaseHas('pages', [
            'id' => $page->id,
        ]);

        $this->assertDatabaseHas('page_langs', [
            'page_id' => $page->id,
            'lang'    => $lang,
        ]);
    }
}
