<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\PageService;

use OctoCmsModule\Sitebuilder\Services\PageService;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;
use Throwable;

/**
 * Class SavePageTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\PageService
 */
class SavePageTest extends TestCase
{


    /**
     * @throws Throwable
     */
    public function test_storePage()
    {
        $fields = [
            'change_freq'      => Page::CHANGE_FREQ_WEEKLY,
            'name'             => 'name',
            'published'        => true,
            'sitemap'          => true,
            'sitemap_priority' => 0.5,
            'type'             => Page::TYPE_STANDARD,
            'pageLangs'        => [],
        ];

        /** @var PageService $pageService */
        $pageService = new PageService();

        /** @var Page $page */
        $page = $pageService->savePage(new Page(), $fields);

        $this->assertInstanceOf(Page::class, $page);

        $this->assertDatabaseHas('pages', [
            'id'               => $page->id,
            'name'             => $fields['name'],
            'published'        => $fields['published'],
            'sitemap'          => $fields['sitemap'],
            'sitemap_priority' => $fields['sitemap_priority'],
            'change_freq'      => $fields['change_freq'],
            'type'             => $fields['type'],
        ]);
    }

    /**
     * @throws Throwable
     */
    public function test_updatePage()
    {
        $fields = [
            'change_freq'      => 'daily',
            'name'             => 'new name',
            'published'        => false,
            'sitemap'          => false,
            'sitemap_priority' => 1,
            'type'             => Page::TYPE_HOMEPAGE,
            'pageLangs'        => [],
        ];

        $page = Page::factory()->create([
            'change_freq'      => Page::CHANGE_FREQ_WEEKLY,
            'name'             => 'name',
            'published'        => true,
            'sitemap'          => true,
            'sitemap_priority' => 0.5,
            'type'             => Page::TYPE_STANDARD,
        ]);

        /** @var PageService $pageService */
        $pageService = new PageService();

        $pageService->savePage($page, $fields);

        $this->assertDatabaseHas('pages', [
            'id'               => $page->id,
            'name'             => $fields['name'],
            'published'        => $fields['published'],
            'sitemap'          => $fields['sitemap'],
            'sitemap_priority' => $fields['sitemap_priority'],
            'change_freq'      => $fields['change_freq'],
            'type'             => $fields['type'],
        ]);
    }
}
