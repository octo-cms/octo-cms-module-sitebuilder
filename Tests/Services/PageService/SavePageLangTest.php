<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\PageService;

use OctoCmsModule\Sitebuilder\Services\PageService;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;

/**
 * Class SavePageLangTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\PageService
 */
class SavePageLangTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_storePageLang()
    {
        $pageLangs = [
            [
                'lang'             => 'it',
                'url'              => 'url it',
                'meta_title'       => 'meta_title it',
                'meta_description' => 'meta_description it',
            ],
            [
                'lang'             => 'en',
                'url'              => 'url en',
                'meta_title'       => 'meta_title en',
                'meta_description' => 'meta_description en',
            ],
        ];

        /** @var Page $page */
        $page = Page::factory()->create();

        /** @var PageService $pageService */
        $pageService = new PageService();

        $this->invokeMethod(
            $pageService,
            'savePageLang',
            [
                'page'      => $page,
                'pageLangs' => $pageLangs,
            ]
        );

        foreach ($pageLangs as $pageLang) {
            $this->assertDatabaseHas('page_langs', [
                'page_id'          => $page->id,
                'lang'             => $pageLang['lang'],
                'url'              => $pageLang['url'],
                'meta_title'       => $pageLang['meta_title'],
                'meta_description' => $pageLang['meta_description'],
            ]);
        }
    }

    /**
     * @throws ReflectionException
     */
    public function test_updatePageLang()
    {
        $pageLangs = [
            [
                'lang'             => 'it',
                'url'              => 'new url it',
                'meta_title'       => 'new meta_title it',
                'meta_description' => 'new meta_description it',
            ],
        ];

        /** @var Page $page */
        $page = Page::factory()
            ->has(
                PageLang::factory()->state([
                    'lang'             => 'it',
                    'url'              => 'url it',
                    'meta_title'       => 'meta_title it',
                    'meta_description' => 'meta_description it',
                ])
            )->create();

        /** @var PageService $pageService */
        $pageService = new PageService();

        $this->invokeMethod(
            $pageService,
            'savePageLang',
            [
                'page'      => $page,
                'pageLangs' => $pageLangs,
            ]
        );

        foreach ($pageLangs as $pageLang) {
            $this->assertDatabaseHas('page_langs', [
                'page_id'          => $page->id,
                'lang'             => $pageLang['lang'],
                'url'              => $pageLang['url'],
                'meta_title'       => $pageLang['meta_title'],
                'meta_description' => $pageLang['meta_description'],
            ]);
        }
    }
}
