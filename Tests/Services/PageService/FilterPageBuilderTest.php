<?php

namespace OctoCmsModule\Sitebuilder\Tests\Services\PageService;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use OctoCmsModule\Sitebuilder\Services\PageService;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class FilterPageBuilderTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Services\PageService
 */
class FilterPageBuilderTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $filters = [
            'published'   => false,
            'softDeleted' => false,
            'type'        => 'homepage',
        ];

        $providers[] = [$filters];

        $fields = $filters;
        $fields['published'] = true;
        $providers[] = [$fields];

        $fields = $filters;
        $fields['softDeleted'] = true;
        $providers[] = [$fields];

        $fields = $filters;
        $fields['type'] = 'standard';
        $providers[] = [$fields];

        return $providers;
    }

    /**
     * @param $filters
     *
     * @dataProvider dataProvider
     */
    public function test_filters($filters)
    {
        /** @var Page $page */
        Page::factory()->create([
            'published'  => $filters['published'],
            'deleted_at' => $filters['softDeleted'] ? Carbon::now() : null,
            'type'       => $filters['type'],
        ]);

        /** @var PageService $pageService */
        $pageService = new PageService();

        /** @var Builder $pageBuilderFromService */
        $pageBuilderFromService = $pageService->filterPageBuilder(
            Page::query(),
            $filters,
            ''
        );

        $this->assertInstanceOf(Builder::class, $pageBuilderFromService);

        $this->assertCount(1, $pageBuilderFromService->get());
    }

    public function test_querySearch()
    {
        /** @var string $query */
        $query = Page::TYPE_HOMEPAGE;

        /** @var Page $page */
        $page = Page::factory()->create([
            'name' => 'homepage',
            'type' => $query,
        ]);

        Page::factory()->create([
            'name' => 'standard page',
            'type' => Page::TYPE_STANDARD,
        ]);

        /** @var PageService $pageService */
        $pageService = new PageService();

        /** @var Builder $pageBuilderFromService */
        $pageBuilderFromService = $pageService->filterPageBuilder(
            Page::query(),
            [],
            $query
        );

        $this->assertInstanceOf(Builder::class, $pageBuilderFromService);

        $this->assertCount(1, $pageBuilderFromService->get());

        $this->assertEquals(
            $page->id,
            $pageBuilderFromService->first()->id
        );
    }

    public function test_querySearchPageLang()
    {
        /** @var string $query */
        $query = 'titolo del page lang';

        /** @var Page $page */
        $page = Page::factory()
            ->has(PageLang::factory()->state(['meta_title' => $query]))
            ->create(['name' => 'homepage', 'type' => Page::TYPE_HOMEPAGE]);

        /** @var PageService $pageService */
        $pageService = new PageService();

        /** @var Builder $pageBuilderFromService */
        $pageBuilderFromService = $pageService->filterPageBuilder(
            Page::query(),
            [],
            $query
        );

        $this->assertInstanceOf(Builder::class, $pageBuilderFromService);

        $this->assertCount(1, $pageBuilderFromService->get());

        $this->assertEquals(
            $page->id,
            $pageBuilderFromService->first()->id
        );
    }
}
