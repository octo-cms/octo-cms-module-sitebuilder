<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class BlockHtmlTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class BlockHtmlTest extends TestCase
{


    public function test_BlockHtmlMorphManyPageLangContents()
    {
        /** @var BlockHtml $blockHtml */
        $blockHtml = BlockHtml::factory()
            ->has(PageLangContent::factory()->count(2), 'pageLangContents')
            ->create();

        $blockHtml->load('pageLangContents');

        $this->assertInstanceOf(PageLangContent::class, $blockHtml->pageLangContents->first());
    }

    public function test_getAndSetSettingsAttribute()
    {
        /** @var BlockHtml $blockHtml */
        $blockHtml = BlockHtml::factory()->create();

        /** @var array $settingsArray */
        $settingsArray = ['settings'];

        $blockHtml->settings = $settingsArray;

        $blockHtml->save();

        $this->assertDatabaseHas('block_html', [
            'settings' => serialize($settingsArray)
        ]);

        $this->assertEquals(
            $blockHtml->settings,
            $settingsArray
        );
    }

    public function test_getAndSetLayoutAttribute()
    {
        /** @var BlockHtml $blockHtml */
        $blockHtml = BlockHtml::factory()->create();

        /** @var array $layoutArray */
        $layoutArray = ['layout'];

        $blockHtml->layout = $layoutArray;

        $blockHtml->save();

        $this->assertDatabaseHas('block_html', [
            'layout' => serialize($layoutArray)
        ]);

        $this->assertEquals(
            $blockHtml->layout,
            $layoutArray
        );
    }
}
