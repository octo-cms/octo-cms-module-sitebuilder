<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Sitebuilder\Entities\FooterContent;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class FooterContentTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class FooterContentTest extends TestCase
{


    public function test_FooterContentBelongsToFooter()
    {
        /** @var FooterContent $footerContent */
        $footerContent = FooterContent::factory()->create();

        $footerContent->load('footer');

        $this->assertInstanceOf(Footer::class, $footerContent->footer);
    }

    public function test_PageLangContentHasOneBlockHtml()
    {
        /** @var FooterContent $footerContent */
        $footerContent = FooterContent::factory()
            ->has(BlockHtml::factory(), 'type')
            ->create();

        $footerContent->type()->associate(BlockHtml::factory()->create())->save();

        $footerContent->load('type');

        $this->assertEquals(
            'BlockHtml',
            $footerContent->content_type
        );

        $this->assertInstanceOf(BlockHtml::class, $footerContent->type);
    }
}
