<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use OctoCmsModule\Sitebuilder\DTO\PagelangContentTargetsDTO;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PageLangContentTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class PageLangContentTest extends TestCase
{


    public function test_PageLangContentBelongsToPageLang()
    {
        /** @var PageLangContent $pageLangContent */
        $pageLangContent = PageLangContent::factory()->create();

        $pageLangContent->load('pageLang');

        $this->assertInstanceOf(PageLang::class, $pageLangContent->pageLang);
    }

    public function test_PageLangContentHasOneBlockHtml()
    {
        /** @var PageLangContent $pageLangContent */
        $pageLangContent = PageLangContent::factory()->create();

        /** @var BlockHtml $blockHtml */
        $blockHtml = BlockHtml::factory()->create();

        $pageLangContent->type()->associate($blockHtml)->save();

        $pageLangContent->load('type');

        $this->assertEquals(
            'BlockHtml',
            $pageLangContent->content_type
        );

        $this->assertInstanceOf(BlockHtml::class, $pageLangContent->type);
    }

    public function test_BlockEntityData()
    {
        /** @var PageLangContent $pageLangContent */
        $pageLangContent = PageLangContent::factory()
            ->create([
                'data' => ['test' => 'test'],
            ]);

        $this->assertEquals(
            ['test' => 'test'],
            $pageLangContent->data
        );

    }

    public function test_getAndSetTargetsAttribute()
    {
        /** @var PageLangContent $pageLangContent */
        $pageLangContent = PageLangContent::factory()->create();

        $pageLangContent->targets = new PagelangContentTargetsDTO(
            PageLangContent::TARGET_TYPE_CUSTOM, [1, 2, 3]
        );

        $pageLangContent->save();

        $this->assertDatabaseHas('page_lang_contents', [
            'targets' => serialize($pageLangContent->targets),
        ]);

        $this->assertEquals(
            [1, 2, 3],
            $pageLangContent->targets->values
        );
    }

    public function test_getAndSetLayoutAttribute()
    {
        /** @var PageLangContent $pageLangContent */
        $pageLangContent = PageLangContent::factory()->create();

        $pageLangContent->layout = [
            ['type' => 'boolean', 'name' => 'divider-top'],
        ];

        $pageLangContent->save();

        $this->assertDatabaseHas('page_lang_contents', [
            'layout' => serialize($pageLangContent->layout),
        ]);
    }
}
