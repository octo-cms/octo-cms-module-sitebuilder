<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class MenuLangTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class MenuLangTest extends TestCase
{


    public function test_MenuLangBelongsToMenu()
    {
        /** @var MenuLang $menuLang */
        $menuLang = MenuLang::factory()->create();

        $menuLang->load('menu');

        $this->assertInstanceOf(Menu::class, $menuLang->menu);
    }
}
