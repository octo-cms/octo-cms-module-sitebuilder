<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PageLangTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class PageLangTest extends TestCase
{


    public function test_PageLangBelongsToPage()
    {
        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()->create();

        $pageLang->load('page');

        $this->assertInstanceOf(Page::class, $pageLang->page);
    }

    public function test_PageLangHasManyContents()
    {
        /** @var PageLang $pageLang */
        $pageLang = PageLang::factory()
            ->has(PageLangContent::factory()->count(2), 'contents')
            ->create();

        $pageLang->load('contents');

        $this->assertInstanceOf(Collection::class, $pageLang->contents);
        $this->assertInstanceOf(PageLangContent::class, $pageLang->contents->first());
    }
}
