<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use OctoCmsModule\Sitebuilder\Entities\BlockEntity;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class BlockEntityTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class BlockEntityTest extends TestCase
{


    public function test_getAndSetSettingsAttribute()
    {
        /** @var BlockEntity $blockEntity */
        $blockEntity = BlockEntity::factory()->create();

        /** @var array $settingsArray */
        $settingsArray = ['settings'];

        $blockEntity->settings = $settingsArray;

        $blockEntity->save();

        $this->assertDatabaseHas('block_entities', [
            'settings' => serialize($settingsArray)
        ]);

        $this->assertEquals(
            $blockEntity->settings,
            $settingsArray
        );
    }

    public function test_getAndSetLayoutAttribute()
    {
        /** @var BlockEntity $blockEntity */
        $blockEntity = BlockEntity::factory()->create();

        /** @var array $layoutArray */
        $layoutArray = ['layout'];

        $blockEntity->layout = $layoutArray;

        $blockEntity->save();

        $this->assertDatabaseHas('block_entities', [
            'layout' => serialize($layoutArray)
        ]);

        $this->assertEquals(
            $blockEntity->layout,
            $layoutArray
        );
    }
}
