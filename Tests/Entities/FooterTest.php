<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Sitebuilder\Entities\FooterContent;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PageTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 * @author  danielepasi
 */
class FooterTest extends TestCase
{


    public function test_FooterHasManyContents()
    {
        /** @var Footer $footer */
        $footer = Footer::factory()
            ->has(FooterContent::factory()->count(2), 'contents')
            ->create();

        $footer->load('contents');

        $this->assertInstanceOf(Collection::class, $footer->contents);
        $this->assertInstanceOf(FooterContent::class, $footer->contents->first());
    }
}
