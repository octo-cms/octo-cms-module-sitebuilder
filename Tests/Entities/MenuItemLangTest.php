<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use OctoCmsModule\Sitebuilder\Entities\MenuItemLang;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class MenuItemLangTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class MenuItemLangTest extends TestCase
{


    public function test_MenuItemLangBelongsToMenuItem()
    {
        /** @var MenuItemLang $menuItemLang */
        $menuItemLang = MenuItemLang::factory()->create();

        $menuItemLang->load('menuItem');

        $this->assertInstanceOf(MenuItem::class, $menuItemLang->menuItem);
    }
}
