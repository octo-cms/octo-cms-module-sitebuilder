<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use OctoCmsModule\Sitebuilder\Entities\PageRedirect;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PageRedirectTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class PageRedirectTest extends TestCase
{


    public function test_PageRedirectBelongsToPageLang()
    {
        /** @var PageRedirect $pageRedirect */
        $pageRedirect = PageRedirect::factory()->create();

        $pageRedirect->load('pageLang');

        $this->assertInstanceOf(PageLang::class, $pageRedirect->pageLang);
    }
}
