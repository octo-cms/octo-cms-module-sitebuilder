<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuItemLang;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PageTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 * @author  danielepasi
 */
class MenuItemTest extends TestCase
{


    public function test_MenuItemHasManyMenuItemLang()
    {
        /** @var MenuItem $menuItem */
        $menuItem = MenuItem::factory()->has(MenuItemLang::factory()->count(2))->create();

        $menuItem->load('menuItemLangs');

        $this->assertInstanceOf(Collection::class, $menuItem->menuItemLangs);
        $this->assertInstanceOf(MenuItemLang::class, $menuItem->menuItemLangs->first());
    }

    public function test_MenuItemBelongsToMenu()
    {
        /** @var MenuItem $menuItem */
        $menuItem = MenuItem::factory()->create();

        $menuItem->load('menu');

        $this->assertInstanceOf(Menu::class, $menuItem->menu);
    }

    public function test_getAndSetDataAttribute()
    {
        /** @var MenuItem $menuItem */
        $menuItem = MenuItem::factory()->create();

        /** @var array $dataArray */
        $dataArray = ['data'];

        $menuItem->data = $dataArray;

        $menuItem->save();

        $this->assertDatabaseHas('menu_items', [
            'data' => serialize($dataArray),
        ]);

        $this->assertEquals(
            $menuItem->data,
            $dataArray
        );
    }

    public function test_MenuItemBelongsToPage()
    {
        /** @var MenuItem $menuItem */
        $menuItem = MenuItem::factory()->create();

        $menuItem->load('page');

        $this->assertInstanceOf(Page::class, $menuItem->page);
    }
}
