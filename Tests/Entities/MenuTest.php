<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\MenuLang;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class MenuTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 */
class MenuTest extends TestCase
{


    public function test_MenuHasManyMenu()
    {
        /** @var Menu $menu */
        $menu = Menu::factory()
            ->has(MenuItem::factory()->count(2), 'items')
            ->create();

        $menu->load('items');

        $this->assertInstanceOf(Collection::class, $menu->items);
        $this->assertInstanceOf(MenuItem::class, $menu->items->first());
    }

    public function test_MenuHasManyMenuLangs()
    {
        /** @var Menu $menu */
        $menu = Menu::factory()->has(MenuLang::factory()->count(2))->create();

        $menu->load('menuLangs');

        $this->assertInstanceOf(Collection::class, $menu->menuLangs);
        $this->assertInstanceOf(MenuLang::class, $menu->menuLangs->first());
    }
}
