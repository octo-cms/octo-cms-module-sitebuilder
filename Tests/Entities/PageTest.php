<?php

namespace OctoCmsModule\Sitebuilder\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PageTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Entities
 * @author  danielepasi
 */
class PageTest extends TestCase
{


    public function test_PageHasManyPageLangs()
    {
        /** @var Page $page */
        $page = Page::factory()->has(PageLang::factory()->count(2))->create();

        $page->load('pageLangs');

        $this->assertInstanceOf(Collection::class, $page->pageLangs);
        $this->assertInstanceOf(PageLang::class, $page->pageLangs->first());
    }

    public function test_PageHasManyMenuItems()
    {
        /** @var Page $page */
        $page = Page::factory()->has(MenuItem::factory()->count(2))->create()->first();

        $page->load('menuItems');

        $this->assertInstanceOf(Collection::class, $page->menuItems);
        $this->assertInstanceOf(MenuItem::class, $page->menuItems->first());
    }
}
