<?php

namespace OctoCmsModule\Sitebuilder\Tests\Commands\SyncPageContentsCommand;

use OctoCmsModule\Sitebuilder\Console\SyncPageContentsCommand;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SyncContentWithBlockTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Commands\SyncPageContentsCommand
 */
class SyncContentWithBlockTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_syncContentData()
    {
        /** @var SyncPageContentsCommand $syncPageContentsCommand */
        $syncPageContentsCommand = new SyncPageContentsCommand();

        /** @var array $contentData */
        $contentData = [
            [
                'name'  => 'name-1',
                'type'  => 'type-1',
                'value' => 'value-1',
            ],
        ];

        /** @var array $blockData */
        $blockData = [
            [
                'name' => 'name-1',
                'type' => 'type-1',
            ],
            [
                'name' => 'name-2',
                'type' => 'type-2',
            ],
        ];
        /** @var string $valueKey */
        $valueKey = 'value';

        $this->assertEquals(
            [
                [
                    'name'    => 'name-1',
                    'type'    => 'type-1',
                    $valueKey => 'value-1',
                ],
                [
                    'name'    => 'name-2',
                    'type'    => 'type-2',
                    $valueKey => null,
                ],
            ],
            $this->invokeMethod(
                $syncPageContentsCommand,
                'syncContentWithBlock',
                [
                    'contentData' => $contentData,
                    'blockData'   => $blockData,
                    'valueKey'    => $valueKey,
                ]
            )
        );
    }
}
