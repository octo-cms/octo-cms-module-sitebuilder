<?php

namespace OctoCmsModule\Sitebuilder\Tests\Commands\SyncPageContentsCommand;

use OctoCmsModule\Sitebuilder\Entities\BlockEntity;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class HandleTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Commands\SyncPageContentsCommand
 */
class HandleTest extends TestCase
{


    public function test_syncContentDataWithBlockHtml()
    {
        /** @var BlockHtml $blockHtml */
        $blockHtml = BlockHtml::factory()->create([
            'settings' => [
                [
                    'name' => 'name-1',
                    'type' => 'type-1',
                ],
                [
                    'name' => 'name-2',
                    'type' => 'type-2',
                ],
            ],
            'layout'   => [
                [
                    'name' => 'layout-1',
                    'type' => 'type-1',
                ],
                [
                    'name' => 'layout-2',
                    'type' => 'type-2',
                ],
            ],
        ]);

        /** @var PageLangContent $pageLangContent */
        $pageLangContent = PageLangContent::factory()->create([
            'data'   => [
                [
                    'name'  => 'name-1',
                    'type'  => 'type-1',
                    'value' => 'value-1',
                ],
            ],
            'layout' => [
                [
                    'name'     => 'layout-1',
                    'type'     => 'type-1',
                    'selected' => 'selected-1',
                ],
            ],
        ]);

        $pageLangContent->type()->associate($blockHtml);

        $pageLangContent->save();

        $this->artisan('sync-page-contents')
            ->expectsOutput('Syncing Page Contents ...');

        $this->assertDatabaseHas('page_lang_contents', [
            'id'     => $pageLangContent->id,
            'data'   => serialize([
                [
                    'name'  => 'name-1',
                    'type'  => 'type-1',
                    'value' => 'value-1',
                ],
                [
                    'name'  => 'name-2',
                    'type'  => 'type-2',
                    'value' => null,
                ],
            ]),
            'layout' => serialize([
                [
                    'name'     => 'layout-1',
                    'type'     => 'type-1',
                    'selected' => 'selected-1',
                ],
                [
                    'name'     => 'layout-2',
                    'type'     => 'type-2',
                    'selected' => null,
                ],
            ]),
        ]);
    }

    public function test_syncContentDataWithBlockEntity()
    {
        /** @var BlockHtml $blockEntity */
        $blockEntity = BlockEntity::factory()->create([
            'layout' => [
                [
                    'name' => 'layout-1',
                    'type' => 'type-1',
                ],
                [
                    'name' => 'layout-2',
                    'type' => 'type-2',
                ],
            ],
        ]);

        /** @var PageLangContent $pageLangContent */
        $pageLangContent = PageLangContent::factory()->create([
            'layout' => [
                [
                    'name'     => 'layout-1',
                    'type'     => 'type-1',
                    'selected' => 'selected-1',
                ],
            ],
        ]);

        $pageLangContent->type()->associate($blockEntity);

        $pageLangContent->save();

        $this->artisan('sync-page-contents')
            ->expectsOutput('Syncing Page Contents ...');

        $this->assertDatabaseHas('page_lang_contents', [
            'id'     => $pageLangContent->id,
            'layout' => serialize([
                [
                    'name'     => 'layout-1',
                    'type'     => 'type-1',
                    'selected' => 'selected-1',
                ],
                [
                    'name'     => 'layout-2',
                    'type'     => 'type-2',
                    'selected' => null,
                ],
            ]),
        ]);
    }
}

