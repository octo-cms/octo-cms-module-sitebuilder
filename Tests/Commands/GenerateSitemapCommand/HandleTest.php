<?php

namespace OctoCmsModule\Sitebuilder\Tests\Commands\GenerateSitemapCommand;

use Illuminate\Support\Facades\Storage;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class HandleTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Commands\GenerateSitemapCommand
 */
class HandleTest extends TestCase
{


    public function test_command()
    {
        Storage::fake('public');

        Page::factory()->has(PageLang::factory()->count(2))->create([
            'sitemap' => true,
        ]);

        Setting::factory()->create([
            'name'  => 'fallback_locale',
            'value' => 'it',
        ]);

        $this->artisan('sitemap:generate')
            ->expectsOutput('Generating sitemap XML file ...');

        Storage::disk('public')->assertExists('sitemap.xml');
    }
}
