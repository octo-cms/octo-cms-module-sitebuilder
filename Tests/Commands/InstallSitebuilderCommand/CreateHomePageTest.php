<?php

namespace OctoCmsModule\Sitebuilder\Tests\Commands\InstallSitebuilderCommand;

use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Sitebuilder\Console\InstallSitebuilderCommand;
use OctoCmsModule\Sitebuilder\Entities\Page;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CreateHomePageTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Commands\InstallSitebuilderCommand
 */
class CreateHomePageTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_createHomepage()
    {
        /** @var array $langs */
        $langs = ['it', 'en'];

        /** @var InstallSitebuilderCommand */
        $installSitebuilderCommand = new InstallSitebuilderCommand(new SettingServiceMock());

        /** @var Page $homepage */
        $this->invokeMethod(
            $installSitebuilderCommand,
            'createHomePage',
            [$langs]
        );

        $this->assertDatabaseHas('pages', [
            'name' => 'HomePage',
            'type' => Page::TYPE_HOMEPAGE,
        ]);

        foreach ($langs as $lang) {
            $this->assertDatabaseHas('page_langs', [
                'page_id' => 1,
                'lang'    => $lang,
                'url'     => '',
            ]);
        }
    }
}
