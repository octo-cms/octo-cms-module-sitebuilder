<?php

namespace OctoCmsModule\Sitebuilder\Tests\Commands\InstallSitebuilderCommand;

use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Sitebuilder\Console\InstallSitebuilderCommand;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CreateSettingsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Commands\InstallSitebuilderCommand
 */
class CreateSettingsTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_createHtmlBlocksMultilanguage()
    {
        /** @var array $langs */
        $langs = ['it', 'en'];

        /** @var InstallSitebuilderCommand $installSitebuilderCommand */
        $installSitebuilderCommand = new InstallSitebuilderCommand(new SettingServiceMock());

        $this->invokeMethod(
            $installSitebuilderCommand,
            'createSettings',
            [$langs]
        );

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_admin_mail',
            'value' => serialize(''),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_title',
            'value' => serialize([]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_description',
            'value' => serialize([]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_address',
            'value' => serialize([]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_date_format',
            'value' => serialize('Y-m-d'),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_hour_format',
            'value' => serialize('H:i'),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_main_language',
            'value' => serialize('it'),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_available_languages',
            'value' => serialize($langs),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_logo',
            'value' => serialize([
                'src'  => '',
                'webp' => '',
            ]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_gtm',
            'value' => serialize([
                'enabled'      => false,
                'container_id' => null,
            ]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_social_sharing',
            'value' => serialize([
                'facebook'  => ['enabled' => false, 'link' => null],
                'twitter'   => ['enabled' => false, 'link' => null],
                'instagram' => ['enabled' => false, 'link' => null],
            ]),
        ]);
    }
}
