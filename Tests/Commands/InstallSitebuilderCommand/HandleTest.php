<?php

namespace OctoCmsModule\Sitebuilder\Tests\Commands\InstallSitebuilderCommand;

use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class HandleTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Commands\InstallSitebuilderCommand
 */
class HandleTest extends TestCase
{


    public function test_command()
    {

        $this->artisan('install:sitebuilder')
            ->expectsOutput('Running Install Site builder Command ...')
            ->expectsOutput('Creating Settings ...')
            ->expectsOutput('Creating Homepage ...');

        $this->assertDatabaseHas('settings', [
            'name'  => 'locale',
            'value' => serialize(config('app.locale')),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fallback_locale',
            'value' => serialize(config('app.locale')),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'multilanguage',
            'value' => serialize(false),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'languages',
            'value' => serialize([config('app.locale')]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_admin_mail', 'value' => serialize(''),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_title', 'value' => serialize([]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_description', 'value' => serialize([]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_address', 'value' => serialize([]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_date_format', 'value' => serialize('Y-m-d'),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_hour_format', 'value' => serialize('H:i'),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_main_language', 'value' => serialize(config('app.locale')),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_available_languages', 'value' => serialize([config('app.locale')]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_logo', 'value' => serialize(['src' => '', 'webp' => '']),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_favicon', 'value' => serialize(['src' => '', 'webp' => '']),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_gtm', 'value' => serialize(['enabled' => false, 'container_id' => null]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_social_sharing', 'value' => serialize([
                'facebook'  => ['enabled' => false, 'link' => null],
                'twitter'   => ['enabled' => false, 'link' => null],
                'instagram' => ['enabled' => false, 'link' => null],
            ]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'fe_google_optimize',
            'value' => serialize(['enabled' => false, 'container_id' => null, 'property_id' => null]),
        ]);

        $this->assertDatabaseHas('settings', [
            'name' => 'fe_iubenda', 'value' => serialize(['enabled' => false, 'link' => '']),
        ]);


        $this->assertDatabaseHas('menus', [
            'name'  => 'main',
            'blade' => 'main',
        ]);


    }
}
