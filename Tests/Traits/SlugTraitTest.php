<?php

namespace OctoCmsModule\Sitebuilder\Tests\Traits;

use OctoCmsModule\Sitebuilder\Traits\SlugTrait;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SlugTraitTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Traits
 */
class SlugTraitTest extends TestCase
{
    public function test_retrieveSlug()
    {
        $class = new class {
            use SlugTrait;
        };

        $this->assertEquals('slug-test', $class->retrieveSlug('slug test'));
    }
}
