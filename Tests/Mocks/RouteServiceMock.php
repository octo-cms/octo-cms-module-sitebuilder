<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Sitebuilder\DTO\PageLangDataDTO;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Interfaces\RouteServiceInterface;

/**
 * Class RouteServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests8Mocks
 */
class RouteServiceMock implements RouteServiceInterface
{
    /**
     * @param string $path
     *
     * @return PageLang|null
     */
    public function searchPageLang(string $path): ?PageLang
    {
        return PageLang::factory()->create([
            'url' => $path,
        ]);
    }

    /**
     * @param Page $page
     *
     * @return array|string[]
     */
    public function getViews(Page $page): array
    {
        return [
            'sitebuilder::index',
        ];
    }

    /**
     * @param PageLang $pageLang
     *
     * @return PageLangDataDTO
     */
    public function getPageLangData(PageLang $pageLang): PageLangDataDTO
    {
        $pageLangDTO = new PageLangDataDTO();
        $pageLangDTO->template = 'sitebuilder';
        return $pageLangDTO;

    }
}
