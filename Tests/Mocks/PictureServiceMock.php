<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Sitebuilder\Interfaces\PictureServiceInterface;

/**
 * Class PictureServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class PictureServiceMock implements PictureServiceInterface
{

    /**
     * @param array  $pictureFields
     * @param string $path
     *
     * @return array|string[]|null
     */
    public function uploadPicture(array $pictureFields, string $path): ?array
    {
        return [
            'src'  => 'src',
            'webp' => 'webp',
        ];
    }

    /**
     * @param mixed $entity
     * @param array $content
     *
     * @return bool
     */
    public function savePicturesEntity($entity, $content): bool
    {
        return true;
    }
}
