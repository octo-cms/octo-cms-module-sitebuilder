<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use Carbon\Carbon;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Interfaces\SitemapServiceInterface;
use Spatie\Sitemap\Tags\Url;

/**
 * Class SitemapServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class SitemapServiceMock implements SitemapServiceInterface
{
    /**
     * @param Page   $page
     * @param string $fallbackLocale
     * @param array  $languages
     *
     * @return Url|null
     */
    public function generateUrlInstance(Page $page, string $fallbackLocale, array $languages):? Url
    {
        return Url::create('url')
            ->setLastModificationDate(Carbon::now())
            ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
            ->setPriority(0.5);
    }
}
