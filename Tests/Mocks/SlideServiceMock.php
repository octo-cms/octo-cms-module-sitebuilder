<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Sitebuilder\Interfaces\SlideServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Class SlideServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class SlideServiceMock implements SlideServiceInterface
{
    /**
     * @param Slide $slide
     * @param array $fields
     *
     * @return Slide
     */
    public function saveSlide(Slide $slide, array $fields): Slide
    {
        return $slide;
    }
}
