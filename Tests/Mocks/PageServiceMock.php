<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use Illuminate\Database\Eloquent\Builder;
use OctoCmsModule\Sitebuilder\Interfaces\PageServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class PageServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class PageServiceMock implements PageServiceInterface
{
    /**
     * @param Builder $pages
     * @param array   $filters
     * @param string  $query
     *
     * @return Builder
     */
    public function filterPageBuilder(Builder $pages, array $filters = [], string $query = ''): Builder
    {
        return Page::query()->orderByDesc('updated_at');
    }

    /**
     * @param Page  $page
     * @param array $pageLangsData
     */
    public function updatePageLangsContents(Page $page, array $pageLangsData)
    {
        // TODO: Implement updatePageLangsContents() method.
    }

    /**
     * @param Page  $page
     * @param array $fields
     *
     * @return Page
     */
    public function savePage(Page $page, array $fields): Page
    {
        return $page;
    }
}
