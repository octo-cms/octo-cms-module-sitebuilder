<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Sitebuilder\Interfaces\FooterServiceInterface;

/**
 * Class FooterServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class FooterServiceMock implements FooterServiceInterface
{
    /**
     * @param array $fields
     *
     * @return bool
     */
    public function saveFooters(array $fields): bool
    {
        return true;
    }
}
