<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock as SettingServiceMockCore;
use OctoCmsModule\Sitebuilder\Interfaces\SettingServiceInterface;

/**
 * Class SettingServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class SettingServiceMock extends SettingServiceMockCore implements SettingServiceInterface
{
    public function saveSiteSettings(array $fields)
    {
        // TODO: Implement saveSiteSettings() method.
    }
}
