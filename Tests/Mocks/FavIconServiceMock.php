<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Sitebuilder\Interfaces\FavIconServiceInterface;

/**
 * Class FavIconServiceMock
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class FavIconServiceMock implements FavIconServiceInterface
{
    /**
     * @param string $file
     * @return bool
     */
    public function createFromFile(string $file): bool
    {
        return true;
    }

    /**
     * @param string $url
     * @return bool
     */
    public function createFromUrl(string $url): bool
    {
        return true;
    }
}
