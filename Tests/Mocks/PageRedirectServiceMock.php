<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Sitebuilder\Interfaces\PageRedirectServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;

/**
 * Class PageRedirectServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class PageRedirectServiceMock implements PageRedirectServiceInterface
{
    /**
     * @param PageLang $pageLang
     * @param array    $fields
     *
     * @return PageRedirect
     */
    public function storePageRedirect(PageLang $pageLang, array $fields): PageRedirect
    {
        return PageRedirect::factory()->create();
    }

    /**
     * @param PageRedirect $pageRedirect
     * @param array        $fields
     *
     * @return PageRedirect
     */
    public function updatePageRedirect(PageRedirect $pageRedirect, array $fields): PageRedirect
    {
        return $pageRedirect;
    }

    /**
     * @param string $old_url
     *
     * @return PageLang|null
     */
    public function getRedirectPageLang(string $old_url): ?PageLang
    {
        return PageLang::factory()->create();
    }
}
