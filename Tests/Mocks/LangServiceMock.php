<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Sitebuilder\Interfaces\LangServiceInterface;

/**
 * Class LangServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 * @author  danielepasi
 */
class LangServiceMock implements LangServiceInterface
{

    /**
     * @inheritDoc
     */
    public function getLangFromPath(string $path): string
    {
        return 'it';
    }

    /**
     * @inheritDoc
     */
    public function removeLangPrefixFromPath(string $path): string
    {
        return $path;
    }
}
