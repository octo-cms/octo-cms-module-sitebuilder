<?php

namespace OctoCmsModule\Sitebuilder\Tests\Mocks;

use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Interfaces\MenuServiceInterface;

/**
 * Class MenuServiceMock
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Mocks
 */
class MenuServiceMock implements MenuServiceInterface
{


    /**
     * @inheritDoc
     */
    public function storeMenuItems(Menu $menu, array $fields)
    {
        return $menu;
    }

    /**
     * @param Menu  $menu
     * @param array $menuLangs
     *
     * @return Menu
     */
    public function saveMenu(Menu $menu, array $menuLangs): Menu
    {
        return $menu;
    }

    /**
     * @return array|null
     */
    public function getMainMenu(): ?array
    {
        return null;
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function getFooterMenu(int $id): ?array
    {
        return null;
    }
}
