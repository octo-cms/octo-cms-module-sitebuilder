<?php

namespace OctoCmsModule\Sitebuilder\Tests\Components\ImageComponent;

use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\PictureLang;
use OctoCmsModule\Core\Traits\ImageSrcTrait;
use OctoCmsModule\Sitebuilder\View\Components\ImageComponent;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetPictureDataTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Components\ImageComponent
 */
class GetPictureDataTest extends TestCase
{
    use ImageSrcTrait;

    public function test_renderPicture()
    {
        /** @var string */
        $class = 'class';

        /** @var string */
        $defaultSrc = 'default';

        /** @var Picture $picture */
        $picture = Picture::factory()->create();

        PictureLang::factory()->create([
            'picture_id'  => $picture->id,
            'lang'        => 'en',
            'alt'         => 'alt-en',
            'caption'     => 'caption-en',
            'title'       => 'title-en',
            'description' => 'description-en',
        ]);

        /** @var PictureLang $pictureLangIt */
        $pictureLangIt = PictureLang::factory()->create([
            'picture_id'  => $picture->id,
            'lang'        => 'it',
            'alt'         => 'alt-it',
            'caption'     => 'caption-it',
            'title'       => 'title-it',
            'description' => 'description-it',
        ]);

        /** @var ImageComponent */
        $imageComponent = new ImageComponent($picture->load('pictureLangs')->toArray(), $defaultSrc, $class);

        $this->assertEquals(
            "<img  src='default' class='class' alt='alt-it' caption='caption-it' description='description-it' title='title-it' />",
            $imageComponent->render()
        );

    }


    public function dataProvider()
    {
        $providers = [];

        $providers[] = [
            ['src' => 'src', 'alt' => 'alt', 'caption' => 'cap', 'title' => 'tit', 'description' => 'des'],
            "<img  src='default' class='class' alt='alt' caption='cap' description='des' title='tit' />"
        ];

        $providers[] = [
            ['src' => 'src', 'caption' => 'cap', 'title' => 'tit', 'description' => 'des'],
            "<img  src='default' class='class' caption='cap' description='des' title='tit' />"
        ];

        $providers[] = [
            ['src' => 'src', 'alt' => 'alt', 'title' => 'tit', 'description' => 'des'],
            "<img  src='default' class='class' alt='alt' description='des' title='tit' />"
        ];

        $providers[] = [
            ['src' => 'src', 'alt' => 'alt', 'caption' => 'cap', 'description' => 'des'],
            "<img  src='default' class='class' alt='alt' caption='cap' description='des' />"
        ];

        $providers[] = [
            ['src' => 'src', 'alt' => 'alt', 'caption' => 'cap', 'title' => 'tit',],
            "<img  src='default' class='class' alt='alt' caption='cap' title='tit' />"
        ];

        return $providers;
    }

    /**
     * @param $pictureData
     * @param $result
     * @dataProvider dataProvider
     */
    public function test_renderPictureArray($pictureData, $result)
    {

        $this->assertEquals(
            $result,
            (new ImageComponent($pictureData, 'default', 'class'))->render()
        );
    }
}
