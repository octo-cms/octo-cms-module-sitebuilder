<?php

namespace OctoCmsModule\Sitebuilder\Tests\Components\FooterContentHandlerComponent;

use OctoCmsModule\Sitebuilder\Entities\FooterContent;
use OctoCmsModule\Sitebuilder\Tests\Mocks\MenuServiceMock;
use OctoCmsModule\Sitebuilder\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Sitebuilder\View\Components\FooterContentHandlerComponent;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetDataTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Components\FooterContentHandlerComponent
 */
class GetDataTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];
        $providers[] = ['Menu', null, null];
        $providers[] = ['random', [['name' => 'name', 'value' => 'value']], ['name' => 'value']];

        return $providers;
    }

    /**
     * @param $contentType
     * @param $expectedData
     *
     * @throws ReflectionException
     * @dataProvider dataProvider
     */
    public function test_getDataMenu($contentType, $inputData, $expectedData)
    {

        /** @var array $data */


        /** @var array $content */
        $content = [
            'content_type' => $contentType,
            'content_id'   => 1,
            'size'         => 'size',
            'data'         => $inputData,
            'targets'      => [1, 2, 3],
            'layout'       => [],
        ];

        /** @var FooterContent $footerContent */
        $footerContent = FooterContent::factory()->create($content);

        /** @var FooterContentHandlerComponent $footerContentHandlerComponent */
        $footerContentHandlerComponent = new FooterContentHandlerComponent($footerContent, new SettingServiceMock(), new MenuServiceMock());


        $this->assertEquals(
            [
                'size'    => $content['size'],
                'data'    => $expectedData,
                'targets' => $content['targets'],
                'layout'  => $content['layout'],
            ],
            $this->invokeMethod(
                $footerContentHandlerComponent,
                'getData',
                ['content' => $footerContent]
            ),
        );
    }
}
