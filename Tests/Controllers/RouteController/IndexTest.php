<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\RouteController;

use Illuminate\Http\Response;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class IndexTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\RouteController
 */
class IndexTest extends TestCase
{



    public function test_indexRandomId()
    {
        $response = $this->json('GET', route('sitebuilder.any.index'));

        $response->assertStatus(Response::HTTP_OK);
    }
}
