<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SettingController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class UpdateSiteSettingsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SettingController
 */
class UpdateSiteSettingsTest extends TestCase
{


    public function dataProvider()
    {
        $providers = [];

        $data = [
            [
                'name'  => 'name-1',
                'value' => 'value-1',
            ],
            [
                'name'  => 'name-2',
                'value' => 'value-2',
            ],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields[0]['value']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }


    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_updateSiteSettingsRequest(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('sitebuilder.settings.update.site.settings'),
            $fields
        );

        $response->assertStatus($status);
    }
}
