<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController
 */
class StoreTest extends TestCase
{


    public function dataProvider()
    {
        $providers = [];

        $data = [
            'caption'      => 'caption',
            'sub_caption'  => 'sub_caption',
            'action_label' => 'label',
            'action_link'  => 'link',
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['caption']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['sub_caption']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['action_label']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['action_link']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_store(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('sitebuilder.slides.store'),
            $fields
        );

        $response->assertStatus($status);
    }
}
