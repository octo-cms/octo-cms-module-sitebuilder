<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController
 */
class UpdateTest extends TestCase
{


    public function dataProvider()
    {
        $providers = [];

        $data = [
            'caption'      => 'caption',
            'sub_caption'  => 'sub_caption',
            'action_label' => 'label',
            'action_link'  => 'link',
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['caption']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields['sub_caption']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields['action_label']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields['action_link']);
        $providers[] = [$fields, Response::HTTP_OK];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_update(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Slide $slide */
        $slide = Slide::factory()->create();

        $response = $this->json(
            'PUT',
            route('sitebuilder.slides.update', ['id' => $slide->id]),
            $fields
        );

        $response->assertStatus($status);
    }
}
