<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController
 */
class DatatableTest extends TestCase
{


    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        Slide::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.sitebuilder.slides.index');
    }
}
