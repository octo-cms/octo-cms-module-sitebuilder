<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Class ShowTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController
 */
class ShowTest extends TestCase
{


    public function test_show()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Slide $slide */
        $slide = Slide::factory()->create();

        $response = $this->json(
            'GET',
            route('sitebuilder.slides.show', ['id' => $slide->id,])
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            "data" => [
                'caption',
                'sub_caption',
                'action_label',
                'action_link',
                'pictures',
            ],
        ]);
    }

    public function test_showNotFound()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('sitebuilder.slides.show', ['id' => 100])
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
