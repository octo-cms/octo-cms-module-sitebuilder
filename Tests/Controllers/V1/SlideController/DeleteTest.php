<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Class DeleteTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\SlideController
 */
class DeleteTest extends TestCase
{


    public function test_delete()
    {
        /** @var Slide $slide */
        $slide = Slide::factory()->create()->first();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'DELETE',
            route('sitebuilder.slides.delete', ['id' => $slide->id])
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('slides', ['id' => $slide->id]);
    }
}
