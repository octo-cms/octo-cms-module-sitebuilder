<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetAllContentsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController
 */
class GetAllContentsTest extends TestCase
{



    public function test_store()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('sitebuilder.pages.ge.all.contents'));

        $response->assertStatus(Response::HTTP_OK);

    }
}
