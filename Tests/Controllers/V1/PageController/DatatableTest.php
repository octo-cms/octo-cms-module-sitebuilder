<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController
 */
class DatatableTest extends TestCase
{



    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        Page::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.sitebuilder.pages.index');
    }
}
