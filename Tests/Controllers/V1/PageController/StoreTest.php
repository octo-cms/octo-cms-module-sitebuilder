<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;


/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController
 */
class StoreTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            'change_freq'      => Page::CHANGE_FREQ_WEEKLY,
            'name'             => 'name',
            'published'        => true,
            'sitemap'          => true,
            'sitemap_priority' => 0.5,
            'type'             => Page::TYPE_STANDARD,
            'pageLangs'        => [
                [
                    'lang'             => 'it',
                    'url'              => 'url',
                    'meta_title'       => 'meta_title',
                    'meta_description' => 'meta_description',
                ],
            ],
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['change_freq']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['published']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['sitemap']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['sitemap_priority']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['type']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['pageLangs']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['pageLangs'][0]['lang']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['pageLangs'][0]['meta_title']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['pageLangs'][0]['meta_description']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['pageLangs'][0]['url']);
        $providers[] = [$fields, Response::HTTP_CREATED];


        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_store(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('sitebuilder.pages.store'),
            $fields
        );

        $response->assertStatus($status);
    }
}
