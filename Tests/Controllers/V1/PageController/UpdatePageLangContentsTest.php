<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;

/**
 * Class UpdatePageLangContentsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController
 */
class UpdatePageLangContentsTest extends TestCase
{


    public function dataProvider()
    {
        $providers = [];

        $data = [
            [
                'id'       => 1,
                'lang'     => 'it',
                'contents' => [
                    [
                        'content_id'   => null,
                        'content_type' => null,
                        'data'         => null,
                        'id'           => null,
                        'position'     => 1,
                        'type'         => PageLangContent::TARGET_TYPE_CUSTOM,
                    ],
                ],
            ],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['id']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['lang']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['contents']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['contents'][0]['content_id']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['contents'][0]['content_type']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['contents'][0]['data']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['contents'][0]['id']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['contents'][0]['position']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields[0]['contents'][0]['type']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_update(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Page $page */
        $page = Page::factory()->create()->first();

        $response = $this->json(
            'PUT',
            route('sitebuilder.pages.page.langs.update.contents', ['id' => $page->id]),
            $fields
        );

        $response->assertStatus($status);
    }
}
