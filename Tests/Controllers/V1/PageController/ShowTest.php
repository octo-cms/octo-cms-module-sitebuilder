<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class ShowTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController
 */
class ShowTest extends TestCase
{


    public function test_show()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Page $page */
        $page = Page::factory()->create();

        $response = $this->json(
            'GET',
            route('sitebuilder.pages.show', ['id' => $page->id])
        );

        $response->assertStatus(Response::HTTP_OK);
    }
}
