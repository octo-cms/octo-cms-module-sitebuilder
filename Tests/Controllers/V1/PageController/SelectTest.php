<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class SelectTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController
 */
class SelectTest extends TestCase
{


    public function test_store()
    {
        Sanctum::actingAs(self::createAdminUser());

        Page::factory()->create([
            'name' => 'test-name',
        ]);

        $response = $this->json(
            'POST',
            route('admin.select.sitebuilder.pages.index', [
                'query' => 'test',
            ])
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'name' => 'test-name',
        ]);

    }
}
