<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;

/**
 * Class PageLangContentsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageController
 */
class PageLangsTest extends TestCase
{



    public function test_pageLangContent()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Page $page */
        $page = Page::factory()
            ->has(
                PageLang::factory()
                    ->count(2)
                    ->has(
                        PageLangContent::factory()->for(BlockHtml::factory(), 'type')->count(2),
                        'contents'
                    )
            )
            ->create();

        $response = $this->json(
            'GET',
            route('sitebuilder.pages.page.langs', [
                'id' => $page->id,
            ])
        );

        $response->assertStatus(Response::HTTP_OK);

    }
}
