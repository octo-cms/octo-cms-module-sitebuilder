<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;

/**
 * Class DeleteTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController
 */
class DeleteTest extends TestCase
{


    public function test_delete()
    {
        /** @var PageRedirect $pageRedirect */
        $pageRedirect = PageRedirect::factory()->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'DELETE',
            route('sitebuilder.pages.redirects.show', ['id' => $pageRedirect->id])
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('page_redirects', [
            'id'      => $pageRedirect->id,
            'old_url' => $pageRedirect->old_url,
        ]);
    }
}
