<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController
 */
class StoreTest extends TestCase
{


    public function test_store()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Page $pageLang */
        $pageLang = PageLang::factory()->create();

        $response = $this->json(
            'POST',
            route('sitebuilder.pages.redirects.store'),
            [
                'page_lang_id' => $pageLang->id,
                'old_url'      => 'https://ciao.com',
            ]
        );

        $response->assertStatus(Response::HTTP_CREATED);
    }
}
