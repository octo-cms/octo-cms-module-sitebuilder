<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SearchPageLangByQueryTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController
 */
class SearchPageLangByQueryTest extends TestCase
{



    public function test_searchByTitle()
    {
        Sanctum::actingAs(self::createAdminUser());

        PageLang::factory()->create([
            'meta_title' => 'OctoCMS Title',
            'url'        => 'page/lang/url',
        ]);

        PageLang::factory()->count(2)->create();

        $response = $this->json(
            'POST',
            route('sitebuilder.pages.redirects.page.lang.search'),
            ['query' => 'octo']
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'meta_title' => 'OctoCMS Title',
            'url'        => 'page/lang/url',
        ]);
    }

    public function test_searchByURL()
    {
        Sanctum::actingAs(self::createAdminUser());

        PageLang::factory()->create([
            'meta_title' => 'OctoCMS Title',
            'url'        => 'page/lang/url',
        ]);

        PageLang::factory()->count(2)->create();

        $response = $this->json(
            'POST',
            route('sitebuilder.pages.redirects.page.lang.search'),
            ['query' => 'pag']
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'meta_title' => 'OctoCMS Title',
            'url'        => 'page/lang/url',
        ]);
    }
}
