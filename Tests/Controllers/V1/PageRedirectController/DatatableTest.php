<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController
 */
class DatatableTest extends TestCase
{



    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        PageRedirect::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.sitebuilder.pages.redirects.index');
    }
}
