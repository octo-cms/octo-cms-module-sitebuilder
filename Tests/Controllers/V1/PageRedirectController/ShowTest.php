<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ShowTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController
 */
class ShowTest extends TestCase
{



    public function test_show()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var PageRedirect $pageRedirect */
        $pageRedirect = PageRedirect::factory()->create();

        $response = $this->json(
            'GET',
            route('sitebuilder.pages.redirects.show', ['id' => $pageRedirect->id])
        );

        $response->assertStatus(Response::HTTP_OK);

    }
}
