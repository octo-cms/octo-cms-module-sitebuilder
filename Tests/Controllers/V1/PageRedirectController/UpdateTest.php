<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;

/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\PageRedirectController
 */
class UpdateTest extends TestCase
{



    public function test_update()
    {
        /** @var PageRedirect $pageRedirect */
        $pageRedirect = PageRedirect::factory()->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'PUT',
            route('sitebuilder.pages.redirects.update', ['id' => $pageRedirect->id]),
            [
                'old_url'      => 'path',
                'page_lang_id' => $pageRedirect->id,
            ]
        );

        $response->assertStatus(Response::HTTP_OK);
    }
}
