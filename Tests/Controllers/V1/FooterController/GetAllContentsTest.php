<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\FooterController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetAllContentsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\FooterController
 */
class GetAllContentsTest extends TestCase
{


    public function test_getAllContents()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('sitebuilder.footers.get.all.contents'));

        $response->assertJsonFragment(['blockEntity'=>[], 'blockHtml'=>[] , 'menus'=>[]]);
        $response->assertStatus(Response::HTTP_OK);
    }
}
