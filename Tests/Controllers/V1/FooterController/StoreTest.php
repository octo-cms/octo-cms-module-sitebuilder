<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\FooterController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\FooterController
 */
class StoreTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            [
                'content_align' => 'test',
                'enabled'       => true,
                'id'            => 1,
                'name'          => 'test-name',
                'order'         => 0,
            ],
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        $fields[0]['id'] = null;
        $providers[] = [$fields, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields[0]['content_align']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields[0]['enabled']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields[0]['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param $fields
     * @param $status
     *
     * @dataProvider dataProvider
     */
    public function test_store($fields, $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('sitebuilder.footers.store'),
            $fields
        );

        $response->assertStatus($status);
    }
}
