<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\FooterController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Sitebuilder\Entities\FooterContent;
use OctoCmsModule\Sitebuilder\Entities\Menu;

/**
 * Class IndexTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\FooterController
 */
class IndexTest extends TestCase
{


    public function test_index()
    {
        Menu::factory()->create();

        Footer::factory()
            ->count(3)
            ->has(
                FooterContent::factory()->state([
                    'content_type' => 'Menu',
                    'content_id'   => 1,
                ]),
                'contents'
            )
            ->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('sitebuilder.footers.index')
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'enabled',
                    'content_align',
                    'order',
                ],
            ],
        ]);

    }
}
