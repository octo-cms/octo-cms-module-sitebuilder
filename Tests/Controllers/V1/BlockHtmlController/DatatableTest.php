<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockHtmlController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockHtmlController
 */
class DatatableTest extends TestCase
{


    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        BlockHtml::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.sitebuilder.block.html.index');
    }
}
