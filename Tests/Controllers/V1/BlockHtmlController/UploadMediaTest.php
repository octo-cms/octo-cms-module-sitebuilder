<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockHtmlController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class UploadMediaTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockHtmlController
 */
class UploadMediaTest extends TestCase
{


    public function test_storeNewBlock()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('sitebuilder.block.html.upload.media', [
                'src' => 'test',
            ])
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure(
            ['src', 'webp']
        );
    }
}
