<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockHtmlController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockHtmlController
 */
class StoreTest extends TestCase
{


    public function test_storeNewBlock()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var array $data */
        $data = [
            'id'           => null,
            'blade'        => 'test-new-block',
            'instructions' => 'instructions',
            'settings'     => [['name' => 'test', 'type' => 'test']],
            'layout'       => ['layout' => 'value'],
            'src'          => 'src',
        ];

        $response = $this->json(
            'POST',
            route('sitebuilder.block.html.store', $data)
        );

        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJsonFragment([
            'id'           => 1,
            'blade'        => $data['blade'],
            'instructions' => $data['instructions'],
            'src'          => $data['src'],
            'layout'       => $data['layout'],
            'settings'     => $data['settings'],
        ]);
    }

    public function test_storeUpdateBlock()
    {
        /** @var BlockHtml $blockHtml */
        $blockHtml = BlockHtml::factory()->create([
            'blade'        => 'test-update-block',
            'instructions' => 'instructions',
        ])->first();

        Sanctum::actingAs(self::createAdminUser());

        /** @var array $data */
        $data = [
            'id'           => $blockHtml->id,
            'blade'        => 'test-updated-block',
            'instructions' => 'updated instructions',
            'settings'     => [['name' => 'test', 'type' => 'test']],
            'layout'       => ['layout' => 'value'],
            'src'          => 'src updated',
        ];

        $response = $this->json(
            'POST',
            route('sitebuilder.block.html.store', $data)
        );

        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJsonFragment(
            [
                'id'           => $blockHtml->id,
                'blade'        => $data['blade'],
                'instructions' => $data['instructions'],
                'src'          => $data['src'],
                'layout'       => $data['layout'],
                'settings'     => $data['settings'],
            ]
        );
    }

    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            'id'       => null,
            'blade'    => 'test-new-block',
            'settings' => [['name' => 'test', 'type' => 'test']],
        ];

        $fields = $data;
        unset($fields['blade']);
        $providers[] = $fields;

        $fields = $data;
        $fields['settings'] = [['type' => 'test']];
        $providers[] = $fields;

        $fields = $data;
        $fields['settings'] = [['name' => 'test']];
        $providers[] = $fields;

        return $providers;
    }

    /**
     * @param $data
     *
     * @return void
     * @dataProvider dataProvider
     */
    public function test_storeBadRequest($data)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('sitebuilder.block.html.store', $data)
        );

        $response->assertStatus(Response::HTTP_BAD_REQUEST);

    }
}
