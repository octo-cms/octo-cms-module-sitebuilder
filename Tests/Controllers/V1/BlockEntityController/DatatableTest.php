<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockEntityController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockEntityController
 */
class DatatableTest extends TestCase
{



    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        BlockEntity::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.sitebuilder.block.entities.index');
    }
}
