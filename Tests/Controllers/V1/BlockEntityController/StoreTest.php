<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockEntityController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockEntityController
 */
class StoreTest extends TestCase
{


    public function test_storeNewBlock()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var array $data */
        $data = [
            'id'           => null,
            'module'       => 'Blogs',
            'blade'        => 'blogs',
            'entity'       => 'Blog',
            'instructions' => 'instructions',
            'settings'     => [['name' => 'test', 'type' => 'test']],
            'layout'       => ['layout' => 'value'],
            'src'          => 'src',
        ];

        $response = $this->json(
            'POST',
            route('sitebuilder.block.entities.store', $data)
        );

        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJsonFragment(
            ['blade' => 'blogs']
        );

        $this->assertDatabaseHas('block_entities', [
            'id'           => 1,
            'module'       => $data['module'],
            'blade'        => $data['blade'],
            'entity'       => $data['entity'],
            'instructions' => $data['instructions'],
            'src'          => $data['src'],
            'layout'       => serialize($data['layout']),
        ]);

    }

    public function test_storeUpdateBlock()
    {
        /** @var BlockEntity $blockEntity */
        $blockEntity = BlockEntity::factory()->create([
            'module'       => 'Blogs',
            'blade'        => 'blogs',
            'entity'       => 'Blog',
            'instructions' => 'instructions',
            'src'          => 'src',
            'settings'     => [['name' => 'old', 'type' => 'test']],
            'layout'       => ['layout' => 'old value'],
        ])->first();

        Sanctum::actingAs(self::createAdminUser());

        /** @var array $data */
        $data = [
            'id'           => $blockEntity->id,
            'module'       => 'Posts',
            'blade'        => 'posts',
            'entity'       => 'Post',
            'instructions' => 'update instructions',
            'src'          => 'updated src',
            'settings'     => [['name' => 'updated', 'type' => 'test']],
            'layout'       => ['layout' => 'value'],
        ];

        $response = $this->json(
            'POST',
            route('sitebuilder.block.entities.store', $data)
        );

        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJsonFragment($data);

        $this->assertDatabaseHas('block_entities', [
            'id'           => $blockEntity->id,
            'module'       => $data['module'],
            'blade'        => $data['blade'],
            'entity'       => $data['entity'],
            'instructions' => $data['instructions'],
            'src'          => $data['src'],
            'layout'       => serialize($data['layout']),
        ]);
    }

    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            'id'       => null,
            'module'   => 'Posts',
            'blade'    => 'posts',
            'entity'   => 'Post',
            'settings' => [['name' => 'test', 'type' => 'test']],
        ];

        $fields = $data;
        unset($fields['blade']);
        $providers[] = $fields;

        $fields = $data;
        unset($fields['module']);
        $providers[] = $fields;

        $fields = $data;
        unset($fields['entity']);
        $providers[] = $fields;

        return $providers;
    }

    /**
     * @param $data
     *
     * @return void
     * @dataProvider dataProvider
     */
    public function test_storeBadRequest($data)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('sitebuilder.block.entities.store', $data)
        );

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
