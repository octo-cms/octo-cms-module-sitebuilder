<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockEntityController;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Class SlideBlockEntityIdsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\BlockEntityController
 */
class SlideBlockEntityIdsTest extends TestCase
{


    public function test_blockEntityIds()
    {
        Sanctum::actingAs(self::createAdminUser());

        Slide::factory()->count(15)->create();

        $response = $this->json(
            'POST',
            route('sitebuilder.slides.block.entity.ids'),
            [
                'currentPage' => 1,
                'rowsInPage'  => 10,
                'excludedIds' => []
            ]
        );

        $response->assertStatus(Response::HTTP_OK);

        $content = json_decode($response->getContent(), true);

        $this->assertEquals(15, Arr::get($content, 'total', 0));

        $this->assertEquals(1, Arr::get($content, 'currentPage', 0));

        $this->assertEquals(10, Arr::get($content, 'rowsInPage', 0));

        $this->assertNotEmpty(Arr::get($content, 'collection', []));

    }
}
