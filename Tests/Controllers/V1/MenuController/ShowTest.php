<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Menu;

/**
 * Class ShowTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController
 */
class ShowTest extends TestCase
{



    public function test_show()
    {
        /** @var Menu $menu */
        $menu = Menu::factory()->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('sitebuilder.menu.show', ['id' => $menu->id])
        );

        $response->assertStatus(Response::HTTP_OK);

    }
}
