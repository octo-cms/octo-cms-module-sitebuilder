<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Menu;

/**
 * Class IndexTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController
 */
class IndexTest extends TestCase
{



    public function test_index()
    {

        Menu::factory()->count(3)->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('sitebuilder.menu.index')
        );

        $response->assertStatus(Response::HTTP_OK);


        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'blade',
                ],
            ],
        ]);

    }
}
