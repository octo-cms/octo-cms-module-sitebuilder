<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController
 */
class StoreTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            'blade'     => 'blade-test',
            'name'      => 'name-test',
            'menuLangs' => [
                [ 'lang'  => 'it', 'title' => null ]
            ]
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['blade']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['menuLangs']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param $fields
     * @param $status
     *
     * @dataProvider dataProvider
     */
    public function test_store($fields, $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('sitebuilder.menu.store'),
            $fields
        );

        $response->assertStatus($status);
    }
}
