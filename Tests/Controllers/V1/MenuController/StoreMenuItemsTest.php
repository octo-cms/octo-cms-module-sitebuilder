<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Menu;

/**
 * Class StoreMenuItemsTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController
 */
class StoreMenuItemsTest extends TestCase
{


    public function test_store()
    {

        Sanctum::actingAs(self::createAdminUser());

        /** @var Menu $menu */
        $menu = Menu::factory()->create();

        $data = [
            0 => [
                'data'          => [],
                'id'            => null,
                'menuItemLangs' => [0 => ['lang' => 'it', 'label' => 'test']],
                'order'         => 1,
                'parent_id'     => 0,
                'type'          => 'parent',
            ],
        ];

        $response = $this->json(
            'POST',
            route('sitebuilder.menu.store.items', [
                'id' => $menu->id,
            ]),
            $data
        );

        $response->assertStatus(Response::HTTP_CREATED);

    }
}
