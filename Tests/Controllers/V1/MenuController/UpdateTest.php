<?php

namespace OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Sitebuilder\Entities\Menu;

/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Controllers\V1\MenuController
 */
class UpdateTest extends TestCase
{


    public function dataProvider()
    {
        $providers = [];

        $data = [
            'menuLangs' => [
                [
                    'lang'  => 'it',
                    'title' => 'titolo',
                ],
                [
                    'lang'  => 'en',
                    'title' => 'titolo',
                ],
            ],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['menuLangs']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['menuLangs'][0]['title']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['menuLangs'][0]['lang']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param $fields
     * @param $status
     *
     * @dataProvider dataProvider
     */
    public function test_store($fields, $status)
    {
        /** @var Menu $menu */
        $menu = Menu::factory()->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'PUT',
            route('sitebuilder.menu.update', ['id' => $menu->id]),
            $fields
        );

        $response->assertStatus($status);
    }
}
