<?php

namespace OctoCmsModule\Sitebuilder\Tests\Utils\ContentUtils;

use OctoCmsModule\Sitebuilder\Utils\ContentUtils;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ParseBlockDataTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Utils\BlockUtils
 */
class ParseBlockDataTest extends TestCase
{
    public function test_parseBlockData()
    {
        /** @var array $data */
        $data = [
            [
                'name'  => 'name-1',
                'value' => 'value-1',
            ],
            [
                'name'  => 'name-2',
                'value' => 'value-2',
            ],
        ];

        $this->assertEquals(
            [
                $data[0]['name'] => $data[0]['value'],
                $data[1]['name'] => $data[1]['value'],
            ],
            ContentUtils::parseBlockData($data)
        );
    }

    public function test_parseBlockDataEmpty()
    {
        $this->assertEquals([], ContentUtils::parseBlockData([]));
    }
}
