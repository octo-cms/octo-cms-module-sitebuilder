<?php

namespace OctoCmsModule\Sitebuilder\Tests\Utils\ContentUtils;

use OctoCmsModule\Sitebuilder\Utils\ContentUtils;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ParseBlockLayoutTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Utils\ContentUtils
 */
class ParseBlockLayoutTest extends TestCase
{
    public function test_parseBlockLayout()
    {
        /** @var array $layout */
        $layout = [
            [
                'name'     => 'name-1',
                'type'     => 'select',
                'selected' => [
                    'name'  => 'selected-1',
                    'value' => 'selected-1',
                ],
            ],
            [
                'name'     => 'name-2',
                'type'     => 'boolean',
                'selected' => true,
            ],
        ];

        $this->assertEquals(
            [
                $layout[0]['name'] => $layout[0]['selected']['value'],
                $layout[1]['name'] => $layout[1]['selected'],
            ],
            ContentUtils::parseBlockLayout($layout)
        );
    }

    public function test_parseBlockLayoutEmpty()
    {
        $this->assertEquals([], ContentUtils::parseBlockLayout([]));
    }
}
