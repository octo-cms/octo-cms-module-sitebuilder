<?php

namespace OctoCmsModule\Sitebuilder\Tests\Utils\LivewireUtils;

use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;
use OctoCmsModule\Core\Tests\TestCase;

/**
 *
 * Class GetCustomEntitiesTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Utils\LivewireUtils
 */
class GetCustomEntitiesTest extends TestCase
{


    /**
     * @throws \ReflectionException
     */
    public function test_getCustomEntities()
    {
        User::factory()->count(4)->create();

        $utils = new LivewireUtils();

        $targets = [
            'type'   => 'custom',
            'values' =>
                [
                    ['id' => 1],
                    ['id' => 3],
                    ['id' => 2],
                ],
        ];

        $result = $this->invokeMethod(
            $utils,
            'getCustomEntities',
            ['builder' => User::query(), 'targets' => $targets]
        );

        $this->assertCount(
            3,
            $result
        );

        $this->assertEquals(
            1,
            $result[0]['id']
        );

        $this->assertEquals(
            3,
            $result[1]['id']
        );

        $this->assertEquals(
            2,
            $result[2]['id']
        );
    }
}
