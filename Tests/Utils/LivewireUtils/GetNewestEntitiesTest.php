<?php

namespace OctoCmsModule\Sitebuilder\Tests\Utils\LivewireUtils;

use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetNewestEntitiesTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Utils\LivewireUtils
 */
class GetNewestEntitiesTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_getNewestEntities()
    {
        User::factory()->count(2)->create();

        $utils = new LivewireUtils();

        $targets = [
            'type'   => 'newest',
            'values' => ['limit' => 2],
        ];

        $result = $this->invokeMethod(
            $utils,
            'getNewestEntities',
            ['builder' => User::query(), 'targets' => $targets]
        );

        $this->assertCount(
            2,
            $result
        );
    }
}
