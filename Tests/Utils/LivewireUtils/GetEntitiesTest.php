<?php

namespace OctoCmsModule\Sitebuilder\Tests\Utils\LivewireUtils;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Catalog\Entities\Supplier;
use OctoCmsModule\Services\Entities\Service;
use OctoCmsModule\Sitebuilder\Entities\Slide;
use OctoCmsModule\Sitebuilder\Utils\LivewireUtils;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetEntitiesTest
 *
 * @package OctoCmsModule\Sitebuilder\Tests\Utils\LivewireUtils
 */
class GetEntitiesTest extends TestCase
{


    public function test_getCustomEntities()
    {
        /** @var string $tag */
        $tag = 'main';

        /** @var Service $slideWithMainTag */
        $slideWithMainTag = Slide::factory()->count(3)->create()->first();

        $slideWithMainTag->tag($tag);

        /** @var LivewireUtils $utils */
        $utils = new LivewireUtils();

        /** @var array $targets */
        $targets = [
            'type'   => 'tag',
            'values' => ['tag' => $tag],
        ];

        /** @var Collection $result */
        $result = $utils->getEntities(Slide::query(), $targets);

        $this->assertCount(
            1,
            $result
        );

        $this->assertEquals(
            1,
            $result[0]['id']
        );
    }
}
