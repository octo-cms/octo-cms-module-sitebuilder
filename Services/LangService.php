<?php

namespace OctoCmsModule\Sitebuilder\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Sitebuilder\Interfaces\LangServiceInterface;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;

/**
 * Class LangService
 *
 * @package OctoCmsModule\Sitebuilder\Services
 * @author  danielepasi
 */
class LangService implements LangServiceInterface
{

    /** @var SettingServiceInterface */
    protected $settingService;

    /**
     * LangService constructor.
     *
     * @param SettingServiceInterface $settingService
     */
    public function __construct(SettingServiceInterface $settingService)
    {
        $this->settingService = $settingService;
    }

    /**
     * @inheritDoc
     */
    public function getLangFromPath(string $path): string
    {

        if ($this->settingService->getSettingByName(SettingNameConst::MULTILANGUAGE)) {

            /** @var array $languages */
            if (!empty($languages = $this->settingService->getSettingByName(
                SettingNameConst::LANGUAGES
            ))) {
                preg_match_all(
                    '/^(' . implode('|', $languages) . ')/m',
                    $path,
                    $matches,
                    PREG_SET_ORDER,
                    0
                );

                if (!empty($lang = Arr::get($matches, '0.0', null))) {
                    return $lang;
                }
            }
        }

        return $this->settingService->getSettingByName(SettingNameConst::LOCALE);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function removeLangPrefixFromPath(string $path): string
    {
        if (!$this->settingService->getSettingByName(SettingNameConst::MULTILANGUAGE)) {
            return Str::replaceFirst('/', '', Str::start($path, '/'));
        }

        /** @var array $languages */
        $languages = $this->settingService->getSettingByName(
            SettingNameConst::LANGUAGES
        );

        preg_match_all(
            '/^(' . implode('|', $languages) . ')/m',
            $path,
            $matches,
            PREG_SET_ORDER,
            0
        );

        $replaced =  Str::replaceFirst(Arr::get($matches, '0.0', ''), '', $path);

        return Str::replaceFirst('/', '', Str::start($replaced, '/'));
    }
}
