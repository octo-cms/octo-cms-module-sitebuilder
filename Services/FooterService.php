<?php

namespace OctoCmsModule\Sitebuilder\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use OctoCmsModule\Sitebuilder\Interfaces\FooterServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Sitebuilder\Entities\FooterContent;
use OctoCmsModule\Core\Exceptions\OctoCmsException;
use Throwable;

/**
 * Class FooterService
 *
 * @package OctoCmsModule\Sitebuilder\Services
 */
class FooterService implements FooterServiceInterface
{
    /**
     * @param array $fields
     *
     * @return bool
     * @throws OctoCmsException
     * @throws Throwable
     */
    public function saveFooters(array $fields): bool
    {
        DB::beginTransaction();

        Footer::whereIn(
            'id',
            array_diff(
                Footer::all()->pluck('id')->toArray(),
                Arr::pluck($fields, 'id')
            )
        )->delete();

        foreach ($fields as &$data) {
            /**
 * @var Footer $footer
*/
            $footer = Footer::findOrNew(Arr::get($data, 'id', null));

            $footer->fill($data);
            $footer->save();
            $footer->refresh();

            data_set($data, 'contents.*.footer_id', $footer->id);
        }

        $contents = $this->groupContents($fields);

        if (!$this->saveFootersContents($contents)) {
            DB::rollBack();
            throw new OctoCmsException('Footer contents not saved');
        }

        DB::commit();

        return true;
    }

    /**
     * @param array $fields
     *
     * @return array
     */
    protected function groupContents(array $fields)
    {
        $contents = [];
        foreach ($fields as $data) {
            $contents = Arr::collapse([$contents, Arr::get($data, 'contents', [])]);
        }
        return $contents;
    }

    /**
     * @param array $contents
     *
     * @return bool
     */
    protected function saveFootersContents(array $contents = [])
    {

        FooterContent::whereIn(
            'id',
            array_diff(
                FooterContent::all()->pluck('id')->toArray(),
                Arr::pluck($contents, 'id')
            )
        )->delete();

        foreach ($contents as $content) {

            /**
 * @var FooterContent $footerContent
*/
            $footerContent = FooterContent::findOrNew(Arr::get($content, 'id', null));

            $footerContent->fill($content);
            if (!$footerContent->save()) {
                return false;
            }
        }

        return true;
    }
}
