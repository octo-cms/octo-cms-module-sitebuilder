<?php

namespace OctoCmsModule\Sitebuilder\Services;

use Illuminate\Support\Arr;
use OctoCmsModule\Sitebuilder\Interfaces\PageRedirectServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;

/**
 * Class PageRedirectService
 *
 * @package OctoCmsModule\Sitebuilder\Services
 */
class PageRedirectService implements PageRedirectServiceInterface
{
    /**
     * @param PageLang $pageLang
     * @param array    $fields
     *
     * @return PageRedirect
     */
    public function storePageRedirect(PageLang $pageLang, array $fields): PageRedirect
    {
        /**
 * @var PageRedirect $pageRedirect
*/
        $pageRedirect = new PageRedirect(
            [
            'old_url' => Arr::get($fields, 'old_url', '')
            ]
        );

        $pageRedirect->pageLang()->associate($pageLang)->save();

        return $pageRedirect;
    }

    /**
     * @param PageRedirect $pageRedirect
     * @param array        $fields
     *
     * @return PageRedirect
     */
    public function updatePageRedirect(PageRedirect $pageRedirect, array $fields): PageRedirect
    {
        $pageRedirect->fill($fields)->save();

        return $pageRedirect;
    }

    /**
     * @param string $old_url
     *
     * @return PageLang|null
     */
    public function getRedirectPageLang(string $old_url):? PageLang
    {
        /**
 * @var PageRedirect $pageRedirect
*/
        $pageRedirect = PageRedirect::where('old_url', '=', $old_url)->first();

        if (empty($pageRedirect)) {
            return null;
        }

        return $pageRedirect->pageLang;
    }
}
