<?php

namespace OctoCmsModule\Sitebuilder\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use OctoCmsModule\Core\Constants\CacheTagConst;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Services\CacheService;
use OctoCmsModule\Sitebuilder\DTO\PageLangDataDTO;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Interfaces\RouteServiceInterface;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class RouteService
 *
 * @package OctoCmsModule\Sitebuilder\Services
 */
class RouteService implements RouteServiceInterface
{
    /**
     * @var SettingServiceInterface
     */
    protected $settingService;

    /**
     * RouteService constructor.
     *
     * @param SettingServiceInterface $settingService
     */
    public function __construct(
        SettingServiceInterface $settingService
    ) {
        $this->settingService = $settingService;
    }

    /**
     * @param string $path
     *
     * @return PageLang|Builder|Model|object|null
     */
    public function searchPageLang(string $path): ?PageLang
    {
        $pageLang = PageLang::where('lang', '=', app()->getLocale())
            ->whereHas(
                'Page',
                function (Builder $builder) {
                    $builder->where('published', '=', true);
                }
            );

        if (empty($path)) {
            $pageLang->whereHas(
                'Page',
                function (Builder $builder) {
                    $builder->where('type', '=', Page::TYPE_HOMEPAGE);
                }
            );
        } else {
            $pageLang->where('url', '=', $path);
        }

        return $pageLang->with('page', 'page.pageable')
            ->with('contents', 'contents.type')
            ->first();
    }

    /**
     * @param  PageLang $pageLang
     * @return PageLangDataDTO
     * @throws InvalidArgumentException
     */
    public function getPageLangData(PageLang $pageLang): PageLangDataDTO
    {
        $pageLangData = new PageLangDataDTO();
        $pageLangData->template = strtolower(config('octo-cms.template.module'));
        $pageLangData->pageLang = $pageLang;
        $pageLangData->contents = $pageLang->contents->sortBy('position');
        $pageLangData->metaTitle = $this->getMetaTitle($pageLang);
        $pageLangData->metaDescription = $this->getMetaDescription($pageLang);
        $pageLangData->gtmSettings = $this->settingService->getSettingByName('fe_gtm');
        $pageLangData->logo = $this->settingService->getSettingByName('fe_logo');
        $pageLangData->footers = $this->getFooters();
        $pageLangData->optimizeSettings = $this->settingService->getSettingByName('fe_google_optimize');
        $pageLangData->faviconsData = $this->settingService->getSettingByName(SettingNameConst::FE_FAVICON_DATA);

        return $pageLangData;
    }

    /**
     * @param Page $page
     *
     * @return array
     */
    public function getViews(Page $page): array
    {
        switch ($page->type) {
            case Page::TYPE_PRODUCT:
                $view = 'product.product';
                $module = strtolower(config('octo-cms.catalog.module'));
                break;
            case Page::TYPE_CATALOG:
                $view = 'catalog.catalog';
                $module = strtolower(config('octo-cms.catalog.module'));
                break;
            case Page::TYPE_NEWS:
                $view = 'blog.news';
                $module = strtolower(config('octo-cms.blog.module'));
                break;
            default:
                $view = 'index';
                $module = 'sitebuilder';
                break;
        }

        return [
            "$view",
            strtolower(config('octo-cms.template.module')) . "::$view",
            "$module::$view",
            "sitebuilder::$view",
        ];
    }

    /**
     * @param PageLang $pageLang
     *
     * @return string
     */
    protected function getMetaTitle(PageLang $pageLang): string
    {
        return $pageLang->meta_title ?: $this->settingService->getSettingByName('fe_title');
    }

    /**
     * @param PageLang $pageLang
     *
     * @return string
     */
    protected function getMetaDescription(PageLang $pageLang): string
    {
        return $pageLang->meta_description ?: $this->settingService->getSettingByName('fe_description');
    }


    /**
     * @return Collection
     * @throws InvalidArgumentException
     */
    protected function getFooters() : Collection
    {

        $footers = CacheService::get(CacheTagConst::FOOTER, CacheTagConst::FOOTER);

        if (!empty($footers)) {
            return $footers;
        }

        $footers = Footer::where('enabled', '=', true)
            ->orderBy('order')
            ->get();

        $footers->load(
            ['contents' => function (HasMany $query) {
                $query->orderBy('order');
            }],
            'contents.type'
        );

        CacheService::set(CacheTagConst::FOOTER, CacheTagConst::FOOTER, $footers);

        return $footers;
    }
}
