<?php

namespace OctoCmsModule\Sitebuilder\Services;

use OctoCmsModule\Sitebuilder\Interfaces\PictureServiceInterface;
use OctoCmsModule\Core\Services\PictureService as PictureServiceCore;

/**
 * Class PictureService
 *
 * @package OctoCmsModule\Sitebuilder\Services
 */
class PictureService extends PictureServiceCore implements PictureServiceInterface
{

}
