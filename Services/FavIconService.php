<?php

namespace OctoCmsModule\Sitebuilder\Services;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use OctoCmsModule\Core\Jobs\LoggingJob;
use OctoCmsModule\Sitebuilder\Interfaces\FavIconServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\SettingServiceInterface;
use Octopus\Logger\DTOs\LogMessageDTO;
use stdClass;

/**
 * Class FavIconService
 *
 * @category Octo
 * @package  OctoCmsModule\Sitebuilder\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class FavIconService implements FavIconServiceInterface
{

    /**
     * SettingServiceInterface
     *
     * @var SettingServiceInterface
     */
    protected $settingService;

    /**
     * Sizes
     *
     * @var int[]
     */
    protected $sizes;

    /**
     * Urls
     *
     * @var array
     */
    protected $urls;

    /**
     * Base Url
     *
     * @var string
     */
    protected $gcsBaseUrl;

    /**
     * FavIconService constructor.
     *
     * @param SettingServiceInterface $settingService SettingServiceInterface
     */
    public function __construct(SettingServiceInterface $settingService)
    {
        $this->settingService = $settingService;
        $this->sizes = [57, 60, 72, 76, 114, 120, 144, 152, 180, 192, 32, 96, 16];
        $this->urls = [];

        $this->gcsBaseUrl = PictureService::BUCKET_BASE_URL . '/' . env('GOOGLE_CLOUD_STORAGE_BUCKET');
    }

    /**
     * Name getSizes
     *
     * @return int[]
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * Name createManifestJson
     *
     * @return void
     */
    private function createManifestJson()
    {

        $sizes = [36, 48, 72, 96, 144, 192];
        $icons = [];

        foreach ($sizes as $size) {
            $ico = new stdClass();
            $ico->src = $this->gcsBaseUrl . "/" . $size . "-favicon.png";
            $ico->sizes = $size . "x" . $size;
            $ico->type = "image/png";
            $ico->density = "1.0";

            $icons[] = $ico;
        }

        $obj = new stdClass();
        $obj->name = "App";
        $obj->icons = $icons;

        $name = "manifest.json";
        $path = storage_path("temp") . "/" . $name;

        file_put_contents($path, json_encode($obj));

        Storage::disk('gcs')->putFileAs('favicons', new File($path), $name);
        @unlink($path);

        $this->urls['manifest'] = $this->gcsBaseUrl . "/favicons/" . $name;
    }

    /**
     * Name createFileIco
     *
     * @param string $file File
     *
     * @return void
     */
    private function createFileIco(string $file)
    {
        $img = Image::make($file);
        $img->fit(16);
        $imagick = $img->getCore();
        $name = "favicon.ico";
        $path = storage_path("temp") . "/" . $name;
        imagebmp($imagick, $path);

        Storage::disk('gcs')->putFileAs('favicons', new File($path), $name);
        @unlink($path);
        imagedestroy($imagick);

        $this->urls['ico'] = $this->gcsBaseUrl . "/favicons/" . $name;
    }

    /**
     * Name createFromFile
     *
     * @param string $file File
     *
     * @return bool
     */
    public function createFromFile(string $file): bool
    {

        if (!file_exists($file)) {
            LoggingJob::dispatch(
                new LogMessageDTO(
                    [
                    'action' => 'create favicon',
                    'body'   => 'the image ' . $file . " does not exist",
                    ],
                    __CLASS__,
                    __FUNCTION__
                )
            );

            return false;
        }

        foreach ($this->sizes as $size) {
            $img = Image::make($file);
            $img->fit($size)->encode("png");

            $name = $size . "-favicon.png";
            $path = storage_path("temp") . "/" . $name;
            $img->save($path);

            Storage::disk('gcs')->putFileAs('favicons', new File($path), $name);

            @unlink($path);

            $this->urls['size' . $size . "x" . $size] = $this->gcsBaseUrl . "/favicons/" . $name;
        }

        $this->createFileIco($file);

        $this->createManifestJson();

        LoggingJob::dispatch(
            new LogMessageDTO(
                [
                'action' => 'create favicon',
                'body'   => 'Create all icons',
                ],
                __CLASS__,
                __FUNCTION__
            )
        );

        $this->settingService->saveSiteFaviconData($this->urls);

        return true;
    }

    /**
     * Name createFromUrl
     *
     * @param string $url Url
     *
     * @return bool
     */
    public function createFromUrl(string $url): bool
    {
        $result = false;
        $msg = "The url is empty";

        if ($url != '') {
            if (!Str::startsWith($url, "http")) {
                $url = PictureService::BUCKET_BASE_URL . '/' . env('GOOGLE_CLOUD_STORAGE_BUCKET') . '/' . $url;
            }

            $path = storage_path("temp") . "/" . uniqid('favicon', true) . '.png';

            $img = null;
            $msg = "Can not download image from: " . $url;

            try {
                $img = Image::make($url)->save($path);
            } catch (\Exception $ex) {
                $img = null;
            }

            if (($img != null) && (file_exists($path))) {
                $result = $this->createFromFile($path);
                $msg = $result ? "" : "can not create the favicons from url: ".$url;
                @unlink($path);
            }
        }


        if (!$result) {
            LoggingJob::dispatch(
                new LogMessageDTO(
                    [
                    'action' => 'create favicon from url',
                    'body'   => 'Error: ' . $msg,
                    ],
                    __CLASS__,
                    __FUNCTION__
                )
            );
        }

        return $result;
    }
}
