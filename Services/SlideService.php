<?php

namespace OctoCmsModule\Sitebuilder\Services;

use OctoCmsModule\Sitebuilder\Interfaces\SlideServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Class SlideService
 *
 * @package OctoCmsModule\Sitebuilder\Services
 */
class SlideService implements SlideServiceInterface
{
    /**
     * @param Slide $slide
     * @param array $fields
     *
     * @return Slide
     */
    public function saveSlide(Slide $slide, array $fields): Slide
    {
        $slide->fill($fields);
        $slide->save();

        return $slide;
    }
}
