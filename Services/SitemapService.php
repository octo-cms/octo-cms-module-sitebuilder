<?php

namespace OctoCmsModule\Sitebuilder\Services;

use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Interfaces\SitemapServiceInterface;
use Spatie\Sitemap\Tags\Url;

/**
 * Class SitemapService
 *
 * @package OctoCmsModule\Sitebuilder\Services
 */
class SitemapService implements SitemapServiceInterface
{

    /**
     * @param Page   $page
     * @param string $fallbackLocale
     * @param array  $languages
     *
     * @return Url|null
     */
    public function generateUrlInstance(Page $page, string $fallbackLocale, array $languages):? Url
    {
        /**
 * @var PageLang $defaultPageLang
*/
        $defaultPageLang = $page->pageLangs->where('lang', '=', $fallbackLocale)->first();

        if (empty($defaultPageLang)) {
            return null;
        }

        /**
 * @var Url $urlInstance
*/
        $urlInstance = Url::create($defaultPageLang->url)
            ->setLastModificationDate($defaultPageLang->updated_at)
            ->setChangeFrequency($page->change_freq)
            ->setPriority($page->sitemap_priority);

        foreach ($languages as $language) {
            if ($language === $fallbackLocale) {
                continue;
            }

            /**
 * @var PageLang $pageLang
*/
            $pageLang = $page->pageLangs->where('lang', '=', $language)->first();

            $urlInstance->addAlternate($pageLang->url, $pageLang->lang);
        }

        return $urlInstance;
    }
}
