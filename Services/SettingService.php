<?php

namespace OctoCmsModule\Sitebuilder\Services;

use Illuminate\Support\Arr;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Services\SettingService as SettingServiceCore;
use OctoCmsModule\Sitebuilder\Interfaces\SettingServiceInterface;
use OctoCmsModule\Sitebuilder\Jobs\CreateFavIconJob;

/**
 * Class SettingService
 *
 * @category Octo
 * @package  OctoCmsModule\Sitebuilder\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SettingService extends SettingServiceCore implements SettingServiceInterface
{
    /**
     * Name saveSiteSettings
     *
     * @param array $fields Array
     *
     * @return void
     */
    public function saveSiteSettings(array $fields)
    {
        foreach ($fields as $settingData) {
            $settingName = Arr::get($settingData, 'name', null);

            $settingValue = Arr::get($settingData, 'value', null);

            if ($settingName === SettingNameConst::FE_FAVICON) {
                $this->saveSiteFavicon($settingValue);
                continue;
            }

            Setting::updateOrCreate(
                ['name' => $settingName],
                ['value' => $settingValue]
            );
        }
    }

    /**
     * Name saveSiteFavicon
     *
     * @param mixed $faviconSettings Favicon Settings
     *
     * @return void
     */
    protected function saveSiteFavicon($faviconSettings)
    {
        Setting::updateOrCreate(
            ['name' => SettingNameConst::FE_FAVICON ],
            ['value' => $faviconSettings]
        );

        CreateFavIconJob::dispatch();
    }

    /**
     * Name saveSiteFaviconData
     *
     * @param mixed $faviconData Favicon Data
     *
     * @return void
     */
    public function saveSiteFaviconData($faviconData)
    {
        Setting::updateOrCreate(
            ['name' => SettingNameConst::FE_FAVICON_DATA ],
            ['value' => $faviconData]
        );
    }
}
