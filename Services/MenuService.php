<?php

namespace OctoCmsModule\Sitebuilder\Services;

use OctoCmsModule\Core\Services\CacheService;
use OctoCmsModule\Sitebuilder\DTO\MenuItemDTO;
use OctoCmsModule\Sitebuilder\Entities\MenuLang;
use OctoCmsModule\Core\Exceptions\OctoCmsException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use OctoCmsModule\Sitebuilder\Interfaces\MenuServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\MenuItemLang;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Utils\LanguageUtils;
use Throwable;

/**
 * Class MenuService
 *
 * @category Octo
 * @package  OctoCmsModule\Sitebuilder\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class MenuService implements MenuServiceInterface
{

    public const CACHE_TAG      = 'menu';
    public const CACHE_MENU_KEY = 'main-menu-items';

    /**
     * Name storeMenuItems
     *
     * @param Menu  $menu   Menu
     * @param array $fields Array
     *
     * @return mixed|Menu
     * @throws OctoCmsException
     * @throws Throwable
     */
    public function storeMenuItems(Menu $menu, array $fields)
    {
        DB::beginTransaction();

        // remove old ids
        $menu->items()
            ->whereIn(
                'id',
                array_diff(
                    $menu->items->pluck('id')->toArray(),
                    Arr::pluck($fields, 'id')
                )
            )->delete();

        foreach ($fields as $item) {
            if (!empty($item['id'])) {
                /**
                 * MenuItem
                 *
                 * @var MenuItem $menuItem
                 */
                $menuItem = $menu->items->find($item['id']);
                if (empty($menuItem)) {
                    throw new OctoCmsException('Menu Items not found');
                }
            } else {
                /**
                 * MenuItem
                 *
                 * @var MenuItem $menuItem
                 */
                $menuItem = new MenuItem();
                $menuItem->menu()->associate($menu);
            }

            $menuItem->fill($item);

            if ($menuItem->type == MenuItem::TYPE_PAGE) {
                $menuItem->page_id = Arr::get($item, 'data.page.value', null);
            }

            $menuItem->save();

            foreach (Arr::get($item, 'menuItemLangs', []) as $itemLang) {
                MenuItemLang::updateOrCreate(
                    [
                        'menu_item_id' => $menuItem->id,
                        'lang'         => $itemLang['lang'],
                    ],
                    [
                        'label' => $itemLang['label'],
                    ]
                );
            }
        }

        DB::commit();

        CacheService::flushCacheByTag(self::CACHE_TAG);

        return $menu;
    }

    /**
     * Name saveMenuLangs
     *
     * @param Menu  $menu      Menu
     * @param array $menuLangs menu Langs
     *
     * @return void
     */
    protected function saveMenuLangs(Menu $menu, array $menuLangs)
    {
        foreach ($menuLangs as $menuLang) {
            MenuLang::updateOrCreate(
                [
                    'menu_id' => $menu->id,
                    'lang'    => Arr::get($menuLang, 'lang', ''),
                ],
                [
                    'title' => Arr::get($menuLang, 'title', ''),
                ]
            );
        }
    }

    /**
     * Name saveMenu
     *
     * @param Menu  $menu   menu
     * @param array $fields Array
     *
     * @return Menu
     */
    public function saveMenu(Menu $menu, array $fields): Menu
    {

        $this->saveMenuLangs($menu, Arr::get($fields, 'menuLangs', []));

        return $menu;
    }

    /**
     * Name createMenuItem
     *
     * @param mixed $item Menu Item
     *
     * @return MenuItemDTO
     */
    private function createMenuItem($item)
    {
        $menuItem = new MenuItemDTO();
        $menuItem->label = LanguageUtils::getLangValue($item->menuItemLangs, 'label');
        $menuItem->type = $item->type;
        $menuItem->id = $item->id;
        $menuItem->children = [];

        switch ($item->type) {
            case MenuItem::TYPE_PAGE:
                $page = Page::with('pageLangs')->find($item->data['page']['value']);
                $menuItem->link = LanguageUtils::getLangValue($page->pageLangs, 'url');
                break;

            case MenuItem::TYPE_EXTERNAL:
                $menuItem->link = $item->data['link'];
                break;

            case MenuItem::TYPE_PARENT:
            default:
                break;
        }

        return $menuItem;
    }


    /**
     * Name getMainMenu
     *
     * @return array
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getMainMenu(): array
    {
        $menuItems = [];

        $menu = Menu::whereName(Menu::MAIN_MENU)->first();

        $cachedMenuItems = CacheService::get(self::CACHE_TAG, self::CACHE_MENU_KEY);
        if (is_null($cachedMenuItems)) {
            $items = MenuItem::getTree($menu->id);

            /**
             * MenuItem
             *
             * @var MenuItem $item
             */
            foreach ($items as $item) {
                $menuItem = $this->createMenuItem($item);

                if ($item->children->count() > 0) {
                    $menuItems2 = [];
                    $items2 = $item->children;

                    foreach ($items2 as $item2) {
                        $menuItem2 = $this->createMenuItem($item2);

                        if ($item2->children->count() > 0) {
                            $menuItems3 = [];
                            $items3 = $item2->children;

                            foreach ($items3 as $item3) {
                                $menuItem3 = $this->createMenuItem($item3);
                                $menuItems3[] = $menuItem3;
                            }

                            $menuItem2->children = $menuItems3;
                        }

                        $menuItems2[] = $menuItem2;
                    }

                    $menuItem->children = $menuItems2;
                }

                $menuItems[] = $menuItem;
            }

            CacheService::set(self::CACHE_TAG, self::CACHE_MENU_KEY, $menuItems);
        } else {
            $menuItems = $cachedMenuItems;
        }

        return [
            'blade' => $menu->blade,
            'items' => $menuItems,
        ];
    }

    /**
     * Name getFooterMenu
     *
     * @param int $id Menu Id
     *
     * @return array|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getFooterMenu(int $id): ?array
    {
        /**
         * Menu
         *
         * @var Menu $menu
         */
        $menu = Menu::with('menuLangs')->find($id);
        if (is_null($menu)) {
            return null;
        }

        $menuItems = [];
        $title = LanguageUtils::getLangValue($menu->menuLangs, 'title');

        $keyMenu = "footer-menu-" . $id;

        $cachedMenuItems = CacheService::get(self::CACHE_TAG, $keyMenu);
        if (is_null($cachedMenuItems)) {
            $items = MenuItem::getTree($id);
            /**
             * MenuItem
             *
             * @var MenuItem $item
             */
            foreach ($items as $item) {
                $menuItem = $this->createMenuItem($item);
                $menuItems[] = $menuItem;
            }

            CacheService::set(self::CACHE_TAG, $keyMenu, $menuItems);
        } else {
            $menuItems = $cachedMenuItems;
        }

        return [
            'blade' => $menu->blade,
            'title' => $title,
            'items' => $menuItems,
        ];
    }
}
