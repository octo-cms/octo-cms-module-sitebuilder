<?php

namespace OctoCmsModule\Sitebuilder\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use OctoCmsModule\Sitebuilder\Interfaces\PageServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;
use OctoCmsModule\Core\Exceptions\OctoCmsException;
use Throwable;

/**
 * Class PageService
 *
 * @package OctoCmsModule\Sitebuilder\Services
 */
class PageService implements PageServiceInterface
{
    /**
     * @param Page  $page
     * @param array $fields
     *
     * @return Page
     *
     * @throws Throwable
     */
    public function savePage(Page $page, array $fields): Page
    {
        DB::beginTransaction();

        $page->fill($fields);
        $page->save();

        $this->savePageLang($page, Arr::get($fields, 'pageLangs', []));

        DB::commit();

        return $page;
    }

    /**
     * @param Page  $page
     * @param array $pageLangsData
     *
     * @throws OctoCmsException
     */
    public function updatePageLangsContents(Page $page, array $pageLangsData)
    {
        // saving pageLangs
        foreach ($pageLangsData as $pageLangData) {

            /**
 * @var PageLang $pageLang
*/
            if (empty($pageLang = $page->pageLangs->where('lang', '=', Arr::get($pageLangData, 'lang', null))->first())
            ) {
                throw new OctoCmsException('PageLang not found');
            }

            $this->updatePageLangContents(
                $pageLang,
                Arr::get($pageLangData, 'contents', [])
            );
        }
    }

    /**
     * @param PageLang $pageLang
     * @param array    $contents
     *
     * @return bool
     */
    protected function updatePageLangContents(PageLang $pageLang, array $contents)
    {

        // remove old ids
        $pageLang->contents()
            ->whereIn(
                'id',
                array_diff(
                    $pageLang->contents->pluck('id')->toArray(),
                    Arr::pluck($contents, 'id')
                )
            )->delete();

        foreach ($contents as $content) {

            /**
 * @var PageLangContent $pageLangContent
*/
            $pageLangContent = $pageLang->contents
                ->where('id', '=', $content['id'])
                ->first();

            if (empty($pageLangContent)) {
                /**
 * @var PageLangContent $pageLangContent
*/
                $pageLangContent = new PageLangContent();
                $pageLangContent->content_type = Arr::get($content, 'content_type', '');
                $pageLangContent->content_id = Arr::get($content, 'content_id', '');
                $pageLangContent->pageLang()->associate($pageLang);
            }

            $pageLangContent->fill($content);
            $pageLangContent->save();
        }

        return true;
    }

    /**
     * @param Builder $pages
     * @param array   $filters
     * @param string  $query
     *
     * @return Builder
     */
    public function filterPageBuilder(Builder $pages, array $filters = [], string $query = ''): Builder
    {
        if (Arr::has($filters, 'published')) {
            $pages->where('published', '=', Arr::get($filters, 'published', true));
        }

        if (Arr::get($filters, 'softDeleted', false)) {
            $pages->withTrashed()->where('deleted_at', '!=', null);
        }

        if (!empty($type = Arr::get($filters, 'type', ''))) {
            $pages->where('type', '=', $type);
        }

        if (!empty($query)) {
            $pages->where(
                function (Builder $q) use ($query) {
                    $q->where('name', 'LIKE', '%' . $query . '%')
                        ->orWhere('type', 'LIKE', '%' . $query . '%')
                        ->orWhereHas(
                            'pageLangs',
                            function (Builder $pageLangBuilder) use ($query) {
                                $pageLangBuilder
                                    ->where('meta_title', 'LIKE', '%' . $query . '%')
                                    ->orWhere('meta_description', 'LIKE', '%' . $query . '%')
                                    ->orWhere('url', 'LIKE', '%' . $query . '%');
                            }
                        );
                }
            );
        }

        return $pages;
    }

    /**
     * @param Page  $page
     * @param array $pageLangs
     */
    protected function savePageLang(Page $page, array $pageLangs)
    {
        foreach ($pageLangs as $pageLang) {
            PageLang::updateOrCreate(
                [
                    'page_id' => $page->id,
                    'lang'    => Arr::get($pageLang, 'lang', ''),
                ],
                [
                    'url'              => Arr::get($pageLang, 'url', ''),
                    'meta_description' => Arr::get($pageLang, 'meta_description', ''),
                    'meta_title'       => Arr::get($pageLang, 'meta_title', ''),
                ]
            );
        }
    }
}
