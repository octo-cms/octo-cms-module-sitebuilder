<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\MenuItemLang;

/**
 * Class MenuItemLangFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class MenuItemLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MenuItemLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'menu_item_id' => MenuItem::factory(),
            'lang'         => $this->faker->randomElement(['it', 'en']),
            'label'        => $this->faker->slug(1),
        ];
    }
}
