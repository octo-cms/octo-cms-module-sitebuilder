<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;

/**
 * Class PageLangFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class PageLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PageLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'page_id'          => Page::factory(),
            'lang'             => $this->faker->randomElement(['it', 'en', 'fr']),
            'url'              => $this->faker->slug(3),
            'meta_title'       => $this->faker->text(50),
            'meta_description' => $this->faker->text(100),
        ];
    }
}
