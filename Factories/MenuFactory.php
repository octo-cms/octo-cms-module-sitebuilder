<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\Menu;

/**
 * Class MenuFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class MenuFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Menu::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'  => $this->faker->slug(1),
            'blade' => $this->faker->slug(1),
        ];
    }
}
