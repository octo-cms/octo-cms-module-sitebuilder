<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\BlockEntity;

/**
 * Class BlockEntityFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class BlockEntityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BlockEntity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'entity'   => 'blog',
            'module'   => 'Blogs',
            'blade'    => $this->faker->slug(2) . '.blade.php',
            'settings' => null,
            'layout'   => null,
        ];
    }
}
