<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\BlockHtml;

/**
 * Class BlockHtmlFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class BlockHtmlFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BlockHtml::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'module'   => $this->faker->slug(2),
            'blade'    => $this->faker->slug(2),
            'settings' => null,
            'layout'   => null,
        ];
    }
}
