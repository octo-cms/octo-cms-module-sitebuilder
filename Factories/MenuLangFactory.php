<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuLang;

/**
 * Class MenuLangFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class MenuLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MenuLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'menu_id' => Menu::factory(),
            'lang'    => $this->faker->randomElement(['it', 'en', 'fr']),
            'title'   => $this->faker->text(50),
        ];
    }
}
