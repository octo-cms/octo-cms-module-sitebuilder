<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\Footer;

/**
 * Class FooterFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class FooterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Footer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'          => $this->faker->text(50),
            'enabled'       => $this->faker->boolean,
            'order'         => 0,
            'content_align' => null,
        ];
    }
}
