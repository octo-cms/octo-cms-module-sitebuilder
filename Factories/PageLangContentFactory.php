<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;

/**
 * Class PageLangContentFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class PageLangContentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PageLangContent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'page_lang_id' => PageLang::factory(),
            'position'     => 0,
            'content_id'   => null,
            'content_type' => null,
            'data'         => null,
            'targets'      => null,
        ];
    }
}
