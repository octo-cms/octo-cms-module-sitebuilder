<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Sitebuilder\Entities\PageRedirect;

/**
 * Class PageRedirectFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class PageRedirectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PageRedirect::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'page_lang_id' => PageLang::factory(),
            'old_url'      => $this->faker->url,
        ];
    }
}
