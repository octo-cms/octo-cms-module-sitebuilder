<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\MenuItem;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class MenuItemFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class MenuItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MenuItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'menu_id'   => Menu::factory(),
            'parent_id' => null,
            'page_id'   => Page::factory(),
            'order'     => 0,
            'type'      => null,
            'data'      => null,
        ];
    }
}
