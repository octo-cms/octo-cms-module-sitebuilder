<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\Footer;
use OctoCmsModule\Sitebuilder\Entities\FooterContent;

/**
 * Class FooterContentFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class FooterContentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FooterContent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'footer_id'    => Footer::factory(),
            'content_type' => null,
            'content_id'   => null,
            'order'        => 0,
            'size'         => 12,
            'targets'      => [],
            'data'         => [],
        ];
    }
}
