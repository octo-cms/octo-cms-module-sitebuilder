<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Class PageFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class PageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Page::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'             => $this->faker->text(50),
            'published'        => true,
            'type'             => Page::TYPE_STANDARD,
            'sitemap'          => $this->faker->boolean,
            'change_freq'      => Page::CHANGE_FREQ_WEEKLY,
            'sitemap_priority' => $this->faker->randomFloat(1, 0, 1),
        ];
    }
}
