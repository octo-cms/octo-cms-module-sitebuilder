<?php

namespace OctoCmsModule\Sitebuilder\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Class SlideFactory
 *
 * @package OctoCmsModule\Sitebuilder\Factories
 */
class SlideFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Slide::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'caption'      => $this->faker->text(100),
            'sub_caption'  => null,
            'action_label' => null,
            'action_link'  => null,
        ];
    }
}
