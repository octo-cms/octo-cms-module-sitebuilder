<?php

namespace OctoCmsModule\Sitebuilder\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;

/**
 * Class SyncPageContentsCommand
 *
 * @package OctoCmsModule\Sitebuilder\Console
 */
class SyncPageContentsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sync-page-contents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';


    public function handle()
    {
        $this->info('Syncing Page Contents ...');

        foreach (PageLangContent::with('type')->get() as $pageLangContent) {
            if ($pageLangContent->content_type === 'BlockHtml') {
                $pageLangContent->data = $this->syncContentWithBlock(
                    $pageLangContent->data,
                    $pageLangContent->type->settings,
                    'value'
                );
            }

            $pageLangContent->layout = $this->syncContentWithBlock(
                $pageLangContent->layout,
                $pageLangContent->type->layout,
                'selected'
            );

            $pageLangContent->save();
        }
    }

    /**
     * @param array  $contentData
     * @param array  $blockData
     * @param string $valueKey
     *
     * @return array
     */
    protected function syncContentWithBlock(array $contentData, array $blockData, string $valueKey): array
    {
        foreach ($blockData as $blockDatum) {
            /** @var array $result */
            $result = Arr::where($contentData, function ($contentDatum) use ($blockDatum) {
                return $contentDatum['name'] === $blockDatum['name'];
            });

            if (empty($result)) {
                $contentData[] = [
                    'name'    => Arr::get($blockDatum, 'name', ''),
                    'type'    => Arr::get($blockDatum, 'type', ''),
                    $valueKey => null,
                ];
            }
        }

        return $contentData;
    }
}
