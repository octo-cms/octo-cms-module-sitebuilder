<?php

namespace OctoCmsModule\Sitebuilder\Console;

use Illuminate\Console\Command;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\SitemapServiceInterface;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;

/**
 * Class GenerateSitemapCommand
 *
 * @package OctoCmsModule\Sitebuilder\Console
 */
class GenerateSitemapCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * @param SettingServiceInterface $settingService
     * @param SitemapServiceInterface $sitemapService
     */
    public function handle(SettingServiceInterface $settingService, SitemapServiceInterface $sitemapService)
    {
        $this->info('Generating sitemap XML file ...');
        /**
 * @var Sitemap
*/
        $sitemap = SitemapGenerator::create(env('APP_URL', ''))->getSitemap();

        /**
 * @var string $fallbackLocale
*/
        $fallbackLocale = $settingService->getSettingByName(SettingNameConst::FALLBACK_LOCALE);

        /**
 * @var array $languages
*/
        $languages = $settingService->getSettingByName(SettingNameConst::LANGUAGES);

        foreach (Page::with('pageLangs')->where('sitemap', '=', true)->get() as $page) {
            if (empty($urlInstance = $sitemapService->generateUrlInstance($page, $fallbackLocale, $languages))) {
                continue;
            }

            $sitemap->add($urlInstance);
        }

        $sitemap->writeToDisk('public', 'sitemap.xml');
    }
}
