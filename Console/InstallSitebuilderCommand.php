<?php

namespace OctoCmsModule\Sitebuilder\Console;

use Illuminate\Console\Command;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use OctoCmsModule\Sitebuilder\Entities\Menu;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;
use OctoCmsModule\Core\Entities\Setting;

/**
 * Class InstallSitebuilderCommand
 *
 * @category Octo
 * @package  OctoCmsModule\Sitebuilder\Console
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class InstallSitebuilderCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'install:sitebuilder {--lang=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * SettingServiceInterface
     *
     * @var SettingServiceInterface $settingService
     */
    protected $settingService;

    /**
     * InstallServicesCommand constructor.
     *
     * @param SettingServiceInterface $settingService SettingServiceInterface
     */
    public function __construct(SettingServiceInterface $settingService)
    {
        parent::__construct();
        $this->settingService = $settingService;
    }

    /**
     * Name handle
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Running Install Site builder Command ...');

        $langs = $this->option('lang');

        if (empty($langs)) {
            $langs = [config('app.locale')];
        }


        $this->info('Creating Settings ...');
        $this->createSettings($langs);


        $this->info('Creating Homepage ...');
        $this->createHomePage($langs);

        Menu::factory()->create(
            [
                'name'  => 'main',
                'blade' => 'main',
            ]
        );
    }

    /**
     * Name createSettings
     *
     * @param array $langs Languages Array
     *
     * @return void
     */
    protected function createSettings(array $langs)
    {
        Setting::updateOrCreate(['name' => 'locale'], ['value' => $langs[0]]);
        Setting::updateOrCreate(['name' => 'fallback_locale'], ['value' => $langs[0]]);
        Setting::updateOrCreate(['name' => 'multilanguage'], ['value' => count($langs) > 1]);
        Setting::updateOrCreate(['name' => 'languages'], ['value' => $langs]);
        Setting::updateOrCreate(['name' => 'fe_admin_mail'], ['value' => '']);
        Setting::updateOrCreate(['name' => 'fe_title'], ['value' => []]);
        Setting::updateOrCreate(['name' => 'fe_description'], ['value' => []]);
        Setting::updateOrCreate(['name' => 'fe_address'], ['value' => []]);
        Setting::updateOrCreate(['name' => 'fe_date_format'], ['value' => 'Y-m-d']);
        Setting::updateOrCreate(['name' => 'fe_hour_format'], ['value' => 'H:i']);
        Setting::updateOrCreate(['name' => 'fe_main_language'], ['value' => $langs[0]]);
        Setting::updateOrCreate(['name' => 'fe_available_languages'], ['value' => $langs]);
        Setting::updateOrCreate(['name' => 'fe_logo'], ['value' => ['src' => '', 'webp' => '']]);
        Setting::updateOrCreate(['name' => 'fe_favicon'], ['value' => ['src' => '', 'webp' => '']]);
        Setting::updateOrCreate(['name' => 'fe_gtm'], ['value' => ['enabled' => false, 'container_id' => null]]);
        Setting::updateOrCreate(['name' => 'fe_iubenda'], ['value' => ['enabled' => false, 'link' => '']]);
        Setting::updateOrCreate(
            ['name' => 'fe_social_sharing'],
            [
                'value' => [
                    'facebook'  => ['enabled' => false, 'link' => null],
                    'twitter'   => ['enabled' => false, 'link' => null],
                    'instagram' => ['enabled' => false, 'link' => null],
                ],
            ]
        );
        Setting::updateOrCreate(
            ['name' => 'fe_google_optimize'],
            [
                'value' => ['enabled' => false, 'container_id' => null, 'property_id' => null],
            ]
        );

        Setting::updateOrCreate(['name' => SettingNameConst::FE_FAVICON_DATA], ['value' => []]);
    }

    /**
     * Name createHomePage
     *
     * @param array $langs Languages Array
     *
     * @return void
     */
    protected function createHomePage(array $langs)
    {
        $page = new Page(
            [
                'name' => 'HomePage',
                'type' => Page::TYPE_HOMEPAGE,
            ]
        );

        $page->save();

        foreach ($langs as $lang) {
            $pageLang = new PageLang(
                [
                    'lang'             => $lang,
                    'url'              => '',
                    'meta_title'       => env('APP_NAME') . ' - HomePage',
                    'meta_description' => 'Powered By ' . env('APP_NAME'),
                ]
            );

            $pageLang->page()->associate($page)->save();
        }
    }
}
