<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PageResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 * @author  danielepasi
 */
class BlockHtmlResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'blade'        => $this->blade,
            'module'       => $this->module,
            'settings'     => $this->settings,
            'target'       => $this->target,
            'instructions' => $this->instructions,
            'layout'       => $this->layout,
            'src'          => $this->src,
            'template'     => view()->exists(strtolower(config('octo-cms.template.module'))
                . '::contents.' . $this->blade),
            'standard'     => view()->exists(strtolower($this->module) . '::contents.' . $this->blade),
            'custom'       => view()->exists('contents.' . $this->blade),
            $this->mergeWhen('pageLangContents', [
                'contentCount' => $this->pageLangContents->count(),
            ]),
        ];
    }
}
