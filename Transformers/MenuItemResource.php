<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PageResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 * @author  danielepasi
 */
class MenuItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'menu_id'       => $this->menu_id,
            'parent_id'     => $this->parent_id,
            'order'         => $this->order,
            'type'          => $this->type,
            'data'          => $this->data,
            'menuItemLangs' => MenuItemLangResource::collection($this->whenLoaded('menuItemLangs'))
        ];
    }
}
