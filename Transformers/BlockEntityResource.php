<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BlockEntityResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 */
class BlockEntityResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'           => $this->id,
            'module'       => $this->module,
            'entity'       => $this->entity,
            'blade'        => $this->blade,
            'target'       => $this->target,
            'instructions' => $this->instructions,
            'template'     => view()->exists(strtolower(config('octo-cms.template.module'))
                . '::contents.' . $this->blade),
            'standard'     => view()->exists(strtolower($this->module) . '::contents.' . $this->blade),
            'custom'       => view()->exists('contents.' . $this->blade),
            'settings'     => $this->settings,
            'layout'       => $this->layout,
            'src'          => $this->src,
        ];
    }
}
