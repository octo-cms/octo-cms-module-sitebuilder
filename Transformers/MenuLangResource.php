<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MenuLangResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 */
class MenuLangResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'lang'  => $this->lang,
            'title' => $this->title,
            'menu'  => new MenuResource($this->whenLoaded('menu')),
        ];
    }
}
