<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\PictureResource;

/**
 * Class PageResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 * @author  danielepasi
 */
class SlideResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'position'     => $this->position,
            'caption'      => $this->caption,
            'sub_caption'  => $this->sub_caption,
            'action_label' => $this->action_label,
            'action_link'  => $this->action_link,
            'pictures'     => PictureResource::collection($this->whenLoaded('pictures')),
        ];
    }
}
