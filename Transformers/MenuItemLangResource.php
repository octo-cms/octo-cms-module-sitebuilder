<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PageResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 * @author  danielepasi
 */
class MenuItemLangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'menu_item_id' => $this->menu_item_id,
            'lang'         => $this->lang,
            'label'        => $this->label,
        ];
    }
}
