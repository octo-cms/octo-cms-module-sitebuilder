<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MenuResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 */
class MenuResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'blade'     => $this->blade,
            'items'     => MenuItemResource::collection($this->whenLoaded('items')),
            'menuLangs' => MenuLangResource::collection($this->whenLoaded('menuLangs')),
        ];
    }
}
