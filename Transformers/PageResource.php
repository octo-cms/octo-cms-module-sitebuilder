<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\PictureResource;

/**
 * Class PageResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 * @author  danielepasi
 */
class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'type'             => $this->type,
            'published'        => $this->published,
            'sitemap'          => $this->sitemap,
            'change_freq'      => $this->change_freq,
            'sitemap_priority' => $this->sitemap_priority,
            'pageLangs'        => PageLangResource::collection($this->whenLoaded('pageLangs')),
            'pictures'         => PictureResource::collection($this->whenLoaded('pictures')),
            'menuItems'        => MenuItemResource::collection($this->whenLoaded('menuItems')),
        ];
    }
}
