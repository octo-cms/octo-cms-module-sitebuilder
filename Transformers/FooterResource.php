<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MenuResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 */
class FooterResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'enabled'       => $this->enabled,
            'order'         => $this->order,
            'content_align' => $this->content_align,
            'contents'      => FooterContentResource::collection($this->whenLoaded('contents')),

        ];
    }
}
