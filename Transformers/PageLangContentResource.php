<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PageResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 * @author  danielepasi
 */
class PageLangContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'page_lang_id' => $this->page_lang_id,
            'targets'      => $this->targets,
            'position'     => $this->position,
            'content_type' => $this->content_type,
            'content_id'   => $this->content_id,
            'pageLang'     => $this->whenLoaded('pageLang'),
            'data'         => $this->data,
            'layout'       => $this->layout,
            'ids'          => $this->ids,
            $this->mergeWhen('type', [
                'type' => $this->type->toArray(),
            ]),

        ];
    }
}
