<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PageResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 * @author  danielepasi
 */
class PageLangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'page_id'          => $this->page_id,
            'lang'             => $this->lang,
            'url'              => $this->url,
            'meta_title'       => $this->meta_title,
            'meta_description' => $this->meta_description,
            'contents'         => PageLangContentResource::collection($this->whenLoaded('contents')),
        ];
    }
}
