<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PageResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 * @author  danielepasi
 */
class FooterContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'footer_id'    => $this->footer_id,
            'targets'      => $this->targets,
            'order'        => $this->order,
            'content_type' => $this->content_type,
            'content_id'   => $this->content_id,
            'data'         => $this->data,
            'size'         => $this->size,
            'layout'       => $this->layout,
            $this->mergeWhen('type', [
                'type' => $this->type->toArray(),
            ]),

        ];
    }
}
