<?php

namespace OctoCmsModule\Sitebuilder\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PageRedirectResource
 *
 * @package OctoCmsModule\Sitebuilder\Transformers
 */
class PageRedirectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'old_url'  => $this->old_url,
            'pageLang' => new PageLangResource($this->whenLoaded('pageLang')),
        ];
    }
}
