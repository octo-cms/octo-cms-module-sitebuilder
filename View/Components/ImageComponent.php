<?php

namespace OctoCmsModule\Sitebuilder\View\Components;

use Illuminate\Support\Arr;
use Illuminate\View\Component;
use Illuminate\View\View;
use OctoCmsModule\Core\Traits\ImageSrcTrait;
use OctoCmsModule\Sitebuilder\Traits\LangValueTrait;

/**
 * Class ImageComponent
 *
 * @package OctoCmsModule\Sitebuilder\View\Components
 */
class ImageComponent extends Component
{
    use ImageSrcTrait, LangValueTrait;

    protected $pictureData = [];

    protected const IMAGE_ATTRIBUTES = ['alt', 'caption', 'description', 'title'];

    /**
     * ImageComponent constructor.
     *
     * @param $picture
     * @param string $src
     * @param string $class
     */
    public function __construct($picture, string $src, string $class = '')
    {

        $this->pictureData['src'] = $src;
        $this->pictureData['class'] = $class;

        $pictureLangCollection = !empty($picture['picture_langs'])
            ? collect($picture['picture_langs'])
            : collect([]);

        foreach (self::IMAGE_ATTRIBUTES as $key) {
            $this->pictureData[$key] = Arr::get(
                $picture,
                $key,
                $this->getLangValue($pictureLangCollection, $key)
            );
        }

        if (!empty($picture['src']) && !empty($picture['webp'])) {
            $this->pictureData['data-src'] = $this->getImageSrc($picture);
            $this->pictureData['class'] .= " lazy";
        }
    }

    /**
     * @return View|string
     */
    public function render()
    {
        $htmlImage = "<img ";
        foreach ($this->pictureData as $key => $data) {
            if (!empty($data)) {
                $htmlImage .= " $key='$data'";
            }
        }
        $htmlImage .= " />";
        return $htmlImage;
    }
}
