<?php

namespace OctoCmsModule\Sitebuilder\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;
use OctoCmsModule\Sitebuilder\Entities\PageLangContent;
use OctoCmsModule\Core\Traits\ImageSrcTrait;
use OctoCmsModule\Sitebuilder\Interfaces\SettingServiceInterface;
use OctoCmsModule\Sitebuilder\Traits\LangValueTrait;
use OctoCmsModule\Sitebuilder\Utils\ContentUtils;

/**
 * Class PageContentHandler
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Sitebuilder\View\Components
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class PageContentHandler extends Component
{
    use ImageSrcTrait, LangValueTrait;

    /**
     * @var PageLangContent
     */
    public $content;

    public $settingService;

    /**
     * PageContentHandler constructor.
     * @param $content
     * @param SettingServiceInterface $settingService
     */
    public function __construct($content, SettingServiceInterface $settingService)
    {
        $this->content = $content;
        $this->settingService = $settingService;
    }

    /**
     * Name render
     *
     * @return View|string
     */
    public function render()
    {
        $template = strtolower(config('octo-cms.template.module'));

        return view()->first(
            [
                'contents.' . $this->content->type->blade,
                $template . '::contents.' . $this->content->type->blade,
            ],
            [
                'data'    => ContentUtils::parseBlockData($this->content->data),
                'layout'  => ContentUtils::parseBlockLayout($this->content->layout),
                'targets' => $this->content->targets,
            ]
        );
    }

    /**
     * Name getSettingByName
     * @param string $settingName
     *
     * @return mixed
     */
    public function getSettingByName(string $settingName)
    {
        return $this->settingService->getSettingByName($settingName);
    }
}
