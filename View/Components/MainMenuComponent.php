<?php

namespace OctoCmsModule\Sitebuilder\View\Components;

use Illuminate\View\Component;
use OctoCmsModule\Sitebuilder\Interfaces\MenuServiceInterface;

/**
 * Class PageContentHandler
 *
 * @package OctoCmsModule\Sitebuilder\View\Components
 */
class MainMenuComponent extends Component
{

    /**
     * @var MenuServiceInterface
     */
    public $menuService;

    public $menu;


    /**
     * PageContentHandler constructor.
     *
     * @param $menuService
     */
    public function __construct(MenuServiceInterface $menuService)
    {
        $this->menuService = $menuService;
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $this->menu = $this->menuService->getMainMenu();
        $template = strtolower(config('octo-cms.template.module'));

        return view()->first(
            [
                'menu.' . $this->menu['blade'],
                $template . '::menu.' . $this->menu['blade'],
            ]
        );
    }
}
