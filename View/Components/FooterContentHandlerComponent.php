<?php

namespace OctoCmsModule\Sitebuilder\View\Components;

use Closure;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\View\Component;
use Illuminate\View\View;
use OctoCmsModule\Core\Constants\CacheTagConst;
use OctoCmsModule\Core\Services\CacheService;
use OctoCmsModule\Sitebuilder\Entities\FooterContent;
use OctoCmsModule\Core\Traits\ImageSrcTrait;
use OctoCmsModule\Sitebuilder\Interfaces\MenuServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\SettingServiceInterface;
use OctoCmsModule\Sitebuilder\Services\MenuService;
use OctoCmsModule\Sitebuilder\Traits\LangValueTrait;
use OctoCmsModule\Sitebuilder\Utils\ContentUtils;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class PageContentHandler
 *
 * @package OctoCmsModule\Sitebuilder\View\Components
 */
class FooterContentHandlerComponent extends Component
{
    use ImageSrcTrait, LangValueTrait;

    /**
     * @var FooterContent
     */
    public $content;

    public $settingService;

    /**
     * @var MenuService
     */
    public $menuService;

    /**
     * FooterContentHandlerComponent constructor.
     *
     * @param $content
     * @param SettingServiceInterface $settingService
     * @param MenuServiceInterface    $menuService
     */

    public function __construct($content, SettingServiceInterface $settingService, MenuServiceInterface  $menuService)
    {
        $this->content = $content;
        $this->settingService = $settingService;
        $this->menuService=$menuService;
    }

    /**
     * @return Closure|Htmlable|View|string
     * @throws InvalidArgumentException
     */
    public function render()
    {
        $template = strtolower(config('octo-cms.template.module'));
        $bladeFolder = $this->content->content_type == 'Menu' ? 'menu' : 'contents';

        return view()->first(
            [
                $bladeFolder . '.' . $this->content->type->blade,
                $template . '::' . $bladeFolder . '.' . $this->content->type->blade,
            ],
            $this->getData($this->content)
        );
    }

    /**
     * @param FooterContent $content
     *
     * @return array
     * @throws InvalidArgumentException
     */
    protected function getData(FooterContent $content): array
    {
        $data = CacheService::get(CacheTagConst::FOOTER, $content->id);

        if (!empty($data)) {
            return $data;
        }

        $data = [
            'size'    => $this->content->size,
            'data'    => $this->content->content_type == 'Menu'
                ? $this->menuService->getFooterMenu($this->content->content_id)
                : ContentUtils::parseBlockData($this->content->data),
            'targets' => $this->content->targets,
            'layout'  => ContentUtils::parseBlockLayout($this->content->layout),
        ];

        CacheService::set(CacheTagConst::FOOTER, $content->id, $data);

        return $data;
    }

    /**
     * @param string $settingName
     *
     * @return mixed
     */
    public function getSettingByName(string $settingName)
    {
        return $this->settingService->getSettingByName($settingName);
    }
}
