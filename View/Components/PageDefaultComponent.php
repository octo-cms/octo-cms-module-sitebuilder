<?php

namespace OctoCmsModule\Sitebuilder\View\Components;

use Illuminate\View\Component;

/**
 * Class PageContentHandler
 * @package OctoCmsModule\Sitebuilder\View\Components
 */
class PageDefaultComponent extends Component
{

    public $module;

    public $blade;


    /**
     * PageDefaultComponent constructor.
     * @param string $module
     * @param string $blade
     */
    public function __construct(string $module, string $blade)
    {
        $this->module = $module;
        $this->blade = $blade;
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('Sitebuilder::default');
    }
}
