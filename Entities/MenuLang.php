<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Sitebuilder\Factories\MenuLangFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\MenuLang
 *
 * @property int       $id
 * @property int       $menu_id
 * @property string    $lang
 * @property string    $title
 * @property-read Menu $menu
 * @method static Builder|MenuLang newModelQuery()
 * @method static Builder|MenuLang newQuery()
 * @method static Builder|MenuLang query()
 * @method static Builder|MenuLang whereId($value)
 * @method static Builder|MenuLang whereLang($value)
 * @method static Builder|MenuLang whereMenuId($value)
 * @method static Builder|MenuLang whereTitle($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Sitebuilder\Factories\MenuLangFactory factory(...$parameters)
 */
class MenuLang extends Model
{
    use HasFactory;

    protected $fillable = [
        'menu_id',
        'lang',
        'title',
    ];

    public $timestamps = false;

    /**
     * @return MenuLangFactory
     */
    protected static function newFactory()
    {
        return MenuLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
