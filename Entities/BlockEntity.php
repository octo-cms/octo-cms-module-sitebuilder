<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Sitebuilder\Factories\BlockEntityFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\BlockEntity
 *
 * @property int $id
 * @property string $entity
 * @property string $blade
 * @property mixed $settings
 * @method static Builder|BlockEntity newModelQuery()
 * @method static Builder|BlockEntity newQuery()
 * @method static Builder|BlockEntity query()
 * @method static Builder|BlockEntity whereBlade($value)
 * @method static Builder|BlockEntity whereEntity($value)
 * @method static Builder|BlockEntity whereId($value)
 * @method static Builder|BlockEntity whereSettings($value)
 * @mixin Eloquent
 * @property string $module
 * @method static Builder|BlockEntity whereModule($value)
 * @property string|null $src
 * @property string|null $instructions
 * @method static Builder|BlockEntity whereInstructions($value)
 * @method static Builder|BlockEntity whereSrc($value)
 * @property string $target
 * @method static Builder|BlockEntity whereTarget($value)
 * @property mixed $layout
 * @method static Builder|BlockEntity whereLayout($value)
 * @method static \OctoCmsModule\Sitebuilder\Factories\BlockEntityFactory factory(...$parameters)
 */
class BlockEntity extends Model
{
    use HasFactory;

    public const TARGET_PAGE = 'page';
    public const TARGET_FOOTER = 'footer';

    protected $table = 'block_entities';

    protected $fillable = [
        'module',
        'entity',
        'blade',
        'target',
        'instructions',
        'settings',
        'layout',
        'src',
    ];

    public $timestamps = false;

    /**
     * @return BlockEntityFactory
     */
    protected static function newFactory()
    {
        return BlockEntityFactory::new();
    }

    /**
     * @param  $value
     * @return void
     */
    public function setSettingsAttribute($value)
    {
        $this->attributes['settings'] = serialize($value);
    }

    /**
     * @param  $value
     * @return mixed
     */
    public function getSettingsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param  $value
     * @return void
     */
    public function setLayoutAttribute($value)
    {
        $this->attributes['layout'] = serialize($value);
    }

    /**
     * @param  $value
     * @return mixed
     */
    public function getLayoutAttribute($value)
    {
        return unserialize($value);
    }
}
