<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use OctoCmsModule\Sitebuilder\Factories\FooterFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\Footer
 *
 * @property int                                                           $id
 * @property string|null                                                   $name
 * @property int                                                           $enabled
 * @property int                                                           $order
 * @property string|null                                                   $content_align
 * @property Carbon|null                               $created_at
 * @property Carbon|null                               $updated_at
 * @property-read Collection|FooterContent[] $contents
 * @property-read int|null                                                 $contents_count
 * @method static Builder|Footer newModelQuery()
 * @method static Builder|Footer newQuery()
 * @method static Builder|Footer query()
 * @method static Builder|Footer whereContentAlign($value)
 * @method static Builder|Footer whereCreatedAt($value)
 * @method static Builder|Footer whereEnabled($value)
 * @method static Builder|Footer whereId($value)
 * @method static Builder|Footer whereName($value)
 * @method static Builder|Footer whereOrder($value)
 * @method static Builder|Footer whereUpdatedAt($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Sitebuilder\Factories\FooterFactory factory(...$parameters)
 */
class Footer extends Model
{
    use HasFactory;

    protected $table = 'footers';

    protected $fillable = [
        'name',
        'enabled',
        'order',
        'content_align',

    ];

    protected $casts = [
        'enabled' => 'boolean',
        'order'   => 'integer',
    ];

    /**
     * @return FooterFactory
     */
    protected static function newFactory()
    {
        return FooterFactory::new();
    }

    /**
     * @return HasMany
     */
    public function contents()
    {
        return $this->hasMany(FooterContent::class);
    }
}
