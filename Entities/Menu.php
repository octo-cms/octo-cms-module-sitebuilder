<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use OctoCmsModule\Sitebuilder\Factories\MenuFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\Menu
 *
 * @property int                             $id
 * @property string                          $name
 * @property bool                            $active
 * @property int|null                        $parent_id
 * @property int|null                        $page_id
 * @property int                             $position
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|MenuLang[]      $menuLangs
 * @property-read int|null                   $menu_langs_count
 * @property-read Page|null                  $page
 * @method static Builder|Menu newModelQuery()
 * @method static Builder|Menu newQuery()
 * @method static Builder|Menu query()
 * @method static Builder|Menu whereActive($value)
 * @method static Builder|Menu whereCreatedAt($value)
 * @method static Builder|Menu whereId($value)
 * @method static Builder|Menu whereName($value)
 * @method static Builder|Menu wherePageId($value)
 * @method static Builder|Menu whereParentId($value)
 * @method static Builder|Menu wherePosition($value)
 * @method static Builder|Menu whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string                                                   $blade
 * @property-read int|null                                            $items_count
 * @method static Builder|Menu whereBlade($value)
 * @property-read Collection|MenuItem[] $items
 * @method static \OctoCmsModule\Sitebuilder\Factories\MenuFactory factory(...$parameters)
 */
class Menu extends Model
{
    use HasFactory;

    public const MAIN_MENU = 'main';

    protected $table= 'menus';

    protected $fillable = [
        'name',
        'blade',
    ];

    /**
     * @return MenuFactory
     */
    protected static function newFactory()
    {
        return MenuFactory::new();
    }

    /**
     * @return HasMany
     */
    public function items()
    {
        return $this->hasMany(MenuItem::class);
    }

    /**
     * @return HasMany
     */
    public function menuLangs()
    {
        return $this->hasMany(MenuLang::class);
    }
}
