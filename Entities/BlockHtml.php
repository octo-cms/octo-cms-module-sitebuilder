<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use OctoCmsModule\Sitebuilder\Factories\BlockHtmlFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\BlockHtml
 *
 * @property int                                                             $id
 * @property mixed                                                           $data
 * @method static Builder|BlockHtml newModelQuery()
 * @method static Builder|BlockHtml newQuery()
 * @method static Builder|BlockHtml query()
 * @method static Builder|BlockHtml whereData($value)
 * @method static Builder|BlockHtml whereId($value)
 * @mixin Eloquent
 * @property-read PageLangContent                                            $pageLangContent
 * @property string                                                          $blade
 * @property mixed                                                           $settings
 * @property-read int|null                                                   $page_lang_content_count
 * @method static Builder|BlockHtml whereBlade($value)
 * @method static Builder|BlockHtml whereSettings($value)
 * @property-read Collection|PageLangContent[] $pageLangContents
 * @property-read int|null                                                   $page_lang_contents_count
 * @property string|null                                                     $module
 * @method static Builder|BlockHtml whereModule($value)
 * @property string|null $src
 * @property string|null $instructions
 * @method static Builder|BlockHtml whereInstructions($value)
 * @method static Builder|BlockHtml whereSrc($value)
 * @property string $target
 * @method static Builder|BlockHtml whereTarget($value)
 * @property mixed $layout
 * @method static Builder|BlockHtml whereLayout($value)
 * @method static \OctoCmsModule\Sitebuilder\Factories\BlockHtmlFactory factory(...$parameters)
 */
class BlockHtml extends Model
{
    use HasFactory;

    public const GCS_PATH = 'block-html-images';

    public const TARGET_PAGE = 'page';
    public const TARGET_FOOTER = 'footer';

    protected $table = 'block_html';

    protected $fillable = [
        'blade',
        'module',
        'settings',
        'target',
        'instructions',
        'layout',
        'src',
    ];

    public $timestamps = false;

    /**
     * @return BlockHtmlFactory
     */
    protected static function newFactory()
    {
        return BlockHtmlFactory::new();
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setSettingsAttribute($value)
    {
        $this->attributes['settings'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getSettingsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setLayoutAttribute($value)
    {
        $this->attributes['layout'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getLayoutAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return MorphMany
     */
    public function pageLangContents()
    {
        return $this->morphMany(
            PageLangContent::class,
            'type',
            'content_type',
            'content_id'
        );
    }
}
