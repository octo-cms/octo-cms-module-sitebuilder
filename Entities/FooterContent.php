<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use OctoCmsModule\Sitebuilder\DTO\PagelangContentTargetsDTO;
use OctoCmsModule\Sitebuilder\Factories\FooterContentFactory;

/**
 * Class FooterContent
 *
 * @package OctoCmsModule\Sitebuilder\Entities
 * @property int                        $id
 * @property int                        $footer_id
 * @property string|null                $type
 * @property int|null                   $type_id
 * @property int                        $order
 * @property int                        $size
 * @property \PagelangContentTargetsDTO $targets
 * @property mixed                      $data
 * @property-read Footer                $footer
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         newModelQuery()
 * @method static Builder|FooterContent newQuery()
 * @method static Builder|FooterContent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereFooterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereTargets($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereTypeId($value)
 * @mixin Eloquent
 * @property string|null                                     $content_type
 * @property int|null                                        $content_id
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Sitebuilder\Entities\FooterContent
 *         whereContentType($value)
 * @property mixed $layout
 * @method static Builder|FooterContent whereLayout($value)
 * @method static \OctoCmsModule\Sitebuilder\Factories\FooterContentFactory factory(...$parameters)
 */
class FooterContent extends Model
{
    use HasFactory;

    protected $table = 'footer_contents';

    public $timestamps = false;

    protected $fillable = [
        'footer_id',
        'order',
        'size',
        'targets',
        'data',
        'layout',
        'content_type',
        'content_id',
    ];

    /**
     * @return FooterContentFactory
     */
    protected static function newFactory()
    {
        return FooterContentFactory::new();
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return PagelangContentTargetsDTO
     */
    public function getTargetsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setTargetsAttribute($value)
    {
        $this->attributes['targets'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     */
    public function setLayoutAttribute($value)
    {
        $this->attributes['layout'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getLayoutAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return BelongsTo
     */
    public function footer()
    {
        return $this->belongsTo(Footer::class);
    }

    /**
     * @return MorphTo
     */
    public function type()
    {
        return $this->morphTo('type', 'content_type', 'content_id', 'id');
    }
}
