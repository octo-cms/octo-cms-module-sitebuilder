<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use OctoCmsModule\Sitebuilder\DTO\PagelangContentTargetsDTO;
use OctoCmsModule\Sitebuilder\Factories\PageLangContentFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\PageLangContent
 *
 * @property int $id
 * @property int $page_lang_id
 * @property int $position
 * @property int $content_id
 * @property string $content_type
 * @property array|null $data
 * @method static Builder|PageLangContent newModelQuery()
 * @method static Builder|PageLangContent newQuery()
 * @method static Builder|PageLangContent query()
 * @method static Builder|PageLangContent whereContentId($value)
 * @method static Builder|PageLangContent whereContentType($value)
 * @method static Builder|PageLangContent whereData($value)
 * @method static Builder|PageLangContent whereId($value)
 * @method static Builder|PageLangContent wherePageLangId($value)
 * @method static Builder|PageLangContent wherePosition($value)
 * @mixin Eloquent
 * @property-read PageLang       $pageLang
 * @property string              $target
 * @property string              $blade
 * @method static Builder|PageLangContent whereBlade($value)
 * @method static Builder|PageLangContent whereTarget($value)
 * @property-read Model|Eloquent $type
 * @property mixed               $ids
 * @method static Builder|PageLangContent whereIds($value)
 * @property mixed               $targets
 * @method static Builder|PageLangContent whereTargets($value)
 * @property mixed               $layout
 * @method static Builder|PageLangContent whereLayout($value)
 * @method static \OctoCmsModule\Sitebuilder\Factories\PageLangContentFactory factory(...$parameters)
 */
class PageLangContent extends Model
{
    use HasFactory;

    public const TARGET_TYPE_CUSTOM = 'custom';
    public const TARGET_TYPE_TAG = 'tag';
    public const TARGET_TYPE_NEWEST = 'newest';

    public $timestamps = false;

    protected $fillable = [
        'position',
        'data',
        'targets',
        'layout'
    ];

    protected $casts = [
        'position' => 'integer',
    ];

    /**
     * @return PageLangContentFactory
     */
    protected static function newFactory()
    {
        return PageLangContentFactory::new();
    }

    /**
     * @param $value
     * @return void
     */
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    /**
     * @param $value
     * @return PagelangContentTargetsDTO
     */
    public function getTargetsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     * @return void
     */
    public function setTargetsAttribute($value)
    {
        $this->attributes['targets'] = serialize($value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     * @return void
     */
    public function setLayoutAttribute($value)
    {
        $this->attributes['layout'] = serialize($value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getLayoutAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return BelongsTo
     */
    public function pageLang()
    {
        return $this->belongsTo(PageLang::class);
    }

    /**
     * @return MorphTo
     */
    public function type()
    {
        return $this->morphTo('type', 'content_type', 'content_id', 'id');
    }
}
