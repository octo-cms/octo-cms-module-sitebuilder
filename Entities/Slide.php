<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Conner\Tagging\Taggable;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Traits\PicturableTrait;
use OctoCmsModule\Sitebuilder\Factories\SlideFactory;

/**
 * Class Slide
 *
 * @package OctoCmsModule\Sitebuilder\Entities
 * @author danielepasi
 * @property int                       $id
 * @property int                       $slider_id
 * @property int                       $position
 * @property string|null                                            $caption
 * @property-read Slider                                            $slider
 * @method static Builder|Slide newModelQuery()
 * @method static Builder|Slide newQuery()
 * @method static Builder|Slide query()
 * @method static Builder|Slide whereCaption($value)
 * @method static Builder|Slide whereId($value)
 * @method static Builder|Slide wherePosition($value)
 * @method static Builder|Slide whereSliderId($value)
 * @mixin Eloquent
 * @property-read Collection|Media[]                                $media
 * @property-read int|null                                          $media_count
 * @method static Builder|Slide whereHasMedia($tags, $matchAll = false)
 * @method static Builder|Slide whereHasMediaMatchAll($tags)
 * @method static Builder|Slide withMedia($tags = [], $matchAll = false)
 * @method static Builder|Slide withMediaMatchAll($tags = [])
 * @property string|null               $image_url
 * @property string|null               $sub_caption
 * @property string|null               $action_label
 * @property string|null               $action_link
 * @method static Builder|Slide whereActionLabel($value)
 * @method static Builder|Slide whereActionLink($value)
 * @method static Builder|Slide whereImageUrl($value)
 * @method static Builder|Slide whereSubCaption($value)
 * @property-read Collection|Picture[] $pictures
 * @property-read int|null             $pictures_count
 * @property array $tag_names
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Sitebuilder\Entities\Tagged[] $tags
 * @property-read Collection|\Conner\Tagging\Model\Tagged[] $tagged
 * @property-read int|null $tagged_count
 * @method static Builder|Slide withAllTags($tagNames)
 * @method static Builder|Slide withAnyTag($tagNames)
 * @method static Builder|Slide withoutTags($tagNames)
 * @method static \OctoCmsModule\Sitebuilder\Factories\SlideFactory factory(...$parameters)
 */
class Slide extends Model
{
    use PicturableTrait, HasFactory, Taggable;

    public const GCS_PATH = 'slides';

    protected $fillable = [
        'caption',
        'sub_caption',
        'action_label',
        'action_link',
    ];

    protected $casts = [
        'position' => 'integer',
    ];

    public $timestamps = false;

    /**
     * @return SlideFactory
     */
    protected static function newFactory()
    {
        return SlideFactory::new();
    }
}
