<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use OctoCmsModule\Sitebuilder\Factories\PageLangFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\PageLang
 *
 * @property int $id
 * @property int $page_id
 * @property string $lang
 * @property string $template
 * @property string|null $title
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|PageLang newModelQuery()
 * @method static Builder|PageLang newQuery()
 * @method static Builder|PageLang query()
 * @method static Builder|PageLang whereCreatedAt($value)
 * @method static Builder|PageLang whereId($value)
 * @method static Builder|PageLang whereLang($value)
 * @method static Builder|PageLang whereMetaDescription($value)
 * @method static Builder|PageLang whereMetaTitle($value)
 * @method static Builder|PageLang wherePageId($value)
 * @method static Builder|PageLang whereTemplate($value)
 * @method static Builder|PageLang whereTitle($value)
 * @method static Builder|PageLang whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|PageLangContent[] $contents
 * @property-read int|null                                                   $contents_count
 * @property-read Page                                                       $page
 * @property string|null                                                     $url
 * @method static Builder|PageLang whereUrl($value)
 * @property mixed                                                           $data
 * @property string|null $deleted_at
 * @method static Builder|PageLang whereDeletedAt($value)
 * @method static \OctoCmsModule\Sitebuilder\Factories\PageLangFactory factory(...$parameters)
 */
class PageLang extends Model
{
    use HasFactory;

    public const PREFIX_SERVICES = 'services/';
    public const PREFIX_BRANDS = 'brands/';
    public const PREFIX_CATEGORIES = 'categories/';
    public const PREFIX_PRODUCTS = 'products/';
    public const PREFIX_NEWS = 'news/';

    protected $table = 'page_langs';

    protected $fillable = [
        'page_id',
        'lang',
        'url',
        'template',
        'meta_title',
        'meta_description',
    ];

    /**
     * @return PageLangFactory
     */
    protected static function newFactory()
    {
        return PageLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    /**
     * @return HasMany
     */
    public function contents()
    {
        return $this->hasMany(PageLangContent::class);
    }
}
