<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use OctoCmsModule\Sitebuilder\Factories\PageRedirectFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\PageRedirect
 *
 * @property int                             $id
 * @property string                          $old_url
 * @property int                             $page_lang_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read PageLang                   $pageLang
 * @method static Builder|PageRedirect newModelQuery()
 * @method static Builder|PageRedirect newQuery()
 * @method static Builder|PageRedirect query()
 * @method static Builder|PageRedirect whereCreatedAt($value)
 * @method static Builder|PageRedirect whereId($value)
 * @method static Builder|PageRedirect whereOldUrl($value)
 * @method static Builder|PageRedirect wherePageLangId($value)
 * @method static Builder|PageRedirect whereUpdatedAt($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Sitebuilder\Factories\PageRedirectFactory factory(...$parameters)
 */
class PageRedirect extends Model
{
    use HasFactory;

    protected $fillable = [
        'old_url',
        'page_lang_id',
    ];

    /**
     * @return PageRedirectFactory
     */
    protected static function newFactory()
    {
        return PageRedirectFactory::new();
    }

    public function pageLang()
    {
        return $this->belongsTo(PageLang::class);
    }
}
