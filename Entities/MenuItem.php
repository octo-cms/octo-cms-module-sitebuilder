<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OctoCmsModule\Sitebuilder\Factories\MenuItemFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\MenuItem
 *
 * @property int                                                          $id
 * @property int                                                          $menu_id
 * @property int                                                          $_lft
 * @property int                                                          $_rgt
 * @property int|null                                                     $parent_id
 * @property int|null                                                     $page_id
 * @property-read \Kalnoy\Nestedset\Collection|MenuItem[]                 $children
 * @property-read int|null                                                $children_count
 * @property-read Collection|MenuItemLang[] $menuItemLangs
 * @property-read int|null
 *                $menu_item_langs_count
 * @property-read Page|null                                               $page
 * @property-read MenuItem|null                                           $parent
 * @method static Builder|MenuItem d()
 * @method static QueryBuilder|MenuItem newModelQuery()
 * @method static QueryBuilder|MenuItem newQuery()
 * @method static QueryBuilder|MenuItem query()
 * @method static Builder|MenuItem whereId($value)
 * @method static Builder|MenuItem whereLft($value)
 * @method static Builder|MenuItem whereMenuId($value)
 * @method static Builder|MenuItem wherePageId($value)
 * @method static Builder|MenuItem whereParentId($value)
 * @method static Builder|MenuItem whereRgt($value)
 * @mixin Eloquent
 * @property int         $order
 * @property string|null $type
 * @property mixed       $data
 * @property-read Menu   $menu
 * @method static Builder|MenuItem whereData($value)
 * @method static Builder|MenuItem whereOrder($value)
 * @method static Builder|MenuItem whereType($value)
 * @method static \OctoCmsModule\Sitebuilder\Factories\MenuItemFactory factory(...$parameters)
 */
class MenuItem extends Model
{
    use HasFactory;

    public const TYPE_PAGE = 'page';
    public const TYPE_EXTERNAL = 'external';
    public const TYPE_PARENT = 'parent';

    public $timestamps = false;

    protected $table = 'menu_items';

    protected $fillable = [
        'parent_id',
        'order',
        'type',
        'data',
    ];

    /**
     * @return MenuItemFactory
     */
    protected static function newFactory()
    {
        return MenuItemFactory::new();
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }

    /**
     * @return HasMany
     */
    public function menuItemLangs()
    {
        return $this->hasMany(MenuItemLang::class);
    }

    /**
     * @return BelongsTo
     */
    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    /**
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany(MenuItem::class, 'parent_id')
            ->with('menuItemLangs')
            ->with('children');
    }

    /**
     * @param  $menuId
     * @return Builder[]|Collection
     */
    public static function getTree($menuId)
    {
        return MenuItem::with('children')
            ->with('menuItemLangs')
            ->where('parent_id', '=', null)
            ->where('menu_id', '=', $menuId)
            ->orderBy('order')
            ->get();
    }
}
