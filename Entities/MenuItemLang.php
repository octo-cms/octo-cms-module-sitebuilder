<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Sitebuilder\Factories\MenuItemLangFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\MenuItemLang
 *
 * @property-read MenuItem $menuItem
 * @method static Builder|MenuItemLang newModelQuery()
 * @method static Builder|MenuItemLang newQuery()
 * @method static Builder|MenuItemLang query()
 * @mixin Eloquent
 * @property int           $id
 * @property int           $menu_item_id
 * @property string        $lang
 * @property string|null   $label
 * @method static Builder|MenuItemLang whereId($value)
 * @method static Builder|MenuItemLang whereLabel($value)
 * @method static Builder|MenuItemLang whereLang($value)
 * @method static Builder|MenuItemLang whereMenuItemId($value)
 * @method static \OctoCmsModule\Sitebuilder\Factories\MenuItemLangFactory factory(...$parameters)
 */
class MenuItemLang extends Model
{
    use HasFactory;

    protected $table='menu_item_langs';

    public $timestamps = false;

    protected $fillable = [
        'menu_item_id',
        'lang',
        'label',
    ];

    protected $casts = [
        'active' => 'boolean',

    ];

    /**
     * @return MenuItemLangFactory
     */
    protected static function newFactory()
    {
        return MenuItemLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function menuItem()
    {
        return $this->belongsTo(MenuItem::class);
    }
}
