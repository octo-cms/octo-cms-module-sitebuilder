<?php

namespace OctoCmsModule\Sitebuilder\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use OctoCmsModule\Core\Traits\PicturableTrait;
use OctoCmsModule\Sitebuilder\Factories\PageFactory;

/**
 * OctoCmsModule\Sitebuilder\Entities\Page
 *
 * @property int $id
 * @property bool $published
 * @property bool $sitemap
 * @property string $change_freq
 * @property mixed $sitemap_priority
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Page newModelQuery()
 * @method static Builder|Page newQuery()
 * @method static Builder|Page query()
 * @method static Builder|Page whereChangeFreq($value)
 * @method static Builder|Page whereCreatedAt($value)
 * @method static Builder|Page whereId($value)
 * @method static Builder|Page wherePublished($value)
 * @method static Builder|Page whereSitemap($value)
 * @method static Builder|Page whereSitemapPriority($value)
 * @method static Builder|Page whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|PageLang[] $pageLangs
 * @property-read int|null $page_langs_count
 * @property string $name
 * @property string $type
 * @method static Builder|Page whereName($value)
 * @method static Builder|Page whereType($value)
 * @property string|null                $pageable_type
 * @property int|null                   $pageable_id
 * @property-read Model|Eloquent        $pageable
 * @method static Builder|Page wherePageableId($value)
 * @method static Builder|Page wherePageableType($value)
 * @method static \Illuminate\Database\Query\Builder|Page onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Page withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Page withoutTrashed()
 * @property Carbon|null                $deleted_at
 * @method static Builder|Page whereDeletedAt($value)
 * @property-read Collection|Picture[]  $pictures
 * @property-read int|null              $pictures_count
 * @property-read Collection|MenuItem[] $menuItems
 * @property-read int|null              $menu_items_count
 * @method static \OctoCmsModule\Sitebuilder\Factories\PageFactory factory(...$parameters)
 */
class Page extends Model
{
    use SoftDeletes, PicturableTrait, HasFactory;

    public const CHANGE_FREQ_DAILY = 'daily';
    public const CHANGE_FREQ_WEEKLY = 'weekly';
    public const CHANGE_FREQ_MONTHLY = 'monthly';
    public const CHANGE_FREQ_YEARLY = 'yearly';

    public const TYPE_STANDARD = 'standard';
    public const TYPE_HOMEPAGE = 'homepage';
    public const TYPE_SERVICE = 'service';
    public const TYPE_BRAND = 'brand';
    public const TYPE_CATEGORY_PRODUCT = 'category_product';
    public const TYPE_CATEGORY_NEWS = 'category_news';
    public const TYPE_PRODUCT = 'product';
    public const TYPE_CATALOG = 'catalog';
    public const TYPE_NEWS = 'news';
    public const TYPE_BLOG = 'blog';

    public const PAGEABLE_TYPE_SERVICE = 'Service';
    public const PAGEABLE_TYPE_BRAND = 'Brand';
    public const PAGEABLE_TYPE_CATEGORY_PRODUCT = 'CategoryProduct';
    public const PAGEABLE_TYPE_CATEGORY_NEWS = 'CategoryNews';
    public const PAGEABLE_TYPE_PRODUCT = 'Product';
    public const PAGEABLE_TYPE_NEWS = 'News';

    public const GCS_PATH = 'pages';

    protected $fillable = [
        'name',
        'published',
        'type',
        'sitemap',
        'change_freq',
        'sitemap_priority',
    ];

    protected $attributes = [
        'published'        => true,
        'sitemap'          => true,
        'change_freq'      => self::CHANGE_FREQ_WEEKLY,
        'type'             => self::TYPE_STANDARD,
        'sitemap_priority' => 0.5,
    ];

    protected $casts = [
        'published'        => 'boolean',
        'sitemap'          => 'boolean',
        'sitemap_priority' => 'decimal:1',
    ];

    /**
     * @return PageFactory
     */
    protected static function newFactory()
    {
        return PageFactory::new();
    }

    /**
     * @return HasMany
     */
    public function pageLangs()
    {
        return $this->hasMany(PageLang::class);
    }

    /**
     * @return MorphTo
     */
    public function pageable()
    {
        return $this->morphTo('pageable', 'pageable_type', 'pageable_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function menuItems()
    {
        return $this->hasMany(MenuItem::class);
    }
}
