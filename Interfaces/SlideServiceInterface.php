<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

use OctoCmsModule\Sitebuilder\Entities\Slide;

/**
 * Interface SlideServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface SlideServiceInterface
{
    /**
     * @param Slide $slide
     * @param array $fields
     *
     * @return Slide
     */
    public function saveSlide(Slide $slide, array $fields): Slide;
}
