<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

/**
 * Interface FooterServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface FooterServiceInterface
{

    /**
     * @param array $fields
     * @return bool
     */
    public function saveFooters(array $fields): bool;
}
