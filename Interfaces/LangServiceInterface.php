<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

/**
 * Interface PageServiceInterface
 *
 * @package OctoCmsModule\Admin\Interfaces
 * @author  danielepasi
 */
interface LangServiceInterface
{
    /**
     * @param  string $path
     * @return string
     */
    public function getLangFromPath(string $path) : string;

    /**
     * @param  string $path
     * @return string
     */
    public function removeLangPrefixFromPath(string $path): string;
}
