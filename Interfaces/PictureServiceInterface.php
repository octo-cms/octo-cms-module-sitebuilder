<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

/**
 * Interface PictureServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface PictureServiceInterface extends \OctoCmsModule\Core\Interfaces\PictureServiceInterface
{

}
