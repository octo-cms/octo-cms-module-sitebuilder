<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

use OctoCmsModule\Sitebuilder\Entities\Menu;

/**
 * Interface MenuServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface MenuServiceInterface
{
    /**
     * @param Menu  $menu
     * @param array $fields
     *
     * @return mixed
     */
    public function storeMenuItems(Menu $menu, array $fields);

    /**
     * @param Menu  $menu
     * @param array $fields
     *
     * @return Menu
     */
    public function saveMenu(Menu $menu, array $fields): Menu;

    /**
     * @return array|null
     */
    public function getMainMenu():? array;

    /**
     * @param  int $id
     * @return array|null
     */
    public function getFooterMenu(int $id):? array;
}
