<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

use OctoCmsModule\Sitebuilder\Entities\Page;
use Spatie\Sitemap\Tags\Url;

/**
 * Interface SitemapServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface SitemapServiceInterface
{
    /**
     * @param Page   $page
     * @param string $fallbackLocale
     * @param array  $languages
     *
     * @return Url|null
     */
    public function generateUrlInstance(Page $page, string $fallbackLocale, array $languages):? Url;
}
