<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

use OctoCmsModule\Core\Interfaces\SettingServiceInterface as SettingServiceInterfaceCore;

/**
 * Interface SettingServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface SettingServiceInterface extends SettingServiceInterfaceCore
{
    /**
     * @param array $fields
     */
    public function saveSiteSettings(array $fields);
}
