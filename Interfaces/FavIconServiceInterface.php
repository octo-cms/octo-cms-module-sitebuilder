<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

/**
 * Interface FavIconServiceInterface
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface FavIconServiceInterface
{
    public function createFromUrl(string $url):bool;
    public function createFromFile(string $file):bool;
}
