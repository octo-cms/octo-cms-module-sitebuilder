<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

use OctoCmsModule\Sitebuilder\Entities\PageRedirect;
use OctoCmsModule\Sitebuilder\Entities\PageLang;

/**
 * Interface PageRedirectServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface PageRedirectServiceInterface
{
    /**
     * @param PageLang $pageLang
     * @param array    $fields
     *
     * @return PageRedirect
     */
    public function storePageRedirect(PageLang $pageLang, array $fields): PageRedirect;

    /**
     * @param PageRedirect $pageRedirect
     * @param array        $fields
     *
     * @return PageRedirect
     */
    public function updatePageRedirect(PageRedirect $pageRedirect, array $fields): PageRedirect;

    /**
     * @param string $old_url
     *
     * @return PageLang|null
     */
    public function getRedirectPageLang(string $old_url): ?PageLang;
}
