<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use OctoCmsModule\Sitebuilder\Entities\Page;

/**
 * Interface PageServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface PageServiceInterface
{
    /**
     * @param Page  $page
     * @param array $fields
     *
     * @return Page
     */
    public function savePage(Page $page, array $fields): Page;

    /**
     * @param Builder $pages
     * @param array   $filters
     * @param string  $query
     *
     * @return Builder
     */
    public function filterPageBuilder(Builder $pages, array $filters = [], string $query = ''): Builder;

    /**
     * @param Page  $page
     * @param array $pageLangsData
     */
    public function updatePageLangsContents(Page $page, array $pageLangsData);
}
