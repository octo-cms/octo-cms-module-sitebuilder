<?php

namespace OctoCmsModule\Sitebuilder\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Sitebuilder\DTO\PageLangDataDTO;
use OctoCmsModule\Sitebuilder\Entities\Page;
use OctoCmsModule\Sitebuilder\Entities\PageLang;

/**
 * Interface PageServiceInterface
 *
 * @package OctoCmsModule\Sitebuilder\Interfaces
 */
interface RouteServiceInterface
{

    /**
     * @param string $path
     *
     * @return PageLang|Builder|Model|object|null
     */
    public function searchPageLang(string $path) :? PageLang;

    /**
     * @param  Page $page
     * @return array
     */
    public function getViews(Page $page) : array;

    /**
     * @param  PageLang $pageLang
     * @return PageLangDataDTO
     */
    public function getPageLangData(PageLang $pageLang): PageLangDataDTO;
}
