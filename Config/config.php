<?php

return [
    'name'  => 'Sitebuilder',
    'admin' => [
        'sidebar' => [
            [
                'order'    => 2,
                'label'    => 'sitebuilder',
                'childs'   => [
                    [
                        'label' => 'menu',
                        'route' => 'admin.vue-route.sitebuilder.menu',
                    ],
                    [
                        'label' => 'footers',
                        'route' => 'admin.vue-route.sitebuilder.footers',
                    ],
                    [
                        'label' => 'pages',
                        'route' => 'admin.vue-route.sitebuilder.pages',
                    ],
                    [
                        'label' => 'blocks',
                        'route' => 'admin.vue-route.sitebuilder.blocks',
                    ],
                    [
                        'label' => 'page_redirects',
                        'route' => 'admin.vue-route.sitebuilder.page.redirects',
                    ],
                ],
                'settings' => [
                    [
                        'label' => 'sitebuilder',
                        'route' => 'admin.vue-route.sitebuilder.settings',
                    ],
                ],
            ],
        ],
    ],
];
